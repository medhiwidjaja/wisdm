FactoryGirl.define do
  factory :user do
    sequence(:name)  { |n| "Guest #{n}" }
    sequence(:email) { |n| "guest_#{n}@meryton.net" }
    password "pride&prejudice"
    password_confirmation "pride&prejudice"

    factory :admin do
      roles ['admin']
      account 'pro'
    end

    factory :editor do
      roles ['editor']
      account 'pro'
    end

    factory :free_user do
      roles ['member']
      account 'free'
    end

    factory :basic_user do
      roles ['member']
      account 'basic'
    end

    factory :guest do
      roles ['guest']
    end

    factory :bingley do
      name     'Mr Bingley'
      email    'bingley@netherfield.com'
      account  'basic'
      roles    ['member']
    end

    factory :jane do
      name     'Jane'
      email    'jane@longbourn.net'
      account  'free'
      roles    ['member']
    end
  end

  factory :article do
    sequence(:title) { |n| "Article no. #{n}" }
    description "Lorem ipsum dolor sit amet"
    visibility 'public'
    user

    factory :interesting_article do
      title        "Most interesting person at the party"
      description  "The one I danced with most often"
    end

    factory :public_article do
      visibility 'public'
    end

    factory :private_article do
      visibility 'private'
    end

    factory :internal_article do
      visibility 'internal'
    end
  end

  factory :participation do
    article
    user

    factory :invited_participation do
      invite true

      factory :approved_participation do
        approved true
      end
    end

    factory :requested_participation do
      request true

      factory :confirmed_participation do
        confirmed true
      end
    end
  end

  factory :product do
    user

    factory :great_product do
      title 'A really great product'
    end
  end

end