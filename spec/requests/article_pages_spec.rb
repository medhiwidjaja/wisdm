require 'spec_helper'

describe "Articles" do
  before (:each) do
    @bingley = FactoryGirl.create(:bingley)
    @jane = FactoryGirl.create(:jane)
    @interesting_article = FactoryGirl.create(:interesting_article, user: @jane)
    @interesting_article.feature_now!
    @article_title = 'Most interesting person at the party'
  end
  
  describe "GET /articles" do
    it "works! (now write some real specs)" do
      # Run the generator again with the --webrat flag if you want to use webrat methods/matchers
      get articles_path
      response.status.should be(200)
    end
  end

  describe "GET featured_articles" do
    before do
      visit root_path
      click_link 'Articles'
      click_link 'Featured'
    end
    it "should lead to the featured articles" do
      page.should have_selector 'title', text:'Featured Articles'
      page.should have_selector 'a', text: @article_title
    end
  end

  describe "viewing an article" do
    describe "without a signed-in user" do
      before do
        visit '/articles/'+@interesting_article.slug
      end

      it "should lead to the article" do
        page.should have_content @article_title
      end

      it "should not have an edit button" do
        page.has_no_link? 'Edit', { href: "/articles/#{@interesting_article.slug}/edit"}
      end
    end

    describe "with a signed-in user (article's owner)" do
      before (:each) do
        visit '/users/sign_in'
        fill_in 'user_email',    with: @jane.email
        fill_in 'user_password', with: @jane.password
        click_button 'Sign in'
        visit '/articles/'+@interesting_article.slug
      end

      it "should log the user in" do
        page.should have_content @jane.name
      end

      it "should lead to the article" do
        page.should have_content @article_title
      end

      it "should have an edit button" do
        page.has_link? 'Edit', { href: "/articles/#{@interesting_article.slug}/edit"}
      end
    end
  end

end
