require 'spec_helper'

describe "Participations" do

  let(:user) { FactoryGirl.create(:user) }
  before do
    @article = user.articles.create(title: 'An article', description: 'Lorem ipsum dolor sit amet')
  end

  describe "GET 'index'" do
    it "returns http success" do
      get article_participations_path(@article.id)
      response.status.should be(200)
    end
  end

  # describe "GET 'show'" do
  #   it "returns http success" do
  #     get 'show'
  #     response.should be_success
  #   end
  # end

end