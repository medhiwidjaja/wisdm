require 'spec_helper'

describe "User pages" do

  subject { page }

  describe "sign up page" do
    before { visit new_user_registration_path }
    
    it { should have_selector 'h3', text: 'Sign up' }
    it { should have_selector 'title', text: 'Sign up' }
  end

  describe "sign in page" do
    before { visit new_user_session_path }
    
    it { should have_selector 'h3', text: 'Sign in' }
    it { should have_selector 'title', text: 'Log in' }

    it "should have the right link for forgotten password" do
      click_link "Forgot your password?"
      page.should have_selector 'title', text:'Forgot your password?'
    end
  end

  describe "sign in" do
    before (:each) do
      @bingley = FactoryGirl.create(:bingley)
      visit '/users/sign_in'
    end

    describe "with invalid information" do
      before { click_button 'Sign in' }
      it { should have_selector 'div.alert', text: 'Invalid' }
    end

    describe "with valid information" do
      before do
        fill_in 'user_email',    with: @bingley.email
        fill_in 'user_password', with: @bingley.password
        click_button 'Sign in'
      end
      it { should have_selector 'div.alert', text: 'Signed in successfully' }
    
      it "should log the user in" do
        page.should have_content @bingley.name
      end
    end
    
  end
end