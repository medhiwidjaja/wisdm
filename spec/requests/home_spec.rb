require 'spec_helper'

describe "Home page" do
  before { visit root_path }

  it "should have the right sections" do
    page.should have_selector 'h3', text: 'Featured Articles'
    page.should have_selector 'h3', text: 'New and Updated Articles'
    page.should have_selector 'h3', text: 'Top-rated Articles'
  end

  it "should have the right title" do
    page.should have_selector 'title', text: 'Home'
  end

  it "should have the right links on the layout" do
    click_link "Featured"
    page.should have_selector 'title', text:'Featured Articles'
    click_link "New and updated"
    page.should have_selector 'title', text:'New and updated articles'
    click_link "Sign in"
    page.should have_selector 'title', text:'Log in'
    click_link "Sign up"
    page.should have_selector 'title', text:'Sign up'
  end
end