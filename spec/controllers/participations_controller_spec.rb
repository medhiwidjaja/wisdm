require 'spec_helper'

describe ParticipationsController do

  let(:user) { FactoryGirl.create(:user) }
  before (:each) do
    @article = FactoryGirl.create(:article)
  end

  describe "GET 'index'" do
    it "returns http success" do
      get 'index', article_id: @article.slug
      response.should be_success
    end
  end

  describe "GET 'invite'" do
    it "returns http success" do
      get 'invite', article_id: @article.slug
      response.should be_success
    end
  end

  describe "POST 'send_invitation'" do
    let(:another_user) { FactoryGirl.create(:user) }
    it "should be successful" do
      attrs = FactoryGirl.attributes_for(:participation, :article => @article, :user => another_user)
      post 'send_invitation', article_id: @article.id, participation: attrs
      response.should redirect_to article_participations_path(:article_id => assigns[:article].id)
    end 
  end
end