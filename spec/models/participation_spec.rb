require 'spec_helper'

describe Participation do
  let(:user) { FactoryGirl.create(:user) }
  before do
    @article = user.articles.create(title: 'An article', description: 'Lorem ipsum dolor sit amet')
    @participation = @article.participations.create user: user
  end

  subject { @participation }

  describe "created" do
    it { should respond_to(:invited) } 
    it { should respond_to(:requesting) } 
    it { should be_valid }

  end


end