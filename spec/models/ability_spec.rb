require "cancan/matchers"
require "spec_helper"

describe Ability do
  before(:each) do
    @public_article = FactoryGirl.create(:public_article)
    @private_article = FactoryGirl.create(:private_article)
    @internal_article = FactoryGirl.create(:internal_article)
    @another_person = FactoryGirl.create(:user)
  end

  context "when is an admin" do
    before do
      @admin_user = FactoryGirl.create(:user)
      @admin_user.roles << 'admin'
      @ability = Ability.new(@admin_user)
    end
    it "should be able to manage articles" do
      @ability.should be_able_to(:manage, :all)
    end
  end

  describe "as an admin user" do
    before(:each) do
      @user = FactoryGirl.create(:admin)
      @ability = Ability.new(@user)
    end
    it "should be able to manage all" do
      @ability.should be_able_to(:create, Article)
    end
  end

  describe "as an editor user" do
    before(:each) do
      @joe_editor = FactoryGirl.create(:user)
      @joe_editor.roles << 'editor'
      @ability = Ability.new(@joe_editor)
    end
    it "should be able to manage category" do
      @ability.should be_able_to(:manage, Category)
    end
    it "should be able to manage featured articles" do
      @ability.should be_able_to(:manage, FeaturedArticle)
    end
  end

  describe "as a member with free account" do
    before(:each) do
      @free = FactoryGirl.create(:user)
      @free.account = 'free'
      @ability = Ability.new(@free)
    end
    it "should be able to manage public article" do
      @ability.should be_able_to(:manage, Article.new(:visibility => 'public', :user => @free))
    end
    it "should not be able to create private article" do
      @ability.should_not be_able_to(:create, Article.new(:visibility => 'private', :user => @free))
    end
    it "should not be able to create private article" do
      @ability.should_not be_able_to(:create, Article.new(:visibility => 'internal', :user => @free))
    end
    it "should not be able to read private article" do
      @ability.should_not be_able_to(:read, @private_article)
    end
    it "should not be able to read internal article" do
      @ability.should_not be_able_to(:read, @internal_article)
    end
  end

  describe "as a member with basic account" do
    before(:each) do
      @basic = FactoryGirl.create(:basic_user)
      @basic.account = 'basic'
      @ability = Ability.new(@basic)
    end
    it "should be able to manage new articles he owns" do
      public_article = @basic.articles.create title:'Basic Public', visibility:'public'
      @ability.should be_able_to(:manage, public_article)
    end
    it "should be able to manage private article" do
      private_article = @basic.articles.create title:'Basic Private', visibility:'private'
      @ability.should be_able_to(:manage, private_article)
    end
    it "should not be able to create internal article" do
      internal_article = @basic.articles.create title:'Basic Internal', visibility:'internal'
      @ability.should_not be_able_to(:create, internal_article)
    end
    it "should not be able to read other person's private article" do
      @ability.should_not be_able_to(:read, @private_article)
    end
    it "should not be able to read other person's internal article" do
      @ability.should_not be_able_to(:read, @internal_article)
    end
  end

  describe "as a member with pro account" do
    before(:each) do
      @pro = FactoryGirl.create(:basic_user)
      @pro.account = 'pro'
      @ability = Ability.new(@pro)
    end
    it "should be able to manage new articles he owns" do
      @ability.should be_able_to(:manage, Article.new(:visibility => 'public', :user => @pro))
    end
    it "should be able to manage private article" do
      @ability.should be_able_to(:manage, Article.new(:visibility => 'private', :user => @pro))
    end
    it "should be able to manage internal article" do
      @ability.should be_able_to(:manage, Article.new(:visibility => 'internal', :user => @pro))
    end
    it "should not be able to read other people's private article" do
      @ability.should_not be_able_to(:read, @private_article)
    end
    it "should not be able to read other people's internal article" do
      @ability.should_not be_able_to(:read, @internal_article)
    end
    it "should be able to create PDF report of own article" do
      @ability.should be_able_to(:create_pdf_report, Article.new(:user => @pro))
    end
  end

  describe "as an academic user" do
    before(:each) do
      @academic = FactoryGirl.create(:basic_user)
      @academic.account = 'pro'
      @ability = Ability.new(@academic)
    end
    it "should be able to create PDF report of own article" do
      @ability.should be_able_to(:create_pdf_report, Article.new(:user => @academic))
    end
  end

  describe "as a participant" do
    describe "with basic account" do
      before(:each) do
        @bingley = FactoryGirl.create(:bingley)
        @basic_user = FactoryGirl.create(:basic_user)
        @article = FactoryGirl.create(:article)
        @article.user_id = @bingley.id
        @ability = Ability.new(@basic_user)
      end
      
      describe "with modify permission" do
        before(:each) do
          @article.participations.create user:@basic_user, permissions: ['modify']
        end
        it "should be able to modify Bingley's article" do
          @ability.should be_able_to(:modify, @article)
        end
        it "should not be able to delete Bingley's article" do
          @ability.should_not be_able_to(:destroy, @article)
        end
      end

      describe "with compare permission" do
        before(:each) do
          @article.participations.create user:@basic_user, permissions: ['compare']
        end
        it "should be able to manage Bingley's article" do
          @ability.should be_able_to(:compare, @article)
        end
        it "should not be able to delete Bingley's article" do
          @ability.should_not be_able_to(:destroy, @article)
        end
      end
    end

    describe "with free account" do
      before(:each) do
        @bingley = FactoryGirl.create(:bingley)
        @free_user = FactoryGirl.create(:free_user)
        @article = FactoryGirl.create(:article)
        @article.user_id = @bingley.id
        @ability = Ability.new(@free_user)
      end

      describe "and a public article" do
        describe "with compare permission" do
          before(:each) do
            @article.participations.create user:@free_user, permissions: ['compare']
          end
          it "should be able to compare Bingley's public article" do
            @ability.should be_able_to(:compare, @article)
          end
          it "should not be able to modify Bingley's public article" do
            @ability.should_not be_able_to(:modify, @article)
          end
          it "should not be able to delete Bingley's public article" do
            @ability.should_not be_able_to(:destroy, @article)
          end
        end
      end

      describe "and a private article" do
        before(:each) do
          @article.visibility = 'private'
        end
        describe "with modify permission" do
          before(:each) do
            @article.participations.create user:@free_user, permissions: ['modify']
          end
          it "should_not be able to modify Bingley's private article" do
            @ability.should_not be_able_to(:modify, @article)
          end
          it "should not be able to delete Bingley's private article" do
            @ability.should_not be_able_to(:destroy, @article)
          end
          it "should not be able to rate Bingley's private article" do
            @ability.should_not be_able_to(:rate, @article)
          end
        end
      end
    end

    describe "with basic account" do
      before(:each) do
        @bingley = FactoryGirl.create(:bingley)
        @basic_user = FactoryGirl.create(:basic_user)
        @ability = Ability.new(@basic_user)
      end

      describe "and a public article" do
        before(:each) do
          @public_article = FactoryGirl.create(:article)
          @public_article.user_id = @bingley.id
          @public_article.visibility = 'public'
        end
        describe "with modify permission" do
          before(:each) do
            @public_article.participations.create user:@basic_user, permissions: ['modify']
          end
          it "should be able to modify Bingley's article" do
            @ability.should be_able_to(:modify, @public_article)
          end
          it "should not be able to rate Bingley's public article" do
            @ability.should_not be_able_to(:rate, @public_article)
          end
          it "should not be able to delete Bingley's article" do
            @ability.should_not be_able_to(:destroy, @public_article)
          end
        end
      end

      describe "and a private article" do
        before(:each) do
          @private_article = FactoryGirl.create(:article)
          @private_article.user_id = @bingley.id
          @private_article.visibility = 'private'
        end
        
        describe "with modify permission" do
          before do
            @private_article.participations.create user:@basic_user, permissions: ['modify']
          end
          it "should be able to modify Bingley's private article" do
            @ability.should be_able_to(:modify, @private_article)
          end
          it "should not be able to delete Bingley's private article" do
            @ability.should_not be_able_to(:destroy, @private_article)
          end
          it "should not be able to rate Bingley's private article" do
            @ability.should_not be_able_to(:rate, @private_article)
          end
        end

        describe "with compare permission" do
          before do
            @private_article.participations.create user:@basic_user, permissions: ['compare']
          end
          it "should be able to compare Bingley's private article" do
            @ability.should be_able_to(:compare, @private_article)
          end
          it "should not be able to update Bingley's private article" do
            @ability.should_not be_able_to(:update, @private_article)
          end
          it "should not be able to modify Bingley's private article" do
            @ability.should_not be_able_to(:modify, @private_article)
          end
        end
      end
    end
  end

  # describe "as a user" do
  #   before(:each) do
  #     @bingley = FactoryGirl.create(:bingley)
  #     @jane = FactoryGirl.create(:jane)
  #     @ability = Ability.new(@bingley)
  #   end
  #   it "should be able manage own friend list" do
  #     @ability.should be_able_to(:manage, @bingley.friend_list)
  #   end
  #   it "should not be able manage other people's friend list" do
  #     @ability.should_not be_able_to(:manage, @jane.friend_list)
  #   end
  # end

end