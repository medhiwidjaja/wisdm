require 'spec_helper'

describe Article do

  let(:user) { FactoryGirl.create(:user) }
  before do
    @article = user.articles.build(title: 'An article', description: 'Lorem ipsum dolor sit amet')
  end

  subject { @article }

  it { should respond_to(:title) } 
  it { should respond_to(:description) } 
  its(:user) { should == user }

  describe "when title is not present" do
    before { @article.title = "" }
    it { should_not be_valid }
  end

  describe "accessible attributes" do
    before { @bad_article = Article.new(user_id: user.id) }
    it "should not allow access to user_id" do
      assert @bad_article.user.nil?
    end
  end

  describe "when user is not present" do
    before { @article.user_id = nil }
    it { should_not be_valid }
  end

  describe "creation by a user" do
    it "should increment user's articles count" do
      expect do
        user.articles.create(title: 'An article', description: 'Lorem ipsum dolor sit amet')
      end.to change(user.articles, :count).by(1)
    end
  end

end