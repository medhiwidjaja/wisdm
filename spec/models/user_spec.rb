require 'spec_helper'
require 'cancan/matchers'

describe User do

  before do
    @darcy = User.new(name: 'Mr. Darcy', 
                            email: 'darcy@pemberley.com',
                            password: 'Str0n9pa55UUord',
                            password_confirmation: 'Str0n9pa55UUord') 
    @darcy.save
  end

  subject { @darcy }

  it { should respond_to(:name) } 
  it { should respond_to(:email) } 

  it { should be_valid }

  describe "when name is not present" do
    before { @darcy.name = " " }
    it { should_not be_valid }
  end

  describe "when email is not present" do
    before { @darcy.email = " " }
    it { should_not be_valid }
  end

  describe "when email is a duplicate" do
    before do
      @another_user = User.new(name: 'Mr. Darcy', 
                            email: 'darcy@pemberley.com',
                            password: 'Str0n9pa55UUord',
                            password_confirmation: 'Str0n9pa55UUord')
      @another_user.save
    end
    it "should be invalid" do
      assert @another_user.invalid?
    end
  end

  describe "article associations" do
    let!(:older_article) do
      FactoryGirl.create(:article, user: @darcy, created_at: 1.day.ago)
    end
    let!(:newer_article) do
      FactoryGirl.create(:article, user: @darcy, created_at: 1.hour.ago)
    end

    it "should have the right articles in the right order" do
      @darcy.articles.desc(:created_at).should == [newer_article, older_article]
    end
  end

  describe "watching an article" do
    before (:each) do
      @bingley = FactoryGirl.create(:bingley)
      @jane = FactoryGirl.create(:jane)
      @interesting_article = FactoryGirl.create(:interesting_article, user: @jane)
      @article = FactoryGirl.create(:article, user: @bingley)
      @bingley.watch! @interesting_article
    end
    
    it "should increment the watch count" do
      expect do
        @bingley.watch! @article
      end.to change {@bingley.watch_count}.by(1)
    end

    it "should have the watched article in the watch list" do
      @bingley.watch_list.article_ids.should include(@interesting_article.id)     
    end

    it "should add an entry to the article's watch list" do
      @interesting_article.watch_list_ids.should include(@bingley.watch_list_id)
    end
  end

  describe "with a boring article" do
    before (:each) do
      @bingley = FactoryGirl.create(:bingley)
      @jane = FactoryGirl.create(:jane)
      @boring_article = FactoryGirl.create(:article, user: @jane)
      @bingley.watch! @boring_article
    end

    describe "watching that article" do
      it "should have that article in the watch list" do
        @bingley.watch_list.article_ids.should include(@boring_article.id)     
      end
    end

    describe "watching and unwatching that article" do
      before { @bingley.unwatch! @boring_article }
      
      it "should remove the article from the watch list" do
        @bingley.watch_list.article_ids.should_not include(@boring_article.id)
      end

      it "should remove the entry in the article's watch list" do
        @boring_article.watch_list_ids.should_not include(@bingley.watch_list_id)
      end

      it "should leave the article intact" do
        @boring_article.should be_valid
      end
    end

    describe "decrementing" do
      it "should decrement the watch count" do
        expect do
          @bingley.unwatch! @boring_article
        end.to change {@bingley.watch_count}.by(-1)
      end
    end
  end

  describe "with a product" do
    before (:each) do
      @bingley = FactoryGirl.create(:bingley)
      @jane = FactoryGirl.create(:jane)
      @great_product = FactoryGirl.create(:great_product, user: @jane)
      @bingley.track! @great_product
    end

    describe "tracking that product" do
      it "should have the product in the product watch list" do
        @bingley.product_watch_list.should include(@great_product.id)
        @great_product.watch_list.should include(@bingley.id)
      end
    end

    describe "tracking and untracking that product" do
      before { @bingley.untrack! @great_product }

      it "should remove the product from the watch list" do
        @bingley.product_watch_list.should_not include(@great_product.id)
      end

      it "should remove the user id from the product's watch list" do
        @great_product.watch_list.should_not include(@bingley.id)
      end
    end

  end

end


