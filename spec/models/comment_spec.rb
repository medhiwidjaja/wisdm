require 'spec_helper'

describe Comment do

  before do
    @user = FactoryGirl.create(:user) 
    @article = FactoryGirl.create :article
    @comment = Comment.create commentable: @article, body:'Comment body', user: @user
  end

  describe "that is valid" do
    it "should have a user" do
      @comment.user.should_not be_nil
    end
    
    it "should have a body" do
      @comment.body.should_not be_nil
    end

    it "should have a commentable" do
      @comment.commentable.should_not be_nil
    end
  end
    
  it "should not have a parent if it is a root Comment" do
    @comment.parent.should be_nil
  end

  it "can see how many child Comments it has" do
    @comment.children.size.should == 0
  end

  it "can add child Comments" do
    child = @article.comments.new body:'Comment body', user: @user
    child.save!
    @comment.save_reply child
    @comment.children.size.should == 1
  end

  it "should increment comment count" do
    expect do
      @article.comments.create body:"A new comment", user: @user
    end.to change {@article.comment_count}.by 1
  end

  describe "after having a child added" do
    before do
      @child = @article.comments.create body:'Child comment', user: @user
      @comment.save_reply @child
    end
    
    it "can be referenced by its child" do    
      @child.parent.should == @comment
    end
    
    it "can see its child" do
      @comment.children.first.should == @child
    end
  end

  describe "finders" do
    describe "#find_comments_by_user" do
      before :each do
        @other_user = FactoryGirl.create :user
        @user_comment = @article.comments.create!(:body => "Child comment", :user => @user)
        @non_user_comment = @article.comments.create!(:body => "Child comment", :user => @other_user)
        @comments = Comment.find_comments_by(@user)
      end

      it "should return all the comments created by the passed user" do
        @comments.should include(@user_comment)
      end
      
      it "should not return comments created by non-passed users" do
        @comments.should_not include(@non_user_comment)
      end
    end

    describe "#find_comments_for" do
      before :each do
        @other_user = FactoryGirl.create :user
        @other_article = FactoryGirl.create :article
        @user_comment = @article.comments.create!(:body => 'from user', :user => @user)
        @other_comment = @other_article.comments.create!(:body => 'from other user', :user => @other_user)

        @comments = Comment.find_comments_for(@article)
      end

      it "should return the comments for the passed commentable" do
        @comments.should include(@user_comment)
      end

      it "should not return the comments for non-passed commentables" do
        @comments.should_not include(@other_comment)
      end
    end
  end
end
