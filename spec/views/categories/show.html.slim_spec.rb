require 'spec_helper'

describe "categories/show" do
  before(:each) do
    @category = assign(:category, stub_model(Category,
      :title => "Title",
      :description => "Description",
      :count => "Count",
      :color => "Color",
      :followers_count => "Followers Count"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Title/)
    rendered.should match(/Description/)
    rendered.should match(/Count/)
    rendered.should match(/Color/)
    rendered.should match(/Followers Count/)
  end
end
