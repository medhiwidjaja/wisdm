require 'spec_helper'

describe "categories/index" do
  before(:each) do
    assign(:categories, [
      stub_model(Category,
        :title => "Title",
        :description => "Description",
        :count => "Count",
        :color => "Color",
        :followers_count => "Followers Count"
      ),
      stub_model(Category,
        :title => "Title",
        :description => "Description",
        :count => "Count",
        :color => "Color",
        :followers_count => "Followers Count"
      )
    ])
  end

  it "renders a list of categories" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => "Count".to_s, :count => 2
    assert_select "tr>td", :text => "Color".to_s, :count => 2
    assert_select "tr>td", :text => "Followers Count".to_s, :count => 2
  end
end
