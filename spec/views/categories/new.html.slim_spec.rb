require 'spec_helper'

describe "categories/new" do
  before(:each) do
    assign(:category, stub_model(Category,
      :title => "MyString",
      :description => "MyString",
      :count => "MyString",
      :color => "MyString",
      :followers_count => "MyString"
    ).as_new_record)
  end

  it "renders new category form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => categories_path, :method => "post" do
      assert_select "input#category_title", :name => "category[title]"
      assert_select "input#category_description", :name => "category[description]"
      assert_select "input#category_count", :name => "category[count]"
      assert_select "input#category_color", :name => "category[color]"
      assert_select "input#category_followers_count", :name => "category[followers_count]"
    end
  end
end
