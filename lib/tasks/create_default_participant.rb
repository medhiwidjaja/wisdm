
# Create default participation for all articles
Article.all.each do |article|
  author = article.user
  if article.participations.empty?
    article.participations.create user:author, active:true
  end
end