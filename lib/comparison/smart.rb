# Copyright (c) 2012 Medhi Widjaja

require 'comparison/smart_comparison.rb'

module Comparison
  module Smart
    extend Comparison::Base
    extend ActiveSupport::Concern

    included do
      embeds_many :smart_comparisons
      accepts_nested_attributes_for :smart_comparisons, allow_destroy: false
    end

  end
end