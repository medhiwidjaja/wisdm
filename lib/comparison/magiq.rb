# Copyright (c) 2012 Medhi Widjaja

require 'comparison/magiq_comparison.rb'

module Comparison
  module Magiq
    extend Comparison::Base
    extend ActiveSupport::Concern

    included do
      embeds_many :magiq_comparisons
      accepts_nested_attributes_for :magiq_comparisons, allow_destroy: false
      field :magiq_order, type: Integer
    end

    # Returns an array of hash table 
    #  table = a_judgment.comparison_weights_by_magiq
    #  table.each {|row| puts "#{row[:rank]}. #{Criterion.find(row[:criterion_id]).title}\t #{row[:weight]}" }
    def comparison_weights_by_magiq
      k = magiq_comparisons.size
      self.magiq_comparisons.asc(:rank).collect {|c| Hash[ rank:c.rank, criterion_id:c.id1, weight:self.rank_order_centroid(k,c.rank)] }
    end

    # Rank Order Centroid
    # k = total number of criteria
    # i = rank of the i-th criterion
    def rank_order_centroid(k, i)
      (i..k).map { |j| 1.0/j }.sum / k
    end

    # Rank Sum
    # k = total number of criteria
    # i = rank of the i-th criterion
    def rank_sum(k, i)
      (k - i + 1.0) / (1..k).map { |j| k - j + 1.0 }.sum
    end

    # Rank Reciprocal
    # k = total number of criteria
    # i = rank of the i-th criterion
    def rank_reciprocal(k, i)
      (1.0/i) / (1..k).map { |j| 1.0/j }.sum
    end

    # Rank Exponent
    # k = total number of criteria
    # i = rank of the i-th criterion
    # d = dispersion (0.0 .. 1.0)
    def rank_exponent(k, i, d) 
      (k-i+1)**d / (1..k).map { |j| (k - j + 1.0)**d }.sum
    end

  end
end