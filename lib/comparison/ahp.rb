# Copyright (c) 2012 Medhi Widjaja

require 'matrix'
require 'comparison/ahp_comparison.rb'

# Ahp Module contains the classes for modeling and calculating decision hierarchy using the AHP
# (Analytic Hierarchy Process) method.
# For background theory, see http://en.wikipedia.org/wiki/Analytic_hierarchy_process
# 
module Comparison

  # Ahp embodies a single node in the decision hierarchy.
  # There are two methods to initialize it:
  #   1) node = Ahp::AhpNode.new(matrix, n)
  #      matrix must be a square matrix of dimension n*n. matrix must be in the form of reciprocal matrix
  #      suitable for Ahp calculation. No checking is made whether the matrix is really reciprocal matrix.
  #      Example: 
  #        matrix = Matrix[[1.0, 1/4.0, 4, 1/6.0], [4.0, 1.0, 4, 1/4.0], [1/4.0, 1/4.0, 1.0, 1/5.0], [6.0, 4.0, 5.0, 1.0]]
  #        node = Ahp::AhpNode.new(matrix, 4)
  #   2) node = Ahp::AhpNode.new(ary, n)
  #      ary is an array containing the elements of the upper half above the diagonal of the matrix.
  #      n is the dimension of the square matrix.
  #      The number of elements in the array must be equal to ((n**2)-n)/2
  #      For example, the AhpNode above could also be initialized in the following way
  #        node = Ahp::AhpNode.new([1/4.0, 4, 1/6.0, 4, 1/4.0, 1/5.0], 4)
  #
  # FIXME: prone to rounding error because of Float's lack of precision. Consider using BigDecimal or Rational data type
  #
  module Ahp
    extend Comparison::Base
    extend ActiveSupport::Concern

    included do
      embeds_many :ahp_comparisons
      accepts_nested_attributes_for :ahp_comparisons, allow_destroy: false
    end

    attr_reader :matrix, :n

    def setup_ahp(array, n)
      if array.class == Matrix
        raise MatrixNotSquareError unless array.square?
        @matrix = array
        raise MatrixSizeMismatch unless array.column_size == n
        @n = n
      elsif array.class == Array
        # Accept array of numbers of each element of the upper half of the matrix above the diagonal line.
        # Size of the array has to be equal to number_of_elements(n)
        raise ArrayElementsSizeMismatch unless array.count == number_of_elements(n)

        i = 0
        matrix_array = []

        (0..n-1).each do |row|
          matrix_array << []
          (0..n-1).each do |col|
            if row == col
              matrix_array[row][col] = 1.0
            elsif row < col # above the diagoal
              matrix_array[row][col] = array[i]
              i += 1
            else # row > col
              matrix_array[row][col] = 1.0/matrix_array[col][row]
            end              
          end
        end
        @matrix = Matrix.build(n) { |row, col| matrix_array[row][col] }
        @n = n
      else
        raise ArgumentDataTypeError
      end
    end

    def priority_vector
      l, idx = lambda_max
      v = @matrix.eigen.eigenvectors[idx].collect { |i| i.abs }
      sum = v.reduce(:+)
      v.map { |i| i / sum }
    end

    def pv
      priority_vector
    end

    # The largest lambda value corresponding to the eigenvectors
    # Returns the largest lambda and the row index needed to get the corresponding eigenvector
    def lambda_max
      dmax = 0.0
      idx = 0
      lambdas = @matrix.eigen.eigenvalues
      (0..lambdas.size-1).each do |i|
        d = lambdas[i]
        if d.real? && dmax < d.abs
          dmax = d.abs
          idx = i
        end
      end
      [dmax, idx]
    end

    def lm
      lambda_max.first
    end

    def inconsistency_index
      (lm - @n) / (@n - 1)
    end

    def ci
      self.inconsistency_index
    end

    def inconsistency_ratio
      random_index = [0.0, 0.0, 0.58, 0.9, 1.12, 1.24, 1.32, 1.41, 1.45, 1.49]
      inconsistency_index / random_index[@n-1]
    end

    def cr
      self.inconsistency_ratio
    end

    # Pretty print the matrix. For debugging purpose only. 
    def ppmatrix
      (0..@n-1).each {|i| print "[ "; (0..@n-1).each {|j| printf(" %0.03f ", @matrix.row(i)[j])}; print " ]\n"}
    end

    def eigenvectors
      @matrix.eigen.eigenvectors
    end

  private

    # Number of elements in strict upper half of the matrix required to create a reciprocal matrix
    def number_of_elements(n) 
      ((n**2)-n)/2 
    end

  end

  class ArgumentDataTypeError < ArgumentError
    #  "First argument to AHPNode is not a matrix or an array."
  end

  class ArrayElementsSizeMismatch < ArgumentError
    #  "Number of array elements doesn't match the specified matrix size."
  end

  class MatrixNotSquareError < ArgumentError
    #  "Matrix argument is not a square matrix."
  end

  class MatrixSizeMismatch < ArgumentError
    #  "Matrix size is different from the size argument"
  end

end