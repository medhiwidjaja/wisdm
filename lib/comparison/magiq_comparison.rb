# Copyright (c) 2012 Medhi Widjaja

class MagiqComparison
  include Mongoid::Document
  
  field :id1,  type: Moped::BSON::ObjectId
  field :rank, type: Integer

  embedded_in :judgment
end