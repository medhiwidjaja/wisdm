# Copyright (c) 2012 Medhi Widjaja

class SmartComparison
  include Mongoid::Document
  
  field :id1,   type: Moped::BSON::ObjectId
  field :value, type: Float

  embedded_in :judgment
end