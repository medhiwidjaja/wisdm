# Copyright (c) 2012 Medhi Widjaja
#

module Comparison
  module Base
    extend ActiveSupport::Concern

    included do
      field :weight,   type: Float
      field :weight_n, type: Float
      field :weight_g, type: Float
      field :total_r,  type: Float
      field :total_w,  type: Float
      field :type,     type: Integer
    end

  end
end