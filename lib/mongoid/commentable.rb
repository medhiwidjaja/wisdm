# Copyright (c) 2012 Medhi Widjaja


module Mongoid
  module Commentable
    extend ActiveSupport::Concern

    included do
      field :comment_count, type: Integer, default: 0
      has_many :comments, as: :commentable, dependent: :destroy

      class_eval "def base_class; ::#{self.name}; end"
    end

    def root_comments
      self.comments.roots
    end

  end
end