# Copyright (c) 2012 Medhi Widjaja

## This is an extension of Mongoid::Tree module in mongoid-tree gem

module Mongoid
  module TreeExt
    
    ##
    # Returns an Array containing all leaves of this document. It's cheaper than leaves method above
    # Caution: Can't be chained, since it returns Array instead of Mongoid Criteria object
    # @author: Medhi Widjaja
    def leaf_nodes
      result = [self] if self.leaf?
      if result.nil?
        result =  self.children.collect {|child| child.leaf_nodes }
      else
        result << self.children.collect {|child| child.leaf_nodes }
      end
      result.flatten
    end

    ##
    # Executes block on the leaf nodes under this document. This is much
    # faster (hundreds times faster) than doing node.leaves.each {}
    # It's also 2 times faster than doing node.leaf_nodes.each {}
    # @author: Medhi Widjaja
    def each_leaf(&block)
      block.call(self) if self.leaf?
      self.children.each {|child| child.each_leaf(&block)}
    end

  end
end