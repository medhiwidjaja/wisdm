# Copyright (c) 2012 Medhi Widjaja

module AnalysisMethods
  module ValueTreeExt
    
    def value_tree(node, scores=nil)
      proc = Proc.new {|c, scores| { 
        title: c.title, 
        abbrev: c.abbrev,
        no: c.position,
        cost: c.cost.nil? ? nil : c.cost,
        weight: scores.nil? ? nil : scores.find_by(scorable_id: c.id).weight,
        weight_n: scores.nil? ? nil : scores.find_by(scorable_id: c.id).weight_n,
        normalize: scores.nil? ? nil : scores.find_by(scorable_id: c.id).normalize
      }}

      branch = AnalysisMethods::ValueTree.new(node.id.to_s, proc.call(node, scores))

      if node.class == Criterion
        evaluation = node.privileged_evaluation_by(self)
        scores = evaluation.related_scores
        evaluation.scorables.each do |scorable| 
          new_branch = self.value_tree(scorable, scores)
          branch << new_branch unless new_branch.nil?
        end
      end
      branch
    end

  end
end
