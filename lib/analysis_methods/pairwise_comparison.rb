# Copyright (c) 2012 Medhi Widjaja

class PairwiseComparison
  include Mongoid::Document
  
  field :id1,   type: Moped::BSON::ObjectId
  field :id2,   type: Moped::BSON::ObjectId
  field :value, type: Float
  
  embedded_in :pairwise_comparable

  def slider_value
    self.value
  end

  def comparison_value
    self.value > 0 ? 1/(value+1) : -value+1
  end

  def convert
    v = self.value.to_r
    w = v >= 1 ? -v+1 : v.denominator-1
    w.to_f
  end
end