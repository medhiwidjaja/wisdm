# Copyright (c) 2012 Medhi Widjaja

require 'analysis_methods/direct_comparison.rb'

module AnalysisMethods
  module Smart
    extend ActiveSupport::Concern

    included do
      field       :minimize, type: Boolean, default: false
      field       :range_min, type: Float
      field       :range_max, type: Float
      field       :normalize, type: Boolean, default: true
      field       :smart_notes, type: String
      embeds_one  :value_function 
      embeds_many :smart_comparisons, class_name: 'DirectComparison', as: :direct_comparison
      accepts_nested_attributes_for :smart_comparisons, allow_destroy: false
    end

    def update_smart_scores
      if self.eval_object == "Criterion"
        update_criteria_weights
      elsif self.eval_object == "Alternative"
        update_alternative_ratings
      end
    end

    def update_criteria_weights
      total = self.smart_comparisons.sum &:value
      scorables = self.scorables
      self.smart_comparisons.each do |smart_comparison|
        score = smart_comparison.value
        weight_n = score / total unless total == 0
        
        scorable = scorables.find(smart_comparison.id1)
        self.save_score rating: score,
                        weight: score, 
                        weight_n: weight_n, 
                        scorable: scorable, 
                        with_respect_to: evaluable,
                        eval_method: Evaluation::EVALUATION_METHODS['SMART']
      end
    end

    def update_alternative_ratings
      min = self.range_min || self.smart_comparisons.map{ |c| c.value }.min
      max = self.range_max || self.smart_comparisons.map{ |c| c.value }.max
      range = max - min
      scorables = self.scorables
      
      self.smart_comparisons.each do |smart_comparison|
        rating = smart_comparison.value
        if self.minimize?
          score = (max - rating.to_f) / range unless range == 0
        else
          unless [self.range_min, self.range_max].any? &:blank?
            score = (rating.to_f - min) / range unless range == 0
          else
            score = rating.to_f
          end
        end

        scorable = scorables.find(smart_comparison.id1)
        self.save_score rating: rating,
                        weight: score, 
                        scorable: scorable, 
                        with_respect_to: evaluable,
                        eval_method: Evaluation::EVALUATION_METHODS['SMART']
      end

      # Normalize weights
      scores = self.related_scores
      total_w = scores.sum &:weight
      scores.each do |sc|
        unless total_w == 0
          sc.update_attributes weight_n: sc.weight/total_w, normalize: self.normalize
        end
      end
    end
  end
end