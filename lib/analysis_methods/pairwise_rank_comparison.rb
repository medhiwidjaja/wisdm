# Copyright (c) 2012 Medhi Widjaja

class PairwiseRankComparison
  include Mongoid::Document
  
  field :rank1, type: Integer
  field :rank2, type: Integer
  field :value, type: String
  
  embedded_in :evaluation

  after_save :update_comparables

  private

  def update_comparables

  end
end