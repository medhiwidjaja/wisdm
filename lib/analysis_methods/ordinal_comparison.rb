# Copyright (c) 2012 Medhi Widjaja

class OrdinalComparison
  include Mongoid::Document
  
  field :id1,   type: Moped::BSON::ObjectId
  field :title, type: String
  field :pos,   type: Integer
  field :rank,  type: Integer
  field :score, type: Float
  embedded_in :ordinal_comparable

  validates_uniqueness_of :id1
  validates_presence_of :rank, :pos, :title, :id1 
end