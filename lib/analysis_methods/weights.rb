# Copyright (c) 2012 Medhi Widjaja

module AnalysisMethods
  module Weights
    extend ActiveSupport::Concern

    included do
      # This section is deprecated -->
      field :weight,      type: Float
      field :weight_n,    type: Float
      field :weight_g,    type: Float
      field :total_r,     type: Float
      field :total_w,     type: Float
      # <-----------------------------

      has_many :scores, as: :judgment, dependent: :destroy
    end

    def save_score(args)
      score = self.scores.find_or_create_by scorable_id: args[:scorable].id,
                                          scorable_type: args[:scorable].class.to_s,
                                        with_respect_to: args[:with_respect_to].id,
                                            eval_method: args[:eval_method]
      score.update_attributes! rating: args[:rating], weight: args[:weight], weight_n: args[:weight_n], rank_order: args[:rank_order]
      self.update_attribute :valid, true
      self.evaluable.article.update_attribute :recalc, false
    end

    def related_scores(method=self.eval_method)
      self.scores.where(eval_method: method)
    end

    def score_table(method=self.eval_method)
      unless self.scores.empty?
        scores_array = self.related_scores
        # unless scores_array.any? {|score| score.nil? || score.weight.nil? }
        unless scores_array.any? {|score| score.nil? }
          max = scores_array.max_by(&:weight).weight
          scores_array.map {|score| Hash[
            no:       begin score.scorable.position+1 rescue nil end, 
            id:       begin score.scorable.id rescue nil end, 
            title:    begin score.scorable.title rescue nil end, 
            order:    score.rank_order, 
            rating:   score.rating,
            weight:   score.weight, 
            weight_n: score.weight_n,
            # ratio:    (max.nil? or max==0) ? 0 : score.weight.to_f/max*100
            ratio:    begin score.weight_n*100 rescue nil end
          ]}
        else
          self.scorables.map {|c| Hash[no:c.position+1, title:c.title]}
        end
      end
    end

    def score_for_item(item)
      related_scores.where(scorable_id: item.id).first
    end

    def score_for_item_id(item_id)
      related_scores.where(scorable_id: item_id).first
    end
  end
end