# Copyright (c) 2012 Medhi Widjaja

class MagiqComparison
  include Mongoid::Document
  
  field :id1,   type: Moped::BSON::ObjectId
  field :type,  type: String
  field :title, type: String
  field :pos,   type: Integer
  field :rank,  type: Integer
  field :score, type: Float
  embedded_in :comparison

  validates_uniqueness_of :id1
  #validates_presence_of :rank, :pos, :title, :id1 
end