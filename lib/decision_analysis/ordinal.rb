# Copyright (c) 2012 Medhi Widjaja


module DecisionAnalysis
  module Ordinal
    
    # Rank Order Centroid
    def roc(n, k=1)
      weight = 0.0;
      (k..n).each do |i|
        weight += 1.0 / i
      end
      weight / n
    end

  end
end
