# Copyright (c) 2012 Medhi Widjaja

require 'decision_analysis/smart_comparison.rb'

module DecisionAnalysis
  module Smart
    extend DecisionAnalysis::Base
    extend ActiveSupport::Concern

    included do
      field       :minimize, type: Boolean, default: false
      embeds_one  :value_function 
      embeds_many :smart_comparisons
      accepts_nested_attributes_for :smart_comparisons, allow_destroy: false
    end

    def update_smart_scores(pid)
      total_w = self.smart_comparisons.map{ |c| c.weight }.sum
      min = self.smart_comparisons.map{ |c| c.weight }.min
      max = self.smart_comparisons.map{ |c| c.weight }.max
      range = max - min
      total = self.smart_comparisons.map{|c| max-c.weight}.sum
      article = self.comparable.article

      self.smart_comparisons.each do |smart_comparison|
        weight = smart_comparison.weight
        if self.minimize?
          # Not normalized:
          # if range == 0
          #   weight_n = 1
          # else
          #   weight_n = (max - weight.to_f) / range unless range == 0
          # end
          # Normalized:
          weight_n = (max - weight.to_f) / total unless total == 0
        else
          # Not normalized:
          # if range == 0
          #   weight_n = 1
          # else
          #   weight_n = (weight.to_f - min) / range
          # end
          # Normalized:
          weight_n = weight.to_f / total_w.to_f unless total_w == 0
        end
        if self.comparable.leaf?
          alternative = article.alternatives.find(smart_comparison.id1)
          judgment = alternative.ratings.find_or_create_by participation_id:pid, criterion_id:self.comparable.id
        else
          criterion = article.criteria.find(smart_comparison.id1)
          judgment = criterion.comparisons.find_or_create_by participation_id:pid
        end
        judgment.update_attributes! weight: weight, weight_n: weight_n
      end

      # if self.comparable.leaf?
      #   judgments = article.alternatives.map {|alt| alt.ratings.find_or_create_by participation_id:pid, criterion_id:self.comparable.id}
      # else
      #   judgments = self.comparable.children.map {|child| child.comparisons.find_or_create_by participation_id:pid }
      # end
      # judgments.each do |j|
      #   weight = self.smart_comparisons.where(id1:j.id).first.weight
      #   if self.minimize?
      #     # Not normalized:
      #     if range == 0
      #       weight_n = 1
      #     else
      #       weight_n = (max - weight.to_f) / range unless range == 0
      #     end
      #     # Normalized:
      #     # weight_n = (max - weight.to_f) / total unless total == 0
      #   else
      #     # Not normalized:
      #     if range == 0
      #       weight_n = 1
      #     else
      #       weight_n = (weight.to_f - min) / range
      #     end
      #     # Normalized:
      #     # weight_n = weight.to_f / total_w.to_f unless total_w == 0
      #   end
      #   j.update_attributes! weight: weight, weight_n: weight_n
      # end

    end

  end
end