# Copyright (c) 2012 Medhi Widjaja

require 'decision_analysis/magiq_comparison.rb'

module DecisionAnalysis
  module Magiq
    extend DecisionAnalysis::Base
    extend ActiveSupport::Concern

    included do
      embeds_many :magiq_comparisons
      accepts_nested_attributes_for :magiq_comparisons, allow_destroy: false
      field :dist, type: Integer
    end

    module ClassMethods
      # Rank Order Centroid
      # k = total number of criteria
      # i = rank of the i-th criterion
      def rank_order_centroid(k, i)
        (i..k).map { |j| 1.0/j }.sum / k
      end

      def rank_order_centroid_table(k)
        (1..k).map { |j| rank_order_centroid(k, j) }
      end

      # Rank Sum
      # k = total number of criteria
      # i = rank of the i-th criterion
      def rank_sum(k, i)
        (k - i + 1.0) / (1..k).map { |j| k - j + 1.0 }.sum
      end

      def rank_sum_table(k)
        (1..k).map { |j| rank_sum(k, j) }
      end

      # Rank Reciprocal
      # k = total number of criteria
      # i = rank of the i-th criterion
      def rank_reciprocal(k, i)
        (1.0/i) / (1..k).map { |j| 1.0/j }.sum
      end

      def rank_reciprocal_table(k)
        (1..k).map { |j| rank_reciprocal(k, j) }
      end

      # Rank Exponent
      # k = total number of criteria
      # i = rank of the i-th criterion
      # d = dispersion (0.0 .. 1.0)
      def rank_exponent(k, i, d=0.2) 
        (k-i+1)**d / (1..k).map { |j| (k - j + 1.0)**d }.sum
      end

      def rank_exponent_table(k, d=0.2)
        (1..k).map { |j| rank_exponent(k, j, d) }
      end
    end

    # Returns an array of hash tables 
    #  table = a_judgment.comparison_weights_by_magiq
    #  table.each {|row| puts "#{row[:rank]}. #{Criterion.find(row[:criterion_id]).title}\t #{row[:weight]}" }
    def comparison_weights_by_magiq
      k = magiq_comparisons.size
      self.magiq_comparisons.asc(:rank).collect {|c| Hash[ rank:c.rank, criterion_id:c.id1, weight:self.rank_order_centroid(k,c.rank)] }
    end
   
    # Returns score computed with the specified method;
    # Default method is rank order centroid. Other accepted methods are
    # :rank_sum, :rank_reciprocal, :rank_exponential, :simple_rank
    def update_magiq_scores(method=:rank_order_centroid)
      k = self.magiq_comparisons.size
      score_table = Comparison.send(method.to_s+'_table', k)
      j = 1   # to keep track of the number of items
      r = 1   # to keep track of the number of ranks used
      begin
        n = self.magiq_comparisons.where(rank:r).count
        unless n==0
          score = score_table.slice(j-1, n).sum / n 
          self.magiq_comparisons.where(rank:r).each do |mc|
            mc.update_attribute :score, score
          end
          j += n
        else
          j += 1
        end
        r += 1
      end while j <= k
    end

  end
end