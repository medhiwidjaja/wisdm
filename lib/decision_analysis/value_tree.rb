require 'tree'

# Copyright (c) 2012 Medhi Widjaja
#
# Value Tree model
# This is designed for flexibility to handle different and multiple decision methods
# (SMART, AHP, SMARTER, etc)
#
# TODO: Handle calculation of mixed-method on a per-node basis
#       Each node will have :method key (ahp, smart, vf, smarter, etc)
#
# Example use:
# 
# Setup of Objective tree:
#   article = Article.find(id)
#   root = article.objectives.root
#
# Assuming the nodes in Objective tree are already populated with weights
#   value_tree = root.as_value_tree('_id') {|o| {title: o.title, weight: o.weight}}
#   value_tree.globalize(:weight)   # this will create :weight_g in each node
#
# Each node will then have content hash {:title => ..., :weight => ..., :weight_n => ..., :weight_g => ...}
# The global weight of the first grandchild from root can be accessed with
#   value_tree.children.first.children.first.weight_g
# or by id:
#   value_tree.search(node.id.to_s).weight_g
#
# AHP:
#   node.setup [1/4.0, 4.0, 1/6.0], 3   # initialize with comparison value
#   node.pv                             # calculate Priority Vector
#   vt.update_weights_with_pv(:ahp_weight)    # populate :ahp_weight
#   vt.globalize :ahp_weight            # create :ahp_weight_g in each node
#
# Each node's content hash will have additional keys :ahp_weight, :ahp_weight_n, :global_ahp_weight
# The AHP global weight of a node can then the accessed with:
#   node.weight_ahp_g
#
# To insert comparison of alternatives, setup the leaf nodes with the AHP matrix
#   some_leaf_node.setup some_ahp_matrix, matrix_size
#   


module DecisionAnalysis
  
  class ValueTree < Tree::TreeNode
    # include DecisionAnalysis::AhpMethod::AhpNode

    # Calculate and update node's normalized weights
    # Example: to calculate the whole tree's nodes normalized weights
    #   root.each { |n| n.normalize(:weight) }
    # assuming that the nodes' content contains :weight key
    def normalize (weight) 
      self.parent.content.update(sum: self.parent.children.reduce(0) {|sum, c| sum + c.content[weight] } )
      self.content.update((weight.to_s+"_n").to_sym => self.content[weight].to_f / self.parent.content[:sum].to_f)
      # self.content.update (weight.to_s+"_n").to_sym => self.content[weight].to_f 
    end

    # def normalize (weight)
    #   unless is_leaf?
    #     sum = self.children.reduce(0) { |sum, c| sum+c.content[weight] }
    #     self.children.collect { |c| c.content.update((weight.to_s+"_n").to_sym => c.content[weight]/sum) }
    #   end
    # end

    # Calculate and update all tree's nodes' global weights 
    #
    # Note:
    # To calculate a node's global weight, you can also use:
    #   a_node.parentage.reduce(1) {|tot,n| tot * (n.is_root? ? 1 : n.content[:weight_n]) }
    # (after calculating all nodes' normal weights using normalize)
    #
    def globalize(weight)
      self.each do |node| 
        # No need to do this to alternative nodes
        node.normalize(weight) unless node.is_leaf? or node.is_root?
        # node.normalize(weight) unless node.is_root?
      end
      w_n = (weight.to_s+"_n").to_sym
      w_g = (weight.to_s+"_g").to_sym
      self.each do |node|
        if node.is_root?
          global_weight = 1     # global weight of root is always 1
        else
          global_weight = (node.parent.content[w_g] || 0) * (node.content[w_n] || 0)
        end
        node.content.update(w_g => global_weight)
      end
    end

    # For 'flat model' such as the one in Decision Analysis for Management Judgment book
    # Must first calculate the parent normal weights *BEFORE* inserting the ratings into the tree
    #   sum = 0
    #   self.each_leaf {|n| sum+=n.weight}
    #   self.each_leaf {|n| n.content.update(:weight_n => n.weight/sum*100) }
    def apply_weight_to_ratings
      self.each_leaf do |node|
        weighted_rating = (node.parent.weight_n || 0) * (node.rating || 0)
        node.content.update(:weight_g => weighted_rating)
      end
    end

    # Consolidates the value tree to output a table of rank of alternatives with their scores
    # Output is an array of scores in the form of [[ rank, alt. title, score, ratio ],...]
    # for each alternative sorted by rank
    # this is rather ugly, but it's simpler than using matrix multiplication,
    # since then we have to make sure all elements are in the correct order
    # Note, using matrix multiplication, where vt is value tree containing *criteria only*
    #   mat1 = Matrix.columns(root.leaf_nodes.map {|n| normalize(n.ratings.map {|r| r.value})})
    #   mat2 =  Matrix.column_vector( vt.each_leaf {|o| o.weight_g}.collect {|o| o.weight_g} )
    #   scores = mat1 * mat2
    def consolidate(rating)
      scores = {}
      self.each_leaf do|node|
        # Leaf nodes are alternatives under each leaf objectives
        if scores.has_key?(node.name)
          scores[node.name][:score] += node.content[rating]
          scores[node.name][:obj] = scores[node.name][:obj].merge Hash[node.parent.name, node.content[rating]]
        else
          scores[node.name] = { title: node.title, abbrev: node.abbrev, cost: node.cost, score: node.content[rating] }
          scores[node.name][:obj] = {node.parent.name => node.content[rating]}
        end
      end
      # Insert ratio value
      max_score = scores.reduce(0) { |max, alt| max > alt.last[:score] ? max : alt.last[:score] } 
      scores.each { |k,v| v.update(ratio: v[:score].to_f/max_score) }
      
      # At this point each entry in scores is in the form of:
      # {"alternative id"=>{:title=>"Alt title", :score=>0.xxx, obj_id-1:0.xxx :ratio=>0.xxx}
      
      # Sort by score
      scores = scores.sort { |a,b| a.last[:score] <=> b.last[:score] }.reverse

      # Put into hash
      rank = 0
      score_table = scores.map {|s| rank+=1; {rank: rank, alt_id: s.first}.merge s.last}
    end

    # Returns the node with name == node_name or nil if not found
    def search (node_name)
      match = nil
      self.each { |n| match = n if n.name == node_name }
      match
    end

    # AHP-related methods
    # Copy AHP priority vector members into children's weight
    def update_weights_with_pv(weight)
      child = children.first
      pv.each do |p| 
        child.content.update(weight => p)
        child = child.next_sibling
      end
    end

    # Gives convenience method to access the content's hash member
    # Example:
    # If a node's content is {title: 'Title', weight: 13}
    #   node.title will return 'Title'
    #   node.weight will return 13
    #   node.no_key will raise error
    def method_missing(meth)
      sym = meth.to_sym
      if self.content.has_key?(meth)
        self.content[sym]
      else
        super
      end
    end

    # for consistency with Mongoid::Tree naming 
    def root?
      is_root?
    end

    # for consistency with Mongoid::Tree naming 
    def leaf?
      is_leaf?
    end

    # Pretty print the value tree for debugging in console:
    def ppvt(key) 
      self.collect {|n| puts '  '*n.node_depth+'- '+n.content[:title]+":\t "+(n.content[key].nil? ? '' : n.content[key].round(3).to_s) } 
    end

    # Override marshal_load so that it create ValueTree class nodes instead of Tree::TreeNode
    def marshal_load(dumped_tree_array)
      nodes = { }
      dumped_tree_array.each do |node_hash|
        name        = node_hash[:name]
        parent_name = node_hash[:parent]
        content     = Marshal.load(node_hash[:content])

        if parent_name then
          nodes[name] = current_node = DecisionAnalysis::ValueTree.new(name, content)
          nodes[parent_name].add current_node
        else
          # This is the root node, hence initialize self.
          initialize(name, content)

          nodes[name] = self    # Add self to the list of nodes
        end
      end
    end
  end

end