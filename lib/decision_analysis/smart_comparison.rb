# Copyright (c) 2012 Medhi Widjaja

class SmartComparison
  include Mongoid::Document
  
  field :id1,    type: Moped::BSON::ObjectId
  field :weight, type: Float

  embedded_in :comparison
end