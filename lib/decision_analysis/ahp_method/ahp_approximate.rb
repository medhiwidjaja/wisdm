# In this module the priority vector is not calculated with the eigensystem of the matrix
# 
module AhpApproximate
  class AhpNode
    @matrix = []
    @n = 0

    def initialize(matrix)
      @matrix = matrix
      @n = @matrix.length
      @priority_vector = priority_vector
      @lambda_max = lambda_max
      @inconsistency_index = inconsistency_index
      @inconsistency_ratio = inconsistency_ratio
    end

    def priority_vector
      m = @n - 1
      vector = []
      total = 0
      (0..m).each do |i|
        product = 1.0
        (0..m).each { |j| product *= @matrix[i][j] }
        vector[i] = product**(1.0/@n)
        total += vector[i]
      end
      (0..m).each { |i| vector[i] /= total }
      vector
    end

    def lambda_max
      m = @n - 1
      lm = 0.0
      total = 0.0
      vec = 0.0
      (0..m).each do |j|
        total = 0.0
        (0..m).each { |i| total += @matrix[i][j] }
        vec =  total * @priority_vector[j]
        lm += vec
      end
      lm
    end

    def inconsistency_index
      (@lambda_max - @n) / (@n - 1)
    end

    def inconsistency_ratio
      random_index = [0.0, 0.0, 0.58, 0.9, 1.12, 1.24, 1.32, 1.41, 1.45, 1.49]
      inconsistency_index / random_index[@n-1]
    end
  end
end