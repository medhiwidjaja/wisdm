# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

puts 'EMPTY THE MONGODB DATABASE'
Mongoid.master.collections.reject { |c| c.name =~ /^system/}.each(&:drop)

puts 'Fill database with sample data'

puts 'Make users'
admin = User.create!(name: "Admin",
                     email: "reckonery@yahoo.com",
                     password: "foobar", 
                     password_confirmation: "foobar")
admin.role = "admin"

medhi = User.create!(name: "Medhi",
                     email: "medhiwidjaja@yahoo.com",
                     password: "foobar", 
                     password_confirmation: "foobar")
medhi.role = "admin"

darcy = User.create!(name: "Mr. Darcy",
                     email: "darcy@pemberley.com",
                     password: "foobar", 
                     password_confirmation: "foobar")

5.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name: name,
               email: email,
               password: password,
               password_confirmation: password)
end

45.times do |n|
  name  = Faker::Name.name
  email = "user-#{n+1}@forestep.org"
  password = "password"
  User.create!(name: name,
               email: email,
               password: password,
               password_confirmation: password)
end

puts 'Make articles'
User.all.each do |user|
  rand(4..30).times do
    co = Faker::Company.name
    title = Faker::Company.bs
    phrase = Faker::Company.catch_phrase
    description = phrase + ". Used at " + co + " to " + Faker::Company.bs
    user.articles.create(title: title.capitalize,
                         description: description)
  end
end

puts 'Make relationships'
User.all.each do |user|
  followed_users = []
  followers = []
  rand(30).times do
    other_user = User.all[rand(0..52)]
    followed_users << other_user unless followed_users.include?(other_user)
    followers << other_user unless followed_users.include?(other_user)
  end
  followed_users.each { |followed| user.follow!(followed) }
  followers.each      { |follower| follower.follow!(user) }
end
