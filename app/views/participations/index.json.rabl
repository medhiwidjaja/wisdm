object @participations
attributes :username, :status, :active, :weight, :permissions
node :email do |participation|
  participation.user.email
end
