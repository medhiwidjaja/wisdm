# This will build a JSON object of the Objective tree for tree.jquery.js
# Note that tree.jquery.js wants an array, so don't forget to wrap the JSON
# object inside an array

object @root => :node
attributes :title => :label, :_id => :id,
 :has_incomplete_weights? => :weights_incomplete,
 :has_incomplete_evaluations? => :evaluations_incomplete

# node(:children, :unless => lambda { |n| n.children == [] }) do |n|
#   n.children.map { |c| { :node => partial("objectives/index", :object => c) }  }
# end
# Not quite what we want.

# node(:children, :unless => lambda { |n| n.children == [] }) do |n|
#    n.children.map { |c| { :node => partial("objectives/index", :object => c) }  }
# end
# Not this either.

# child (:children, :unless => lambda { |n| n.children == [] }) => :nodes do
#   extends "objectives/index"
# end
# Nope.

# Finally, the simplest actually does the job
child :children => :children do
  extends "objectives/index"
end
