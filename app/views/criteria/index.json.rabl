# Copyright (c) 2012 Medhi Widjaja

# This will build a JSON object of the Criterion tree for tree.jquery.js
# Note that tree.jquery.js wants an array, so don't forget to wrap the JSON
# object inside an array

object @root => :node
attributes :title => :label, :_id => :id

child :children => :children do |node|
  extends "criteria/index"
end
