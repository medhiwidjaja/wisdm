# Copyright (c) 2012 Medhi Widjaja

# This will build a JSON object of the Criterion tree for tree.jquery.js
# Note that tree.jquery.js wants an array, so don't forget to wrap the JSON
# object inside an array

object @root => :node
attributes :title => :label, :_id => :id
node(:weights_incomplete) { |node| node.has_incomplete_weights_by?(@participation_id) } 
node(:ratings_incomplete) { |node| node.has_incomplete_ratings_by?(@participation_id) }

# child children: :children  do 
#   extends "criteria/tree"
#   node(:weights_incomplete) { |node| node.has_incomplete_weights_by?(@participation_id) } 
#   node(:ratings_incomplete) { |node| node.has_incomplete_ratings_by?(@participation_id) } 
# end

# child children: :children  do 
#   extends "criteria/tree"
#   node(:weights_incomplete) { |node| node.has_incomplete_weights_by?(@participation_id) } 
#   node(:ratings_incomplete) { |node| node.has_incomplete_ratings_by?(@participation_id) } 
# end

node :children do |node|
  node.children.map { |child| partial("criteria/tree", :object => child) }
end