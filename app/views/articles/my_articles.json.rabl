# Copyright (c) 2012 Medhi Widjaja

collection @articles
cache @articles
attributes :title => :label, :description => :description

node(:id)         { |article| article.id.to_s }
node(:slug)       { |a| a.slug }
node(:thumbnail)  { |article| article.pic_snippet }