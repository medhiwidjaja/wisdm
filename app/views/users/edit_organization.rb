- provide :title, "Editing user organization"

- content_for :left_frame do 
  = render partial: 'users/account_sidepanel'

#content-box
  .widget
    .widget-header
      i.icon-user
      h3 Edit Organization
    .well.well-widget-content
      .widget-content-title
        | Organization

      h4 Tell people a little bit about who you are.

      = form_for @user do |f|
        - if @user.errors.any?
          .alert.alert-error
            a.close data-dismiss="alert" ×
            = "Please fix the #{inflectize(@user.errors.count, "error")} as indicated below"
            ul
              - @user.errors.full_messages.each do |message|
                li = message

        .control-group
          = f.label :organization, class: "control-label"
          .controls
            = f.text_area :organization, class: 'input-xxlarge wysihtml5', rows: 5


        #toolbar.button-toolbar
          = f.submit 'Save Profile', class:'btn btn-primary'
