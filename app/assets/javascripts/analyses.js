function build_tree(article_id) {
  $(function() {
      $.getJSON( '/articles/'+article_id+'/objectives.json',
      function(data) {
        $('#analyses-tree').tree({
          data: [data],
          autoOpen: true,
          dragAndDrop: false,
          selectable: true,
          onCreateLi: function(node, $li) {
            if (node.children.length == 0) {
              $li.find('.title').before('<i class="icon-leaf"></i> ');
            } else {
              $li.find('.title').before('<i class="icon-th-list"></i> ');
            }
          }
        });
      });

    });
};

$(function() {
	if ($('#analyses-tree').length > 0) $('#analyses-tree').live(build_tree($('#analyses-tree').data('article')));
});