$(function() {
  $('#sidepanel ul a').pjax('[data-pjax-container]');
  $('.side-widget a:not([data-skip])').pjax('[data-pjax-container]');
  $('a.pjax').pjax('[data-pjax-container]');
  $('a.btn:not([data-remote]):not([data-skip])').pjax('[data-pjax-container]');
  $('a.page').pjax('[data-page-container]');
//  $('header li a:not([data-skip])').pjax('[data-pjax-container]');
});

$(document).on('submit', "form.pjax", function(event) {
	$.pjax.submit(event, '[data-pjax-container]');
});
