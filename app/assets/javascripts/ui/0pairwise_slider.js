var Avalon = window.Avalon || {};

Avalon.Pairwise = {
	sliderDivClass: '.pairwise-slider',
	defaultScale: 'numeric',

	importanceScale: function(val) {
		var scales = ['equally as good as',
						'weakly better than',
						'strongly better than',
						'very strongly better than',
						'extremely better than'];
		return scales[(val+1)/2-1];
	},

	numericScale: function(val) {
		var scales = ['equal to',
					  '2 X', '3 X', '4 X', '5 X', '6 X', '7 X', '8 X', '9 X' ];
		return scales[val-1];
	},

	freeScale: function(val) {
		return val + " x";
	},

	// displayPopover: function(id, val) {
	// 	$("#"+id).popover({title:"Importance", content: importanceScale(val)});
	// 	$("#"+id).popover("show");
	// },

	useScale: function(scale, disable) {
		if (scale=='importance') { 
			$("#ahp-scale").attr("value", 'importance');
			Avalon.Pairwise.build_sliders(2, Avalon.Pairwise.importanceScale, disable);
			label = 'Verbal Importance';
		}
		else if (scale=='numeric') { 
			$("#ahp-scale").attr("value", 'numeric');
			Avalon.Pairwise.build_sliders(1, Avalon.Pairwise.numericScale, disable);
			label = 'Numeric (0..9)';
		}
		else if (scale=='free') { 
			$("#ahp-scale").attr("value", 'free');
			Avalon.Pairwise.build_sliders(0.1, Avalon.Pairwise.freeScale, disable);
			label = 'Free scale (0.0 - 9.0)';
		};
		$('#scale-label').text(label);
	},

	build_sliders: function(numberOfSteps, scaleFunction, disable) {
		var sliderOpts = {
			min: 1,
			max: 9,
			step: numberOfSteps,
			range: 'min',
			disabled: disable,
			change: function(e, ui) {
				var val = $(this).slider("value");
				var id = $(this).attr("id");
				var otherId = id.substring(0,id.length-1) + (id.charAt(id.length-1)=='a'?'b':'a');
				var val_str = (id.charAt(id.length-1)=='a'? val : "1/"+val);
				
				$("#val-"+id.substring(0,id.length-1)).html($(this).attr("name") + " is<br/><span class='importance'>"+scaleFunction(val)+"</span><br/>" + $("#"+otherId).attr("name"));
				$("#comparisons_"+id.substring(0,id.length-1)).attr("value", val_str);
			},
			start: function(e, ui) {
				var id = $(this).attr("id");
				var otherId = id.substring(0,id.length-1) + (id.charAt(id.length-1)=='a'?'b':'a');
				var val = $(this).slider("value");
				if (val == 1) {
					$("#"+id).parent().addClass("slider-selected");
					$("#"+otherId).parent().removeClass("slider-selected");
					$("#"+otherId).slider("value", 1);
					$("#val-"+id).removeClass('hidden').addClass('shown');
					$("#val-"+otherId).removeClass('shown').addClass('hidden');
				}
			},
			slide: function(e, ui) {
				var val = ui.value;
				var id = $(this).attr("id");
				var otherId = id.substring(0,id.length-1) + (id.charAt(id.length-1)=='a'?'b':'a');
				var val_str = (id.charAt(id.length-1)=='a'? val : "1/"+val);

				$("#val-"+id.substring(0,id.length-1)).html($(this).attr("name") + " is<br/><span class='importance'>"+scaleFunction(val)+"</span><br/>" + $("#"+otherId).attr("name"));
			}
		};
		$(Avalon.Pairwise.sliderDivClass).slider(sliderOpts);

		$("div[class^=option]").live("click", (function() {
			$("div[data-pair='"+$(this).data("pair")+"']").removeClass("option-selected");
			$(this).addClass('option-selected');
		}));
	}
};

