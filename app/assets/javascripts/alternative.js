var Avalon = window.Avalon || {};

Avalon.Confirmation = {
	pageIsDirty: false,
	ignoreChanges: false,
	checkChanges: function () {
		if (Avalon.Confirmation.pageIsDirty) {
			var ignoreChanges = confirm("Dirty? "+ Avalon.Confirmation.pageIsDirty +". Unsaved changes. Proceed anyway?");
			Avalon.Confirmation.ignoreChanges = ignoreChanges;
			return (ignoreChanges);
		} /*else {
			var ignoreChanges = confirm("Dirty? "+ Avalon.Confirmation.pageIsDirty +". No changes. Proceed?");
			Avalon.Confirmation.ignoreChanges = ignoreChanges;
			return (ignoreChanges);
		} */
		return true;
	},
	proceed: function (fn)  {
		if (!Avalon.Confirmation.pageIsDirty || (Avalon.Confirmation.pageIsDirty && Avalon.Confirmation.ignoreChanges)) {
			Avalon.Confirmation.pageIsDirty = false;
			fn();
		}
	}
}

$(document).on("ready pjax:end", function() {
	// Avalon.Confirmation.pageIsDirty = false;
	// $('input').not(".ignore").live("keyup", (function() {
	// 	// control with class "ignore" does not fire the pageIsDirty flag
	// 	Avalon.Confirmation.pageIsDirty = true;
	// 	$(this).css({backgroundColor: "yellow"});
	// }));

	// $('textarea').not(".ignore").live("keyup focusout", (function() {
	// 	// control with class "ignore" does not fire the pageIsDirty flag
	// 	Avalon.Confirmation.pageIsDirty = true;
	// 	$(this).css({backgroundColor: "yellow"});
	// }));
	//$("a").not(".always").attr("onclick", "return Avalon.Confirmation.checkChanges();");
	// a with class "always" is not checked

	$('.handle').hide();
	$(".edit-button").bind("click", function() {
		$(this).hide();
		$('.handle').show();
		$('.done-button').show();
		$("#sortable").sortable({ 
			axis: 'y',
			handle: $('.handle'),
			containment: '#sort-section',
			update: function(event, ui){  
					  $('ul li').each(function(){
						$(this).find('span.badge').html($(this).index()+1);
						$(this).find('input.position').attr('value', $(this).index());
					  });                 
					}
		});
	});
	$(".done-button").bind("click", function() {
		$(this).hide();
		$(".edit-button").show();
		$(".handle").hide();
	});
	$("li.alternative").bind("click", function(){
		$("li.alternative").removeClass("active");
		$(this).addClass("active");
	});

    $('.props-table').dataTable( {
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": true,
        "bInfo": false,
        "bAutoWidth": true,
    	"bDestroy": true
    });

    $('.alt-table').dataTable( {
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": true,
        "bInfo": false,
        "bAutoWidth": false,
    	"bDestroy": true,
	    "aoColumnDefs": [
	      { "sWidth": "20px", "aTargets" : [ 0 ] }
	    ]
    });

    $.extend( $.fn.dataTableExt.oStdClasses, {
      "sSortAsc": "header headerSortDown",
      "sSortDesc": "header headerSortUp",
      "sSortable": "header"
    });

	$("#add-property").bind('click', function(event){
		event.preventDefault();
		var a = $("#add-new-form");
		$("#cp_new").hide();
		$("#cp_new").before("<div class='new_form'>"+a.html()+"</div>");
	});
	$(".del_prop_row").on("click", function() {
		var id = $(this).data("propid");
		$("#"+id).remove();
	});
});

