$(function() {
  $.getJSON( '/my_articles.json',
    function(data) {
      $('#my-articles-list-menu').tree({
        data: data,
        autoOpen: true,
        dragAndDrop: false,
        selectable: true,
        onCreateLi: function(node, $li) {
          $li.find('.title').before('<i class="icon-file"></i> ');
        }
      });
    });		
  $('#my-articles-list-menu').bind(
    'tree.click',
    function(event) {
      var node = event.node;
      window.location = "/articles/"+node.id;
    });
});
