$(document).on("ready pjax:end", function() {
    $('.social-icons').tooltip({
        selector: "img[rel=tooltip]",
        placement: "bottom",
        trigger: "hover"
    });
    $('nav').tooltip({
        selector: "li[rel=tooltip]",
        placement: "bottom",
        trigger: "hover"
    });
    $('nav-pills').tooltip({
        selector: "li[rel=tooltip]",
        placement: "right",
        trigger: "hover"
    });
    $('.avatars').tooltip({
        selector:   "img[rel=tooltip]",
        placement:  "bottom",
        trigger:  "hover"
    });

    /* Convenience for forms or links that return HTML from a remote ajax call.
    The returned markup will be inserted into the element id specified.
     */
    $('form[data-update-target]').live('ajax:success', function(evt, data) {
        var target = $(this).data('update-target');
        $('#' + target).html(data);
    });

    $("#toggle-hide").bind( "click", function() {
        $("td").find("i.icon-remove").parents("tr").toggle();
    });
    if ( $('#my-articles-list-menu')[0] ) { 
        $.getJSON( '/my_articles.json', function(data) {
            $('#my-articles-list-menu').tree({
                data: data,
                autoOpen: true,
                dragAndDrop: false,
                selectable: true,
                onCreateLi: function(node, $li) {
                  //$li.find('.title').before('<i class="icon-file"></i> ');
                  if (node.img) {
                    $li.find('.title').before('<img src='+node.img+' style="width:20px;height:20px"></i> ');
                  } else {
                    $li.find('.title').before('<i class="icon-file" style="width:20px"></i> ');
                  }
                }
            });
        });         
        $('#my-articles-list-menu').bind('tree.click', function(event) {
            var node = event.node;
            window.location = "/articles/"+node.id;
        });
    };

    $('.pop-over').popover({
        selector:  "span[rel=popover]",
        trigger:   "hover"
    });
    $('.pop-info').popover({
        selector:  "span[rel=popover]",
        trigger:   "hover"
    });
    
    // To disable click on disabled link
    $('a[disabled=disabled]').click(function(event){
        event.preventDefault(); // Prevent link from following its href
    });
});
function getURLParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
}
$(document).on("ready pjax:end", function() {
    $("#" + getURLParameter('fn')).addClass("active");
    $("li.alternative").live("click", function(){
        $("li.alternative").removeClass("active");
        $("#" + $(this).attr('id')).addClass("active");
    });
});
