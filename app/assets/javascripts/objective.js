function build_tree(article_id) {
	$.getJSON( '/articles/'+article_id+'/objectives.json',
    function(data) {
      $('#objective-tree').tree({
        data: [data],
        autoOpen: true,
        dragAndDrop: false,
        selectable: true,
        onCreateLi: function(node, $li) {
          if (node.children.length == 0) {
            $li.find('.title').before('<i class="icon-leaf"></i> ');
          } else {
            $li.find('.title').before('<i class="icon-th-list"></i> ');
          };
          if (node.weights_incomplete) {
            $li.find('.title').addClass('incomplete');
          }
        }
      });
    });		
  $('#objective-tree').bind(
    'tree.click',
    function(event) {
      var node = event.node;
      $("li.add_objective").slideDown();
      $("a#add_sub").attr("href", "/articles/"+article_id+"/objectives/"+node.id+"/new_sub");
      $.get("/articles/"+article_id+"/objectives/"+node.id+".js");

    });
};

$(function() {
	if ($('#objective-tree').length > 0) $('#objective-tree').live(build_tree($('#objective-tree').data('article')));
	if ($(".pairwise-slider").length > 0) $(".pairwise-slider").live(Avalon.Pairwise.build_sliders());
});
