function build_leaves(root_id) {
  $.getJSON( '/criteria/'+root_id+'/tree.json',
  function(data) {
    $('#criteria-leaves').tree({
      data: [data],
      autoOpen: true,
      dragAndDrop: false,
      selectable: true,
      onCreateLi: function(node, $li) {
        if (node.children.length == 0) {
          $li.find('.title').before('<i class="icon-leaf"></i> ');
        } else {
          $li.find('.title').before('<i class="icon-th-list"></i> ').addClass('unselectable-node');
        }
        if (node.ratings_incomplete) {
          $li.find('.title').addClass('incomplete');
        }
      },
      onCanSelectNode: function(node) {
        if (node.children.length == 0) {
          return true; 
        }
        else {
          return false;
        }
      }
    });
  });
  $('#criteria-leaves').bind(
    'tree.click',
    function(event) {
      var node = event.node;
      $.get('/criteria/'+node.id+'/ratings.js');
    }
  );
};

$(function() {
  if ($('#criteria-leaves').length > 0) $('#criteria-leaves').live(build_leaves($('#criteria-leaves').data('article')));
  if ($(".pairwise-slider").length > 0) $(".pairwise-slider").live(Avalon.Pairwise.build_sliders(".pairwise-slider"));

});