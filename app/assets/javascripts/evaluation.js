function build_leaves(article_id) {
  $.getJSON( '/articles/'+article_id+'/objectives.json',
  function(data) {
    $('#objective-leaves').tree({
      data: [data],
      autoOpen: true,
      dragAndDrop: false,
      selectable: true,
      onCreateLi: function(node, $li) {
        if (node.children.length == 0) {
          $li.find('.title').before('<i class="icon-leaf"></i> ');
        } else {
          $li.find('.title').before('<i class="icon-th-list"></i> ').addClass('unselectable-node');
        }
        if (node.evaluations_incomplete) {
          $li.find('.title').addClass('incomplete');
        }
      },
      onCanSelectNode: function(node) {
        if (node.children.length == 0) {
          return true; 
        }
        else {
          return false;
        }
      }
    });
  });
  $('#objective-leaves').bind(
    'tree.click',
    function(event) {
      var node = event.node;
      $.get('/articles/'+article_id+'/objectives/'+node.id+'/evaluations.js');
    }
  );
};

$(function() {
  if ($('#objective-leaves').length > 0) $('#objective-leaves').live(build_leaves($('#objective-leaves').data('article')));
  if ($(".pairwise-slider").length > 0) $(".pairwise-slider").live(Avalon.Pairwise.build_sliders(".pairwise-slider"));

  $('.objectives-table').dataTable( {
    "sDom": "<'row'<'span8'l><'span8'f>r>t<'row'<'span8'i><'span8'p>>",
    "bPaginate": false,
    "bLengthChange": false,
    "bFilter": false,
    "bSort": true,
    "bInfo": false,
  } );
  $.extend( $.fn.dataTableExt.oStdClasses, {
      "sSortAsc": "header headerSortDown",
      "sSortDesc": "header headerSortUp",
      "sSortable": "header"
  } );
});

