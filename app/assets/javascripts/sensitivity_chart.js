$(document).ready(function(){
	$.jqplot.config.enablePlugins = true;
	var data = #{ @sensitivity_chart_data };
	var chartLabels = #{{ @sensitivity_chart_labels }};
	var weight = #{ @weight };

	$("#sensitivity-chart").bind("jqplotClick", function(ev, gridpos, datapos, neighbor) {
		$('#point-info').html('Value: '+datapos.xaxis.toPrecision(3));
	});

	plot1 = $.jqplot('sensitivity-chart', data, {
		seriesDefaults: {
			pointLabels : {show: false}
		},
		axes: {
			xaxis: { pad: 0 },
			yaxis: { min: 0, tickInterval: 0.05 }
		},
		cursor: {
			showVerticalLine: true
		},
		canvasOverlay: {
			show: true,
			objects: [
				{verticalLine: {
					name: "line3",
					x: weight,
					color: "#d4c35D",
					ymin: 0,
					ymax: 1,
					shadow: false
				}}
			]
		},
		grid: {
				drawBorder: false,
				shadow: false
		},
		legend: {
				show: true,
				location: 'ne',
				placement: 'outside',
				labels: chartLabels
		}
	});

	var data = #{ @chart_data };
	var chartLabels = #{{ @alternative_labels }};
	var names = #{{ @alternative_names }};
	plot1 = $.jqplot('chart', [data], {
		// Only animate if we're not using excanvas (not in IE 7 or IE 8)..
		// animate: !$.jqplot.use_excanvas,
		seriesDefaults:{
			renderer:$.jqplot.BarRenderer,
			rendererOptions: {
				varyBarColor: true
			},
			pointLabels: {show: true}
		},
		series:[{ pointLabels:{ 
			show: true, 
			labels: chartLabels, 
			edgeTolerance:-25, 
			escapeHTML: false
		}}],
		axes: {
			xaxis: {
				renderer: $.jqplot.CategoryAxisRenderer,
				show: false,
				showGridline: false, edgeTolerance:-25, escapeHTML: false,
				tickOptions: { showGridline: false }
			},
			yaxis: {
				padMax: 1.2
			}
		},
		grid: {
			drawBorder: false,
			shadow: false
		},
		highlighter: {
			show: true,
			sizeAdjust: 7.5,
			tooltipLocation: 's',
			tooltipContentEditor: function(str, seriesIndex, pointIndex) {
				var objLabel = names[pointIndex];
				var val = data[pointIndex];
				return "<span style='font-weight:bold; font-size:14pt; color:#333'>"+val.toPrecision(2)+"</span>";
			}
		},
		cursor: {
			show: false
		}
	});
});

