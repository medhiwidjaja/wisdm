function build_leaves(root_id) {
  var $tree = $('#criteria-leaves');
  $.getJSON( '/criteria/'+root_id+"/tree.json?p="+$('#criteria-leaves').data('pid'),
  function(data) {
    $tree.tree({
      data: [data],
      autoOpen: true,
      dragAndDrop: false,
      selectable: true,
      onCreateLi: function(node, $li) {
        if (node.children.length == 0) {
          $li.find('.title').before('<i class="icon-leaf"></i> ');
        } else {
          $li.find('.title').before('<i class="icon-th-list"></i> ').addClass('unselectable-node');
        }
        if (node.ratings_incomplete) {
          $li.find('.title').addClass('incomplete');
        };
        $li.attr("data-id", node.id);
        // if (node.id == $tree.data('select'))
        //   $tree.tree('selectNode', node, true);
      },
      onCanSelectNode: function(node) {
        if (node.children.length == 0) {
          return true; 
        }
        else {
          return false;
        }
      }
    });
  });
  if($tree.data('allowClick')) {
    $tree.bind(
      'tree.click',
      function(event) {
        var node = event.node;
        $("a#summary").attr("href", "/criteria/"+node.id+"/ratings/aggregate_summary");
        $("a#detail").attr("href", "/criteria/"+node.id+"/ratings/aggregate_detail");
        var part = $("li.participant.active").attr("id");
        var url;
        if (part == "summary") {
          url = "/criteria/"+node.id+"/ratings/aggregate_summary";
        } 
        else if (part == "detail") {
          url = "/criteria/"+node.id+"/ratings/aggregate_detail";
        } else {
          url = "/criteria/"+node.id+"/ratings?p="+part;
        };
        $.pjax({
          url: url,
          container: "[data-pjax-container]"
        });
      }
    );
  };
};

$(function() {
  var $tree = $('#criteria-leaves');
  if ($tree.length > 0) $tree.live(build_leaves($tree.data('node')));
  var node = $tree.tree('getNodeById', $tree.data('select')) 
  $tree.tree('selectNode', node, true);
  //if ($(".pairwise-slider").length > 0) $(".pairwise-slider").live(Avalon.Pairwise.buildSliders(".pairwise-slider"));
  
  // Participant
  $("li.participant").bind("click", function(){
      $("li.participant").removeClass("active");
      $(this).addClass("active");
    });

  $("a.participant-anchor").bind("click", function(event){
    var criterionId;
    if ($("ul.tree").find(".selected").length == 0)
      criterionId = $tree.attr("data-node");
    else
      criterionId = $("ul.tree").find(".selected").attr("data-id");
    $.pjax({
      url: "/criteria/"+criterionId+"/ratings?p="+$(this).parent("li").attr('id'),
      container: "[data-pjax-container]"
    });
    event.preventDefault();
  })
});

// Sortable / draggable
function setup_draggable() {
  if ($("ul#sortable").children("li").length==0) {
    $("input:submit[name=commit]").attr("disabled", false);
    $("a.rank-first").attr("disabled", false);
    $("div.rank-first").hide();
  };
  $("li.rank-item").draggable({
    appendTo: ".widget",
    helper: "clone",
    cursor: "move",
    revert: "invalid",
    create: function() {
      $(this).find("strong").before("<i class='icon-reorder'></i>")
    }
  });
};

$(document).on("ready pjax:end", function() {
  $('.objectives-table').dataTable( {
    "bPaginate": false,
    "bLengthChange": false,
    "bFilter": false,
    "bSort": true,
    "bInfo": false,
    "bAutoWidth": false,
    "bDestroy": true,
    "aoColumnDefs": [
      { "sWidth": "28px", "aTargets" : [ 0 ] }
    ]
  } );
  $.extend( $.fn.dataTableExt.oStdClasses, {
    "sSortAsc": "header headerSortDown",
    "sSortDesc": "header headerSortUp",
    "sSortable": "header"
  });

  setup_draggable();

  $("#ranks div.thumbnail ul").droppable({
    accept: "li.rank-item",
    activeClass: "ui-state-highlight",
    hoverClass: "ui-state-hover",
    drop: function(event, ui) {
      $(this).find( ".placeholder" ).remove();
      $(ui.draggable).parent("ul.droppable").has("li:only-child").append("<li class='placeholder'>&nbsp;</li>");
      ui.draggable.appendTo(this).draggable();
      $(this).find("input.order").attr("value", $(this).data("rank"));
      if ($("ul#sortable").children("li").length==0) {
        $("input:submit[name=commit]").attr("disabled", false);
        $("div.rank-first").hide();
        $("a.rank-first").attr("disabled", false);
      } else
        $("input:submit[name=commit]").attr("disabled", true);
    }
  });

  // Pairwise comparison
  disable = $('#pairwise-comparison').data("disable")=='yes';  
  $('#level-scale-5').click(function(event){ Avalon.Pairwise.useScale('level', disable) });
  $('#level-scale-9').click(function(event){ Avalon.Pairwise.useScale('level-9', disable) });
  $('#numeric-scale').click(function(event){ Avalon.Pairwise.useScale('numeric', disable) });
  $('#free-scale').click(function(event){ Avalon.Pairwise.useScale('free', disable) });
  Avalon.Pairwise.useScale($("#ahp-scale").attr("value") || "level", disable); // the default
  $(Avalon.Pairwise.sliderDivClass).each(function(){ 
    if ($(this).data("value")!=undefined && $(this).data("value")!=0) { 
      $(this).slider("value", $(this).data("value")) 
    }
  });
});

