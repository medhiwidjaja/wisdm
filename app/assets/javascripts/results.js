function build_tree(root_id) {
  $(function() {
      $.getJSON( '/criteria/'+root_id+'/tree.json',
      function(data) {
        $('#analyses-tree').tree({
          data: [data],
          autoOpen: true,
          dragAndDrop: false,
          selectable: true,
          onCreateLi: function(node, $li) {
            if (node.children.length == 0) {
              $li.find('.title').before('<i class="icon-leaf"></i> ');
            } else {
              $li.find('.title').before('<i class="icon-th-list"></i> ');
            }
          }
        });
      });

    });
};

$(function() {
	var $tree = $('#analyses-tree');
  if ($tree.length > 0) $tree.live(build_tree($tree.data('node')));
  $tree.unbind('tree.click');
  $tree.bind(
    'tree.click',
    function(event) {
      var node = event.node;
      var group_tag = $tree.data('group') ? "&g=a" : ""
      if (! node.parent.parent) {
        $.pjax({
          //url: "/articles/"+$tree.data('article')+"/results?p="+$tree.data('pid'),
          url: "/articles/"+$tree.data('article')+"/results?p="+$("li.participant.active").attr("id")+group_tag,
          container: "[data-pjax-container]"
        });
      } else {
        $.pjax({
          url: "/criteria/"+node.id+"/results/rank?p="+$("li.participant.active").attr("id")+group_tag,
          container: "[data-pjax-container]"
        });
      }
    }
  );

  // Participant
  $("li.participant").bind("click", function(){
      $("li.participant").removeClass("active");
      $(this).addClass("active");
    });

  $("a.participant-anchor").bind("click", function(event){
    var articleId = $("ul.part-list").data("article");
    var criterionId;
    if ($("ul.tree").find(".selected").length == 0)
      criterionId = $tree.attr("data-node");
    else
      criterionId = $("ul.tree").find(".selected").attr("data-id");
    $.pjax({
      url: "/articles/"+articleId+"/results?p="+$(this).parent("li").attr('id'),
      container: "[data-pjax-container]"
    });
    event.preventDefault();
  })

  // Table
  $('.rank-table').dataTable( {
    "bPaginate": false,
    "bLengthChange": false,
    "bFilter": false,
    "bSort": true,
    "bInfo": false,
    "bAutoWidth": false,
    "bDestroy": true,
    "aoColumnDefs": [
      { "sWidth": "28px", "aTargets" : [ 0 ] },
      { "sWidth": "50%", "aTargets" : [ 1 ] }
    ]
  } );
});

