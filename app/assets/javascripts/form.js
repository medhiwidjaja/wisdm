$(document).on("ready pjax:success", function() {
	$(".wysihtml5").each(function(i, elem) {
		$(elem).wysihtml5();
	});
	$('form a.add_child').click(function() {
		var association = $(this).attr('data-association');
		var row = $(this).attr('data-row');
		var template = $('#' + association + '_fields_template').find('.' + row).parent().html();
		var regexp = new RegExp('new_' + association, 'g');
		var new_id = new Date().getTime();

		$(this).prev("table").find("tr:last").after(template.replace(regexp, new_id));
		return false;
	});

	$('form a.remove_child').live('click', function() {
		var hidden_field = $(this).prev('input[type=hidden]')[0];
		if(hidden_field) {
			hidden_field.value = 'true';
		}
		if ($(this).hasClass("new_record")) {
			$(this).parents('.property-row').remove();
		}
		else {
			$(this).parents('.property-row').hide();
		}
		return false;
	});
});