// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery-1.8.3.min
//= require jquery_ujs
//= require jquery.pjax
//= require ./enable_pjax
//= require jquery-ui-1.9.2.custom.min
//= require ui/jquery.slider-ext
//= require twitter/bootstrap
//= require ./base
//= require tree.jquery
//= require datatables/jquery.dataTables
//= require datatables/paging
//= require jqplot/jquery.jqplot
//= require d3/d3.v2.min
//= require d3/sankey
//= require jqplot/plugins/jqplot.barRenderer.min
//= require jqplot/plugins/jqplot.categoryAxisRenderer.min
//= require jqplot/plugins/jqplot.cursor.min
//= require jqplot/plugins/jqplot.pointLabels.min
//= require jqplot/plugins/jqplot.canvasOverlay.min
//= require jqplot/plugins/jqplot.highlighter.min
//= require jqplot/plugins/jqplot.canvasAxisLabelRenderer.min
//= require jqplot/plugins/jqplot.canvasTextRenderer.min
//= require bootstrap-wysihtml5
//= require jquery.cookie
//= require jquery.event.drag-2.2/jquery.event.drag-2.2
//= require jquery.event.drag-2.2/jquery.event.drag.live-2.2
//= require jquery.event.drop-2.2/jquery.event.drop-2.2
//= require jquery.event.drop-2.2/jquery.event.drop.live-2.2
//= require touch-punch/jquery.ui.touch-punch
//= require ui/pairwise_slider.js
//= require ./form
//= require jquery.dotdotdot-1.5.4-packed