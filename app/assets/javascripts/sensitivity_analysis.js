function build_tree(article_id) {
  $(function() {
      $.getJSON( '/articles/'+article_id+'/objectives.json',
      function(data) {
        $('#analyses-tree').tree({
          data: [data],
          autoOpen: true,
          dragAndDrop: false,
          selectable: true,
          onCanSelectNode: function(node) {
              if ( ! node.parent.parent ) {
                  // Root node can't be selected
                  return false;
              }
              else {
                  // Other nodes can be selected
                  return true;
              }
          },
          onCreateLi: function(node, $li) {
            if ( ! node.parent.parent ) {
                // Root node can't be selected
                $li.find('.title').addClass('unselectable-node');
            };
            if (node.children.length == 0) {
              $li.find('.title').before('<i class="icon-leaf"></i> ');
            } else {
              $li.find('.title').before('<i class="icon-th-list"></i> ');
            }
          }
        });
      });

    });
};

$(function() {
	if ($('#analyses-tree').length > 0) $('#analyses-tree').live(build_tree($('#analyses-tree').data('article')));
});