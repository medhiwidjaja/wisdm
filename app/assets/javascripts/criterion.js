function build_tree(root_id) {
  $.getJSON( '/criteria/'+root_id+"/tree.json?p="+$('#criteria-tree').data('pid'),
    function(data) {
      $('#criteria-tree').tree({
        data: [data],
        autoOpen: true,
        dragAndDrop: true,
        selectable: true,
        onCreateLi: function(node, $li) {
          if (node.children.length == 0) {
            $li.find('.title').before('<i class="icon-leaf"></i> ');
          } else {
            if (node.parent.parent)
              $li.find('.title').before('<i class="icon-th-list"></i> ');
            else
              $li.find('.title').before('<i class="icon-screenshot"></i> ');
          };
          if (node.weights_incomplete) {
            $li.attr('data-incomplete', true)
            $li.find('.title').addClass('incomplete');
          };
          if (node.parent.parent) {
            $li.find('.title').after('<i class="icon-reorder pull-right handle"></i>');
          };
          $li.attr("id", node.id);
          $li.attr("data-id", node.id);
        },
        onIsMoveHandle: function($element) {
          return ($element.is('.handle'));
        },
        onCanMove: function(moved_node) {
          if (! moved_node.parent.parent) {
            return false;  // can't move root node
          }
          else {
            return true;
          }
        },
        onCanMoveTo: function(moved_node, target_node, position) {
          if (!target_node.parent.parent) {
            if (position == 'inside')
              return true;  
            else
              return false;   // can't move to root node
          }
          else {
            return true;
          }
        }
      });
      $('.handle').hide();
    });	
  if($('#criteria-tree').data('allowClick')) {
    $('#criteria-tree').bind(
    'tree.click',
    function(event) {
      var node = event.node;
      $("#add-criterion").slideDown();
      $("a#add-sub").attr("href", "/criteria/"+node.id+"/new_sub");
      $("a#summary").attr("href", "/criteria/"+node.id+"/aggregate_summary");
      $("a#detail").attr("href", "/criteria/"+node.id+"/aggregate_detail");
      var part = $("li.participant.active").attr("id");
      var url;
      if (part == "summary") {
        url = "/criteria/"+node.id+"/aggregate_summary";
      } 
      else if (part == "detail") {
        url = "/criteria/"+node.id+"/aggregate_detail";
      } 
      else {
        url = "/criteria/"+node.id+"?p="+part;
      };
      $.pjax({
        url: url,
        container: "[data-pjax-container]"
      });
    });
    $('#criteria-tree').bind(
    'tree.move',
    function(event) {
      var moved_node = event.move_info.moved_node,
          target_node = event.move_info.target_node,
          position = event.move_info.position,
          previous_parent = event.move_info.previous_parent;
      $.post("/criteria/"+moved_node.id+"/move",
        {id:moved_node.id, to:target_node.id, position: position, pid: previous_parent.id}  
      );
    });
  };
  $(".edit-button").bind("click", function() {
    if (confirm('Moving a criterion to a different branch will erase any existing comparisons related to the originating and the target branch. \n\nProceed?')) {
      $(this).hide();
      $('.handle').show();
      $('.done-button').show();
    };
  });
  $(".done-button").bind("click", function() {
    $(this).hide();
    $(".edit-button").show();
    $(".handle").hide();
    build_tree($('#criteria-tree').data('node'));
  });
};

$(function() {
  // Tree
  if ($('#criteria-tree').length > 0) $('#criteria-tree').live(build_tree($('#criteria-tree').data('node')));

  // Participant
  $("li.participant").bind("click", function(){
      $("li.participant").removeClass("active");
      $(this).addClass("active");
    });

  $("a.participant-anchor").bind("click", function(event){
    var criterionId;
    if ($("ul.tree").find(".selected").length == 0)
      criterionId = $("#criteria-tree").attr("data-node");
    else
      criterionId = $("ul.tree").find(".selected").attr("id");
    $.pjax({
      url: "/criteria/"+criterionId+"?p="+$(this).parent("li").attr('id'),
      container: "[data-pjax-container]"
    });
    event.preventDefault();
  })

  // Rank table / form
  // var check=function() {
  //   if($('select#comparison_method').val() != ''){
  //     $('input[type="submit"]').removeAttr('disabled');
  //     $('input#set_common').removeAttr('disabled');
  //   } else {
  //     $('input#compare').attr('disabled','disabled');
  //     $('input#set_common').attr('disabled','disabled');
  //   }
  // };
  // check();
  // $('select#comparison_method').change(function(){
  //   check();
  // });
});

$(document).on("ready pjax:end", function() {
  // Table setup
  $('.objectives-table').dataTable( {
    "bPaginate": false,
    "bLengthChange": false,
    "bFilter": false,
    "bSort": true,
    "bInfo": false,
    "bAutoWidth": false,
    "bDestroy": true,
    "aoColumnDefs": [
      { "sWidth": "28px", "aTargets" : [ 0 ] }
    ]
  } );
  
  // Sortable / draggable
//function setup_draggable() {
  if ($("ul#sortable").children("li").length==0) {
    $("input:submit[name=commit]").attr("disabled", false);
    $("a.rank-first").attr("disabled", false);
    $("div.rank-first").hide();
  };
  $("li.rank-item").draggable({
    appendTo: ".widget",
    helper: "clone",
    cursor: "move",
    revert: "invalid",
    create: function() {
      $(this).find("strong").before("<i class='icon-reorder'></i>")
    }
  });

  $("#ranks div.thumbnail ul").droppable({
    accept: "li.rank-item",
    activeClass: "ui-state-highlight",
    hoverClass: "ui-state-hover",
    drop: function(event, ui) {
      $(this).find( ".placeholder" ).remove();
      $(ui.draggable).parent("ul.droppable").has("li:only-child").append("<li class='placeholder'>&nbsp;</li>");
      ui.draggable.appendTo(this).draggable();
      $(this).find("input.order").attr("value", $(this).data("rank"));
      if ($("ul#sortable").children("li").length==0) {
        $("input:submit[name=commit]").attr("disabled", false);
        $("div.rank-first").hide();
        $("a.rank-first").attr("disabled", false);
      } else
        $("input:submit[name=commit]").attr("disabled", true);
    }
  });
});

$(document).on("ready pjax:end", function() {
  disable = $('#pairwise-comparison').data("disable")=='yes';
  $('#importance-scale-5').click(function(event){ Avalon.Pairwise.useScale('importance', disable) });
  $('#importance-scale-9').click(function(event){ Avalon.Pairwise.useScale('importance-9', disable) });
  $('#numeric-scale').click(function(event){ Avalon.Pairwise.useScale('numeric', disable) });
  $('#free-scale').click(function(event){ Avalon.Pairwise.useScale('free', disable) });
  Avalon.Pairwise.useScale($("#ahp-scale").attr("value") || "importance", disable); // the default
  $(Avalon.Pairwise.sliderDivClass).each(function(){ 
    if ($(this).data("value")!=undefined && $(this).data("value")!=0) { 
      $(this).slider("value", $(this).data("value")) 
    }
  });

});


// jQuery(function($){
//    $('.drag')
//       .drag("start",function( ev, dd ){
//          $( dd.available ).addClass("available");
//       })
//       .drag(function( ev, dd ){
//          $( this ).css({
//             top: dd.offsetY,
//             left: dd.offsetX
//          });
//       })
//       .drag("end",function( ev, dd ){
//          $( dd.available ).removeClass("available");
//       });
//    $('.drop')
//       .drop(function(){
//          $( this ).toggleClass("dropped");
//       });
// });

// $(document).on("ready pjax:end", function() {
//   if ($("ul#sortable").children("li").length==0) {
//     $("input:submit[name=commit]").attr("disabled", false);
//     $("a.rank-first").attr("disabled", false);
//     $("div.rank-first").hide();
//   };
//   $(".rank-item").drag(function(ev,dd) {

//   });
//   $("#ranks div.thumbnail ul")
//     .drop("start",function(){
//          $( this ).addClass("active");
//     })
//     .drop("end",function(){
//        $( this ).removeClass("active");
//     });
//     // drop: function(event, ui) {
//     //   $(this).find( ".placeholder" ).remove();
//     //   $(ui.draggable).parent("ul.droppable").has("li:only-child").append("<li class='placeholder'>&nbsp;</li>");
//     //   ui.draggable.appendTo(this);
//     //   $(this).find("input.order").attr("value", $(this).data("rank"));
      
//     //   if ($("ul#sortable").children("li").length==0) {
//     //     $("input:submit[name=commit]").attr("disabled", false);
//     //     $("div.rank-first").hide();
//     //     $("a.rank-first").attr("disabled", false);
//     //   } else
//     //     $("input:submit[name=commit]").attr("disabled", true);
//     // }



// });