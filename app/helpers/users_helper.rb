# Copyright (c) 2012 Medhi Widjaja

module UsersHelper
  # Returns the Gravatar (http://gravatar.com) for the given user.
  def gravatar_for(user, options = { size: 50 })
    size = options[:size]
    gravatar_url = user.gravatar_url options
    image_tag gravatar_url, alt: "#{user.name}", 
      width: size, height: size, class: "gravatar"
  end

  # def gravatar_url_for(user, options = { size: 50 })
  #   gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
  #   size = options[:size]
  #   "http://gravatar.com/avatar/#{gravatar_id}.png?s=#{size}"
  # end

end