# Copyright (c) 2012 Medhi Widjaja

module AlternativesHelper
  def add_new_property_link(form_builder, alternative)
    #prop = Property.new
    prop = alternative.properties.new
    form_builder.fields_for :properties, prop, :child_index => 'STUB' do |f|
      html = render(partial: 'alternatives/property', locals: { form: f })

      link_to_function 'Add new property', "add_fields(this, \"#{escape_javascript(html)}\")", { id:'add_property_row', class: 'btn btn-mini btn-warning' } 
      #link_to 'Add a property', "#", { id:'add_property_row', class: 'btn btn-mini btn-warning' } 
      # See: assets/javascripts/alternative.js
    end
  end

  # def add_new_property_link(form_builder, alternative)
  #   prop = alternative.properties.new
  #   html = render(partial: 'property', locals: { form: form_builder, property_id: prop.id.to_s })

  #   link_to_function 'Add a property', "add_fields(this, \"#{escape_javascript(html)}\")", { id:'add_property_row', class: 'btn btn-mini btn-warning' } 
  #   # See: assets/javascripts/alternative.js

  # end

  def add_common_properties_link(form_builder, article)
    html = ""
    article.common_properties.each do |property|
      prop = Property.new(k: property)
      form_builder.fields_for :properties, prop, :child_index => prop.id.to_s do |f|
        html += render(partial: 'alternatives/property', locals: { form: f })
      end
    end
    link_to_function 'Add common properties', "add_fields(this, \"#{escape_javascript(html)}\");$(this).remove()", { id:'add_common_properties', class: 'btn btn-mini btn-warning' } 
    # See: assets/javascripts/alternative.js
  end

  def remove_property_link(form_builder)
    if form_builder.object.new_record?
      # If the property is a new record, we can just remove the div from the dom
      link_to_function('Remove'.html_safe,
        "$(this).parents('.property_row').remove();",
        {class: 'btn btn-mini btn-danger'});
    else
      # However if it's a "real" record it has to be deleted from the database,
      # for this reason the new fields_for, accept_nested_attributes helpers give us _delete,
      # a virtual attribute that tells rails to delete the child record.
      form_builder.hidden_field(:_destroy) +
              link_to_function('Remove'.html_safe,
               "remove_fields(this)", {class: 'btn btn-mini btn-danger'})
      # See: assets/javascripts/alternative.js
    end
  end

end
