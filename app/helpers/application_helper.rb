# Copyright (c) 2012 Medhi Widjaja

module ApplicationHelper
  def logo
    image_tag("foreplot.png", alt: "FOREPLOT")
  end

  def wrap(text)
    sanitize(raw(text.split.map{ |s| wrap_long_string(s) }.join(' ')))
  end

  # For subnavbar menu
  def link_status(link, options={ :disabled => false })
    status = session[:link] == link ? 'active' : ''
    status += options[:disabled] ? ' disable' : ''
  end

  # For error messages in alert
  def inflectize(n, noun)
    if n == 1
      noun
    else
      noun.pluralize
    end
  end

  def find_criterion(id)
    begin
      Criterion.find(id)
    rescue
      nil
    end
  end

  def find_alternative(id)
    begin
      Alternative.find(id)
    rescue
      nil
    end
  end

  def description_snippet(description, len=100)
    unless description.blank?
      doc = Nokogiri::HTML::DocumentFragment.parse description
      doc.css("h1,h2,h3,h4,h5,h6").each {|h| h.name = 'h5'}
      doc.css('img').remove
      doc.css('br').remove
      #doc.to_html.truncate(len)
      doc.to_html
    end
  end

  def pic_snippet(description)
    unless description.blank?
      pic_node = Nokogiri::HTML(description).at_css('img')
      pic_node[:src] unless pic_node.nil?
    end
  end

  def store_location
    session[:return_to] = request.request_uri
  end

  def redirect_back
    redirect_to(session[:return_to])
  end

  def save_referer
    unless is_logged_in?
      unless session['referer']
        session['referer'] = request.env["HTTP_REFERER"] || 'none'
      end
    end
  end

  def redirect_to_referer
    redirect_to(session[:referer])
  end

  # For dynamic form fields
  # See this article http://stackoverflow.com/questions/1704142/unobtrusive-dynamic-form-fields-in-rails-with-jquery
  # See also form.js, 
  def new_child_fields_template(form_builder, association, options = {})
    options[:object] ||= form_builder.object.class.reflect_on_association(association).klass.new
    options[:partial] ||= association.to_s.singularize
    options[:form_builder_local] ||= :f

    content_for :jstemplates do
      content_tag(:div, :id => "#{association}_fields_template", :style => "display: none") do
        form_builder.fields_for(association, options[:object], :child_index => "new_#{association}") do |f|        
          content_tag(:table) do
            render(:partial => options[:partial], :locals => { options[:form_builder_local] => f })
          end    
        end
      end
    end
  end

  # For dynamic form fields
  def add_child_link(name, association, options = {})
    link_to(name, "javascript:void(0)", 
      {:class => "add_child", :"data-association" => association, :"data-row" => "#{association.to_s.singularize}-row"}
        .merge(options) { |key, old, new|
          old + ' ' + new
        }
    )
  end

  # For dynamic form fields
  def remove_child_link(name, f, options = {})
    css_class = "remove_child"
    css_class += " new_record" if f.object.new_record? 
    f.hidden_field(:_destroy) + link_to(name, "javascript:void(0)", 
      {:class => css_class}.merge(options) { |key, old, new|
          old + ' ' + new
        }
    )
  end

  # For comments
  def comments_url(commentable)
    "/#{commentable.class.to_s.downcase.pluralize}/#{commentable.id.to_s}/comments"
  end

  def comment_update_url(comment)
    "/#{comment.commentable.class.to_s.downcase.pluralize}/#{comment.commentable.id.to_s}/comments/#{comment.id.to_s}"
  end

  def new_reply_url_for(comment)
    "/#{comment.commentable.class.to_s.downcase.pluralize}/#{comment.commentable.id.to_s}/comments/#{comment.id.to_s}/new_reply"
  end

  def reply_url_for(comment)
    "/#{comment.commentable.class.to_s.downcase.pluralize}/#{comment.commentable.id.to_s}/comments/#{comment.parent_id}/reply"
  end

  def edit_url_for(comment)
    "/#{comment.commentable.class.to_s.downcase.pluralize}/#{comment.commentable.id.to_s}/comments/#{comment.id.to_s}/edit"
  end

  # For Toolbar in Criteria and Ratings
  def link_to_comparison_method(method, criterion_id, participation_id, options={}, readonly=false)
    label = readonly ? 'View comparison' : Evaluation::method_verb(method)
    class_option = options[:class]
    skip_option = options['data-skip']
    case method
    when Evaluation::EVALUATION_METHODS['MAGIQ']
      link_to label, rank_criterion_evaluations_path(criterion_id)+"?p=#{participation_id}", class: class_option, 'data-skip' => skip_option
    when Evaluation::EVALUATION_METHODS['SMART']
      link_to label, smart_criterion_evaluations_path(criterion_id)+"?p=#{participation_id}", class: class_option, 'data-skip' => skip_option
    when Evaluation::EVALUATION_METHODS['Pairwise']
      link_to label, pairwise_criterion_evaluations_path(criterion_id)+"?p=#{participation_id}", class: class_option, 'data-skip' => skip_option
    end
  end

  # For layout
  def has_left_frame?
    @has_left_frame.nil? ? 
      true : # <= default, change to preference
      @has_left_frame
  end

  def has_right_frame?
    @has_right_frame.nil? ? 
      true : # <= default, change to preference
      @has_right_frame
  end

  # This will make will_paginate work with Twitter Bootstrap
  class BootstrapLinkRenderer < ::WillPaginate::ActionView::LinkRenderer
    protected

    def html_container(html)
      tag :div, tag(:ul, html), container_attributes
    end

    def page_number(page)
      tag :li, link(page, page, :rel => rel_value(page)), 
          :class => ('active' if page == current_page)
    end

    def gap
      tag :li, link(super, '#'), :class => 'disabled'
    end

    def previous_or_next_page(page, text, classname)
      tag :li, link(text, page || '#'), 
          :class => [classname[0..3], classname, ('disabled' unless page)].join(' ')
    end
  end

  # This will make will_paginate work with Twitter Bootstrap
  def page_navigation_links(pages, param=:page)
    will_paginate(pages, :class => 'pagination', 
                         :inner_window => 2, 
                         :outer_window => 0,
                         :param_name => param,    # Needed for pages with multiple models
                         :renderer => BootstrapLinkRenderer, 
                         :previous_label => '&larr;'.html_safe, 
                         :next_label => '&rarr;'.html_safe)
  end

  private

  def wrap_long_string(text, max_width = 30)
    zero_width_space = "&#8203;"
    regex = /.{1,#{max_width}}/
    (text.length < max_width) ? text :
                                text.scan(regex).join(zero_width_space)
  end
end
