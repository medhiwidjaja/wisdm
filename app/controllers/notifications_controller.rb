# Copyright (c) 2012 Medhi Widjaja

class NotificationsController < ApplicationController
  before_filter :disable_right_frame!, :enable_left_frame!

  def show
    @notification = Notification.find(params[:id])
    authorize! :show, @notification
    @article = Article.find(@notification.article_id)
  end

  def dismiss
    @notification = Notification.find(params[:id])
    authorize! :edit, @notification
    previous_page = params[:page]
    @notification.update_attributes state: 'complete', 
                                   accepted_by: current_user.id, 
                                   accepted_at: Time.now.utc
  end
end