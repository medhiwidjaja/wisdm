# Copyright (c) 2012 Medhi Widjaja

class ParticipationsController < ApplicationController
  before_filter :disable_right_frame!, :enable_left_frame!
  before_filter :authenticate_user!
  
  def index
    @article = Article.find(params[:article_id])
    authorize! :read, @article
    @participations = @article.participations
    respond_to do |format|
      format.html
      format.json
    end
  end

  def invite
    @article = Article.find(params[:article_id])
    @participations = @article.participations
    @participation = @article.participations.build
    authorize! :create, @participation
  end

  def send_invitation
    article = Article.find(params[:article_id]) 
    authorize! :create, article.participations.new
    list = params[:invitees]
    # message = params[:message]
    list.each do |row|
      id = row.first
      invitee = User.find(id)
      email = row.last[:email]
      name = row.last[:name]
      participation = article.participations.build(user_id: id, username: name, invited:true)
      participation.save!
      invitee.notifications.push Notification.new_notice invitee, participation, 'invite', current_user, article.title
    end
    redirect_to article_participations_path(article.slug), notice: 'The invitees have been notified.'
  end

  def new_request
    @article = Article.find(params[:article_id])
    @participations = @article.participations.all
    @participation = @article.participations.build permissions:['compare', 'rate', 'view']
    authorize! :request, @participation
    @notification = Notification.new
  end

  def send_request
    article = Article.find(params[:article_id])
    participation = article.participations.build(params[:participation])
    authorize! :request, participation
    body = params[:notification][:body]
    if participation.save
#      participation.reload
      redirect_to article_participations_path(article.slug), notice: 'Request has been sent.'
      article.user.notifications.push Notification.new_notice article.user, participation, 'request', current_user, article.title, body
    else
      render action: :request
    end
  end

  def approve
    article = Article.find(params[:article_id])
    participation = article.participations.find(params[:id])
    authorize! :approve, participation
    notification = Notification.find(params[:notification])
    sender = User.find(notification.sender_id)
    attributes = params[:participation] || {}
    case params[:commit]
    when 'Approve'
      participation.attributes = attributes.merge({ "approved" => true, "active" => true })
      participation.save!
      sender.notifications.push Notification.new_notice sender, participation, 'approve', current_user, article.title
      flash[:notice] = "You have approved #{sender.name} to join in the article"
    when 'Decline'
      participation.attributes = attributes.merge({ "rejected" => true })
      participation.save!
      sender.notifications.push Notification.new_notice sender, participation, 'reject', current_user, article.title
      flash[:notice] = "You have rejected #{sender.name} to join in the article"
    end
    notification.update_attributes state: 'complete', accepted_by: current_user.id, accepted_at: Time.now.utc
    redirect_to article_participations_path(article.slug) 
    # TODO: Handle this with AJAX, similar to Notification#dismiss
  end

  def confirm
    article = Article.find(params[:article_id])
    participation = article.participations.find(params[:id])
    authorize! :confirm, participation
    unless params[:notification].blank?
      notification = Notification.find(params[:notification])
      sender = User.find(notification.sender_id)
    end
    case params[:commit]
    when 'Confirm'
      participation.update_attributes! confirmed: true, active: true
      unless params[:notification].empty? || params[:notification].nil?
        sender.notifications.push Notification.new_notice sender, participation, 'confirm', current_user, article.title
      end
      flash[:notice] = 'You are now participating in this article.'
    when 'Decline'
      participation.update_attributes! declined: true
      unless params[:notification].empty? || params[:notification].nil?
        sender.notifications.push Notification.new_notice sender, participation, 'decline', current_user, article.title
      end
      flash[:notice] = 'You have declined to participate in this article.'
    end
    unless params[:notification].blank?
      notification.update_attributes state: 'complete', accepted_by: current_user.id, accepted_at: Time.now.utc
    end
    redirect_to article_participations_path(article.slug)
    # TODO: Handle this with AJAX, similar to Notification#dismiss
  end

  def edit
    @article = Article.find(params[:article_id])
    @participations = @article.participations
    @participation = @participations.find(params[:id])
    authorize! :update, @participation
  end

  def show
    @article = Article.find(params[:article_id])
    @participations = @article.participations
    @participation = @participations.find(params[:id])
    # authorize! :read, @participation
    respond_to do |format|
      format.html
      format.json { render json: @participation }
    end 
  end

  def update
    @article = Article.find(params[:article_id])
    @participation = @article. participations.find(params[:id])
    authorize! :update, @participation
    respond_to do |format|
      if @participation.update_attributes(params[:participation])
        format.html { redirect_to article_participations_path(@article.slug), notice: 'Participation was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @participation.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    participation = Participation.find(params[:id])
    authorize! :delete, participation
    article_id = participation.article.slug
    participation.destroy
    
    respond_to do |format|
      format.html { redirect_to article_participations_path(article_id) }
      format.json { head :no_content }
    end
  end

  def assign_weights
    @participation = Participation.find(params[:id])
    authorize! :update, @participation
  end

  def update_weights
    participation = Participation.find(params[:id])
    authorize! :update, participation
  end

end