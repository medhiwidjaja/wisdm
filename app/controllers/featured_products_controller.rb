# Copyright (c) 2012 Medhi Widjaja

class FeaturedProductsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :disable_right_frame!, :enable_left_frame!

  load_and_authorize_resource

  def index
    if params[:expired] == "1"
      @featured_products = FeaturedProduct.expired.desc(:expire_date).paginate(page: params[:page])
    else
      @featured_products = FeaturedProduct.current.desc(:expire_date).paginate(page: params[:page])
    end
  end

  def show
    #@featured_product = FeaturedProduct.find(params[:id])
    @product = @featured_product.product
  end

  def new
    @products = Product.public.desc(:updated_at).map{|a| [a.title, a.id] }

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @featured_product }
    end
  end

  def edit
    #@featured_product = FeaturedProduct.find(params[:id])
    @product = @featured_product.product
    @products = Product.public.desc(:updated_at).map{|a| [a.title, a.id] }
  end

  def create
    #@featured_product = FeaturedProduct.create(params[:featured_product])

    respond_to do |format|
      if @featured_product.save
        format.html { redirect_to manage_featured_products_path, notice: 'Feature product was successfully created.' }
        format.json { render json: manage_featured_product_path(@featured_product), status: :created, location: @featured_product }
      else
        format.html { render action: "new" }
        format.json { render json: @featured_product.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    #@featured_product = FeaturedProduct.find(params[:id])

    respond_to do |format|
      if @featured_product.update_attributes(params[:featured_product])
        format.html { redirect_to manage_featured_product_path(@featured_product), notice: 'Feature product was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @featured_product.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    #@featured_product = FeaturedProduct.find(params[:id])
    @featured_product.destroy

    respond_to do |format|
      format.html { redirect_to manage_featured_products_path }
      format.json { head :no_content }
    end
  end

end