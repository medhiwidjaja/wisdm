# Copyright (c) 2012 Medhi Widjaja

class WatchListsController < ApplicationController
  before_filter :authenticate_user!

  def create
    @article = Article.find(params[:article_id])
    @user = current_user
    unless current_user == @article.user
      @user.watch! @article
      respond_to do |format|
        format.html { redirect_to @article }
        format.js
      end
    end
  end

  def destroy
    @article = Article.find(params[:article_id])
    @user = current_user
    unless current_user == @article.user
      @user.unwatch! @article
      respond_to do |format|
        format.html { redirect_to @article }
        format.js
      end
    end
  end
end