# Copyright (c) 2012 Medhi Widjaja

class FeaturedArticlesController < ApplicationController
  before_filter :authenticate_user!
  before_filter :disable_right_frame!, :enable_left_frame!

  load_and_authorize_resource

  def index
    if params[:expired] == "1"
      @featured_articles = FeaturedArticle.expired.desc(:expire_date).paginate(page: params[:page])
    else
      @featured_articles = FeaturedArticle.current.desc(:expire_date).paginate(page: params[:page])
    end
  end

  def show
    #@featured_article = FeaturedArticle.find(params[:id])
    @article = @featured_article.article
  end

  def new
    #@featured_article = FeaturedArticle.new
    # @articles = Article.asc(:_id).paginate(page: params[:page])
    @articles = Article.public.desc(:updated_at).map{|a| [a.title, a.id] }

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @featured_article }
    end
  end

  def edit
    #@featured_article = FeaturedArticle.find(params[:id])
    @article = @featured_article.article
    @articles = Article.public.desc(:updated_at).map{|a| [a.title, a.id] }
  end

  def create
    #@featured_article = FeaturedArticle.create(params[:featured_article])

    respond_to do |format|
      if @featured_article.save
        format.html { redirect_to manage_featured_articles_path, notice: 'Featured article was successfully created.' }
        format.json { render json: manage_featured_article_path(@featured_article), status: :created, location: @featured_article }
      else
        format.html { render action: "new" }
        format.json { render json: @featured_article.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    #@featured_article = FeaturedArticle.find(params[:id])

    respond_to do |format|
      if @featured_article.update_attributes(params[:featured_article])
        format.html { redirect_to manage_featured_article_path(@featured_article), notice: 'Feature article was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @featured_article.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    #@featured_article = FeaturedArticle.find(params[:id])
    @featured_article.destroy

    respond_to do |format|
      format.html { redirect_to manage_featured_articles_path }
      format.json { head :no_content }
    end
  end

end