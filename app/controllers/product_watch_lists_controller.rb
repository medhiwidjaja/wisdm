# Copyright (c) 2012 Medhi Widjaja

class ProductWatchListsController < ApplicationController

  def create
    @user = current_user
    authorize! :update, @user
    @product = Product.find(params[:product_id])
    @user.track! @product
    respond_to do |format|
      # format.html { redirect_to product_path(@product.slug) }
      format.js
    end
  end

  def destroy
    @user = current_user
    authorize! :update, @user
    @product = Product.find(params[:product_id])
    @user.untrack! @product
    respond_to do |format|
      format.html { redirect_to @product }
      format.js
    end
  end
end