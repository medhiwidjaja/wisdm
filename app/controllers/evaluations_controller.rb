# Copyright (c) 2012 Medhi Widjaja

class EvaluationsController < ApplicationController
  before_filter :disable_right_frame!, :enable_left_frame!
  # commented out for YC demo
  # before_filter :authenticate_user!
  before_filter :setup, except: [ :index ]
  before_filter :menu

  def index
    criterion = Criterion.find(params[:criterion_id])
    article = criterion.article
    authorize! :view, article
  end

  def rank
    @num_ranks = @criterion.leaf? ? @article.alternatives.count : @criterion.children.count
    if session[:rkcmp].nil?
      mismatched = false
    else
      mismatched = session[:rkcmp].length != @num_ranks
    end
    if @criterion.leaf? # comparing alternatives
      @article.alternatives.asc(:position).each do |alt|
        rc = @evaluation.rank_comparisons.find_or_initialize_by id1:alt.id
        rc.title = alt.title
        rc.pos = alt.position
      end
      @ranks_incomplete = @evaluation.rank_comparisons.any? {|c| c.rank.nil? }
      if @ranks_incomplete or mismatched
        begin
          @rank_comparisons = @evaluation.rank_comparisons.asc(:pos)
        rescue
          flash[:error] = "Oops, I couldn't find one or more alternatives in this comparison. You will have to redo the comparison."
          @rank_comparisons = @evaluation.rank_comparisons.all
        end
      else
        unless session[:rkcmp].nil?
          @rank_comparisons = session[:rkcmp].map {|n, comp| OrdinalComparison.new comp }
          session[:rkcmp] = nil
        else
          @rank_comparisons = @evaluation.rank_comparisons.asc(:rank)
        end
      end 
    else                # comparing sub-criteria
      @criterion.children.each do |child|
        rc = @evaluation.rank_comparisons.find_or_initialize_by id1:child.id
        rc.title = child.title
        rc.pos = child.position
      end
      @ranks_incomplete = @evaluation.rank_comparisons.any? {|c| c.rank.nil? }
      if @ranks_incomplete or mismatched
        begin
          @rank_comparisons = @evaluation.rank_comparisons.asc(:pos)
        rescue
          flash[:error] = "Oops, I couldn't find one or more criteria in this comparison. You will have to redo the comparison."
          @rank_comparisons = @evaluation.rank_comparisons.all
        end
      else
        unless session[:rkcmp].nil?
          @rank_comparisons = session[:rkcmp].map {|n, comp| OrdinalComparison.new comp }
          session[:rkcmp] = nil
        else
          @rank_comparisons = @evaluation.rank_comparisons.asc(:rank)
        end
      end
    end
    
    @roc_table = Evaluation.rank_order_centroid_table(@rank_comparisons.count)
    @rank_sum_table = Evaluation.rank_sum_table(@rank_comparisons.count)
    @rank_reciprocal_table = Evaluation.rank_reciprocal_table(@rank_comparisons.count)
    @rank_exponential_table = Evaluation.rank_exponential_table(@rank_comparisons.count)

    respond_to do |format|
      format.html
      format.js
    end
  end

  def reset_rank
  end

  def direct
  end

  def smart
    if @criterion.leaf? # comparing alternatives
      @article.alternatives.each do |alt|
        @evaluation.smart_comparisons.find_or_initialize_by id1:alt.id        
      end
      @smart_comparisons = @evaluation.smart_comparisons.sort_by {|s| Alternative.find(s.id1).position }
    else                # comparing sub-criteria
      @criterion.children.each do |child|
        @evaluation.smart_comparisons.find_or_initialize_by id1:child.id
      end
      @smart_comparisons = @evaluation.smart_comparisons
    end
  end

  def pairwise
    if @criterion.leaf? # comparing alternatives
      @pairs = @article.alternatives.asc(:position).map{|alt| alt.id }.combination(2)
    else                # comparing sub-criteria
      @pairs = @criterion.children.asc(:id).map{|c| c.id }.combination(2)
    end
    @pairs.each do |pair|
      @evaluation.pairwise_comparisons.find_or_initialize_by id1:pair.first, id2:pair.last
    end
  end

  def rank_pairwise
  #    self.criterion.
  end

  # def adjust
  #   @rank_comparisons = @evaluation.rank_comparisons
  #   @roc_table = Evaluation.rank_order_centroid_table(@rank_comparisons.count)
  #   @rank_sum_table = Evaluation.rank_sum_table(@rank_comparisons.count)
  #   @rank_reciprocal_table = Evaluation.rank_reciprocal_table(@rank_comparisons.count)
  #   @rank_exponent_table = Evaluation.rank_exponent_table(@rank_comparisons.count)
  #   @num_ranks = @criterion.leaf? ? @article.alternatives.count : @criterion.children.count

  #   if @criterion.leaf? # comparing alternatives
  #     # For smart comparison
  #     @article.alternatives.each do |alt|
  #       @evaluation.smart_comparisons.find_or_initialize_by id1:alt.id        
  #     end
  #     @smart_comparisons = @evaluation.smart_comparisons.sort_by {|s| Alternative.find(s.id1).position }
  #     # For pairwise comparison
  #     @pairs = @article.alternatives.asc(:position).map{|alt| alt.id }.combination(2)
  #     @pairs.each do |pair|
  #       @evaluation.ahp_comparisons.find_or_initialize_by id1:pair.first, id2:pair.last
  #     end 
  #   else                # comparing sub-criteria
  #     # For smart comparison
  #     @criterion.children.each do |child|
  #       @evaluation.smart_comparisons.find_or_initialize_by id1:child.id
  #     end
  #     @smart_comparisons = @evaluation.smart_comparisons
  #     # For pairwise comparison
  #     @pairs = @criterion.children.asc(:id).map{|c| c.id }.combination(2) 
  #     @pairs.each do |pair|
  #       @evaluation.ahp_comparisons.find_or_initialize_by id1:pair.first, id2:pair.last
  #     end
  #   end
  # end

  def update_rank
    authorize! :update, @evaluation
    @evaluation.attributes = params[:evaluation]
    @evaluation.rank_method = Evaluation::RANK_METHODS[:rank_order_centroid] if params[:evaluation][:rank_method].blank?
    @evaluation.eval_method = Evaluation::EVALUATION_METHODS['MAGIQ']
    @evaluation.eval_object = @criterion.leaf? ? "Alternative" : "Criterion"
    if @evaluation.ranks_are_valid?
      if @evaluation.save!
        @evaluation.update_rank_comparisons Evaluation::RANK_METHOD_NAMES[@evaluation.rank_method]
        @article.model_time_stamp
        @evaluation.update_rank_scores
        flash[:notice] = "Sub-criteria ranking was successfully saved."
        redirect_on_success
      else
        flash[:error] = "Save failed. Please complete all rankings."
        redirect_to rank_criterion_evaluations_path(@criterion.id)
      end
    else
      session[:rkcmp] = params[:evaluation][:rank_comparisons_attributes]
      flash[:error] = "Error: Please check tied rankings and make sure there are no empty ranking slots above any item."
      redirect_to rank_ criterion_evaluations_path(@criterion.id)
      # render partial: 'rank_error', content_type: 'text/html'
    end
  end

  def update_smart
    authorize! :update, @evaluation
    @evaluation.attributes = params[:evaluation]
    all_is_numeric = @evaluation.smart_comparisons.all? { |c| begin Float(c.value) ; true end rescue false }
    if all_is_numeric
      @evaluation.eval_method = Evaluation::EVALUATION_METHODS['SMART']
      @evaluation.eval_object = @criterion.leaf? ? "Alternative" : "Criterion"
      if @evaluation.save!
        @evaluation.update_smart_scores
        @article.model_time_stamp
        flash[:notice] = (@criterion.leaf? ? "Ratings" : "Weights") + " were successfully saved."
        redirect_on_success
      else
        flash[:error] = "Errors occurred. " + (@criterion.leaf? ? "Ratings" : "Weights") + " were not saved."
        redirect_to smart_criterion_evaluations_path(@criterion.id)
      end
    else
      flash[:error] = "Errors occurred. Weight must be a number."
      redirect_to smart_criterion_evaluations_path(@criterion.id)
    end
  end

  def update_pairwise
    authorize! :update, @evaluation
    @evaluation.attributes = params[:evaluation]
    @evaluation.eval_method = Evaluation::EVALUATION_METHODS['Pairwise']
    @evaluation.eval_object = @criterion.leaf? ? "Alternative" : "Criterion"
    if @evaluation.save!
      @article.model_time_stamp
      @evaluation.save_ahp_scores
      if @evaluation.cr > 0.12
        flash[:error] = "Consistency is poor (Inconsistency Ratio=#{@evaluation.cr.round(2)}). Please review all comparisons."
        redirect_to pairwise_criterion_evaluations_path(@criterion.id)
        # render action: :pairwise
      else
        flash[:notice] = "Pairwise comparisons were successfully saved. Consistency Ratio = #{@evaluation.cr.round(2)}"
        redirect_on_success
      end
    else
      flash[:error] = "Errors occurred. Pairwise comparisons couldn't be saved."
      redirect_to pairwise_criterion_evaluations_path(@criterion.id)
    end
  end


private

  def setup
    # TODO: change @criterion to @evaluable when Evaluation is applicable to
    #       something else other than Criterion
    @criterion = Criterion.find(params[:criterion_id])
    @article = @criterion.article
    # commented out for YC demo
    # authorize! :compare, @article
    @article_id = @article.id.to_s 
    if params[:p]
      @participation_id = params[:p]
      @participation = @article.participations.find(@participation_id)
      user = @participation.user
    else
      user = current_user
      @participation = @article.participations.where(user_id:user.id).first
      @participation_id = @participation.id
    end
    @evaluation = @criterion.evaluations.find_or_create_by participation_id:@participation_id
  end

  def subcriteria_comparisons(children)
    unless children.empty?
      max = children.max_by {|c| c.weight }.weight unless children.any? { |c| c.weight.nil? }
      children.map {|c| Hash[
        no:c.position+1, title:c.title, order: c.magiq_order, weight:c.weight, 
        ratio: max.nil? ? '-' : c.weight/max*100
      ]}
    end
  end

  def redirect_on_success
    if @criterion.leaf?   # alternative comparison
      redirect_to criterion_ratings_path @criterion.id
    else
      redirect_to criterion_path @criterion.id
    end
  end

  def redirect_on_error
    if @criterion.leaf?   # alternative comparison
      redirect_to criterion_ratings_path @criterion.id
    else
      redirect_to criterion_path @criterion.id
    end
  end

  def menu
    if @criterion.leaf?
      session[:link] = 'rate'
    else
      session[:link] = 'criteria'
    end
  end
end
