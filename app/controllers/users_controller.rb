# Copyright (c) 2012 Medhi Widjaja

require 'will_paginate/array'

class UsersController < ApplicationController
  before_filter :authenticate_user!
  before_filter :disable_right_frame!, :enable_left_frame!

  respond_to :html, :json

  def index
    authorize! :read, User
    respond_to do |format|
      format.html { @users = User.asc(:_id).paginate(page: params[:page]) }
      format.json { 
        rgx = /#{params[:term]}/i
        @names = User.where(name:rgx).collect {|u| u.profile_info}.paginate(page: params[:page])
        if params[:callback]
          render json: @names, callback: params[:callback]
        else
          respond_with @names
        end
      }
    end
  end

  def show
    @user = User.find(params[:id])
    authorize! :read, @user
    @following_ids = current_user.friend_list.following_ids
    
    respond_to do |format|
      format.html do
        case params[:fn]
        when 'article'
          @action_title = 'Articles'
          respond_with @public_articles = @user.articles.public.desc(:updated_at).paginate(page: params[:page])
        when 'particle' 
          @action_title = 'Private Articles'
          respond_with @private_articles = @user.articles.private.desc(:updated_at).paginate(page: params[:_page])
        when 'participating'
          @action_title = 'Participating'
          respond_with @participating = Participation.where(user:@user).all_active.collect { |p| p.article unless p.article.nil? || p.article.user == @user }.compact.paginate(page: params[:page])
        when 'bookmarks'
          @action_title = 'Bookmarks'
          respond_with @watched_articles = @user.watch_list.articles.desc(:updated_at).paginate(page: params[:page])
        when 'product'
          @action_title = 'Products'
          respond_with @public_products = @user.products.public.desc(:updated_at).paginate(page: params[:page])
        when 'pproduct'
          @action_title = 'Private Products'
          respond_with @private_products = @user.products.private.desc(:updated_at).paginate(page: params[:page])
        when 'tracking'
          @action_title = 'Products tracked'
          respond_with @products_tracked = Product.in(id:@user.product_watch_list).desc(:updated_at).paginate(page: params[:page])
        when 'following'
          respond_with @following = @user.friend_list.following.paginate(page: params[:following_page])
        when 'followers'
          respond_with @followers = @user.friend_list.followers.paginate(page: params[:followers_page])
        else
          respond_with @user.profile_info
        end
      end
       
      format.json do
        if params[:callback]
          render json: @user.profile_info, callback: params[:callback]
        else
          respond_with @user.profile_info
        end
      end
    end
  end

  def edit
    @user = User.find(params[:id])
    authorize! :edit, @user
  end

  def account
    @user = User.find(params[:id])
    authorize! :account, @user
  end

  def update
    user = User.find(params[:id])
    authorize! :update, user

    if user.update_attributes(params[:user])
      redirect_to user, notice: 'User was successfully updated.'
    else
      render action: "edit"
    end
  end

  def following
    @user = User.find(params[:id])
    @header_title = "#{@user.name}'s followed users"
    authorize! :read, @user
    @following_ids = @user.friend_list.following_ids
    @users = @user.friend_list.following.paginate(page: params[:page]).map {|u| u}

    # For performance reason, so that it won't hit the database again
    @pages = @user.friend_list.following.paginate(page: params[:page])
    @mode = :following
    respond_to do |format|
      format.html { render 'show_follow' }
      format.json do
        if params[:cb]
          render json: @users, callback: params[:cb]
        else
          respond_with @users
        end
      end
    end
  end

  def followers
    @user = User.find(params[:id])
    @header_title = "#{@user.name}'s followers"
    authorize! :read, @user
    @following_ids = @user.friend_list.following_ids
    @users = @user.friend_list.followers.paginate(page: params[:page]).map {|u| u}
    
    # For performance reason, so that it won't hit the database again
    @pages = @user.friend_list.following.paginate(page: params[:page])
    @mode = :followers
    respond_to do |format|
      format.html { render 'show_follow' }
      format.json do
        if params[:cb]
          render json: @users, callback: params[:cb]
        else
          respond_with @users
        end
      end
    end
  end

  def profile
    @user = User.find(params[:id])
    authorize! :read, @user
  end

  def current
    @user = current_user
    authorize! :read, @user
    respond_to do |format|
      format.html { render 'show' }
      format.json do
        if params[:cb]
          render json: [ @user.profile_info ], callback: params[:cb]
        else
          render json: [ @user.profile_info ]
        end
      end
    end
  end

=begin
  def follow
    @user = User.find(params[:id])
    if @user != current_user
      current_user.follow!(@user)
      flash[:success] = "Now following #{@user.name}"
      message = flash[:success]
    end

    respond_to do |format|
      format.html do
        redirect_to user_path(@user)
      end
      format.js {
        render(:json => {:success => success,
                 :message => message }.to_json)
      }
    end
  end

  def unfollow
    @user = User.find(params[:id])
    current_user.unfollow!(@user)
    flash[:success] = "Now unfollowing #{@user.name}"
    message = flash[:success]
    respond_to do |format|
      format.html do
        redirect_to user_path(@user)
      end
      format.js {
        render(:json => {:success => success,
                 :message => message }.to_json)
      }
    end
  end
=end


end
