class Admin::ManageController < ApplicationController
  before_filter :authenticate_user!
  before_filter :disable_right_frame!, :enable_left_frame!

  # def feature
  #   authorize! :manage, FeaturedArticle
  #   @today = Date.today
  #   @featured_articles = FeaturedArticle.where(:start_date.lt => @today).and(:expire_date.gt => @today)
  #   @articles = Article.paginate(page: params[:page])
  # end

  def users
    authorize! :manage, User
    names = params[:q]
    unless names.blank?
      @users = User.search page: params[:page] do
        query { string names }
      end
      @names = names
      respond_to do |format|
        format.html # search.html.erb
      end
    else
      @users = User.asc(:name).paginate(page: params[:page])
    end
    @names = names
  end

  def user
    authorize! :manage, User
    @user_id = params[:user_id]
    @user = User.find @user_id
  end

  def update_user_account
    authorize! :manage, User
    @user = User.find(params[:user_id])
    authorize! :update, @user

    if @user.update_attributes(params[:user])
      redirect_to manage_users_path, notice: "User #{@user.name} was successfully updated."
    else
      flash[:error] = "Error: could not save user account data."
      render action: 'user'
    end
  end

  def settings
  end

end