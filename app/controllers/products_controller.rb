# Copyright (c) 2012 Medhi Widjaja

class ProductsController < ApplicationController
  before_filter :disable_right_frame!, :enable_left_frame!
  before_filter :authenticate_user!, except: [:index, :show, :featured, :top]
  helper :alternatives

  # GET /products
  # GET /products.json
  def index
    @search_string = params[:q]
    if @search_string
      @products = Product.search @search_string, page: params[:page]
    elsif params[:o] == "wn"
      @products = Product.where(user: current_user).paginate(page: params[:page])
    elsif params[:fn] == "tracking"
      @products = Product.in(id:current_user.product_watch_list).desc(:updated_at).paginate(page: params[:page])
    else
      @products = Product.public.paginate(page: params[:page])
    end
    respond_to do |format|
      format.html # index.html.erb
      format.js
      format.json { render json: @products }
    end
  end

  # GET /products/1
  # GET /products/1.json
  def show
    @product = Product.find(params[:id])
    authorize! :read, @product

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @product }
    end
  end

  # GET /products/new
  # GET /products/new.json
  def new
    @product = current_user.products.new
    authorize! :create, @product

    unless params[:export].blank?
      alternative = Alternative.find params[:export]
      @product.title = alternative.title
      @product.description = alternative.description
      @product.abbrev = alternative.abbrev
      @product.properties = alternative.properties
    end

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @product }
    end
  end

  # GET /products/1/edit
  def edit
    @product = Product.find(params[:id])
    authorize! :edit, @product
  end

  # POST /products
  # POST /products.json
  def create
    @product = current_user.products.new(params[:product])
    authorize! :create, @product

    respond_to do |format|
      if @product.save
        format.html { redirect_to @product, notice: 'Product was successfully created.' }
        format.json { render json: @product, status: :created, location: @product }
      else
        format.html { render action: "new" }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /products/1
  # PUT /products/1.json
  def update
    @product = Product.find(params[:id])
    authorize! :update, @product

    respond_to do |format|
      if @product.update_attributes(params[:product])
        format.html { redirect_to @product, notice: 'Product was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product = Product.find(params[:id])
    authorize! :destroy, @product
    @product.destroy

    respond_to do |format|
      format.html { redirect_to products_url }
      format.json { head :no_content }
    end
  end

  def featured
    @featured_products = FeaturedProduct.current.desc(:expire_date).paginate(page: params[:page])
    respond_to do |format|
      format.html 
      format.json { render json: @featured_products }
    end
  end

  def top
    # TODO: select and sort products by some criteria:
    #   sort by most viewed, most liked, most used, etc.
    @products = Product.public.paginate(page: params[:page])
    respond_to do |format|
      format.html 
      format.json { render json: @products }
    end
  end

  def like
    @product = Product.find(params[:product_id])
    authorize! :read, @product

    current_user.like_this! @product
    respond_to do |format|
      format.html { redirect_to @product }
      format.js
    end
  end

  def watch
    @product = Product.find(params[:product_id])
    authorize! :read, @product

    current_user.track! @product
    respond_to do |format|
      format.html { redirect_to @product }
      format.js
    end
  end
 
  def info_panel
    @product = Product.find(params[:product_id])
    authorize! :read, @product
    render layout: false
  end
end
