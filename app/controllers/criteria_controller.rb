# Copyright (c) 2012 Medhi Widjaja

class CriteriaController < ApplicationController
  before_filter :disable_right_frame!, :enable_left_frame!
  # before_filter :authenticate_user!, except: [:tree]
  # line below is for YC demo
  before_filter :authenticate_user!, except: [:index, :tree, :show]
  before_filter :criterion_menu
  
  def index
    @article = Article.find(params[:article_id])
    authorize! :view, @article
    @article_id = @article.id.to_s 
    @participation = valid_participation(@article)
    @participation_id = @participation.id
    @participations = @article.participations
    @criteria = @article.criteria
    if @criteria.empty?
      if can? :modify, @article
        @root = @article.initialize_criteria_tree 
        unless can? :compare, @article
          unless @article.common_properties.blank?
            flash[:notice] = "This Criteria Tree was initially setup with the articles's common properties
                that you setup in the Alternatives screen.
                You are free to modify the criteria as you wish."
          else
            flash[:notice] = "You have not specify any criteria. Select the root node of the Criteria Tree and
                create some sub-criteria from there."
          end
        end
      end
    else
      @root = @criteria.root
    end

    if @root
      if can? :compare, @article
        @evaluation = @root.evaluations.find_or_initialize_by participation_id:@participation_id
      else
        default_participation_id = @article.participation_by(@article.user.id).id
        @evaluation = @root.evaluations.where(participation_id: default_participation_id).first
      end
      unless @evaluation.nil?
        @table = @evaluation.score_table @evaluation.eval_method
        @comparison_method = Evaluation::EVALUATION_METHOD_NAMES[@evaluation.eval_method]
      end
    else
      flash[:error] = "No criteria have been set for this article."
    end

    # Evaluation
    @criterion = @root
  end

  def show
    @criterion = Criterion.find(params[:id])
    @article = @criterion.article
    authorize! :view, @article
    @article_id = @article.id.to_s 
    @participation = valid_participation(@article)
    @participation_id = @participation.id

    if @participation.user.can? :compare, @article
      @evaluation = @criterion.evaluations.find_or_initialize_by participation_id:@participation_id
      @table = @evaluation.score_table @evaluation.eval_method
      @comparison_method = Evaluation::EVALUATION_METHOD_NAMES[@evaluation.eval_method]
    else
      default_participation_id = @article.participation_by(@article.user.id).id
      @evaluation = @criterion.evaluations.where(participation_id: default_participation_id).first
      unless @evaluation.nil?
        @table = @evaluation.score_table @evaluation.eval_method
        @comparison_method = Evaluation::EVALUATION_METHOD_NAMES[@evaluation.eval_method]
      end
    end
    
    @comments = @criterion.root_comments.desc(:created_at)
    @commentable = @criterion
    if user_signed_in?
      @comment = @criterion.comments.new user:current_user
    end

    respond_to do |format|
      format.html do # show.html.slim 
        if request.xhr?
          render layout: false
        end
      end
      format.js   # show.js.erb
    end
  end

  def edit
    @criterion = Criterion.find(params[:id])
    @root = @criterion.root
    @article = @criterion.article
    authorize! :modify, @article
    @article_id = @article.id.to_s
    @participation = @article.participations.where(user_id:current_user.id).first
    @participation_id = @participation.id
  end

  def update
    @criterion = Criterion.find(params[:id])
    @article = @criterion.article
    authorize! :modify, @article
    @root = @criterion.root
    @article_id = @article.id.to_s
    @participation = @article.participations.where(user_id:current_user.id).first
    @participation_id = @participation.id
    
    respond_to do |format|
      if @criterion.update_attributes(params[:criterion])
        @article.model_time_stamp
        format.html { redirect_to @criterion, notice: 'Criterion was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @criterion.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    criterion = Criterion.find(params[:id])
    article = criterion.article
    authorize! :modify, article
    article_id = criterion.article.id
    criterion.destroy
    article.model_time_stamp
    respond_to do |format|
      format.html { redirect_to article_criteria_path(article_id) }
      format.json { head :no_content }
    end
  end

  def tree
    root_id = params[:criterion_id]
    @root = Criterion.find(root_id)
    authorize! :view, @root.article
    @participation = valid_participation(@root.article)
    @participation_id = @participation.id

    if @participation.tree.nil? || @participation.tree_is_stale?
      tree = @root.as_tree(@participation_id)
      @participation.update_attributes tree: tree, tree_ts: Time.now.utc
    end

    respond_to do |format|
      format.json { render text: @participation.tree }
    end
  end

  def new
    @article = Article.find(params[:article_id])
    authorize! :modify, @article
    @article_id = @article.id.to_s 
    @criterion = @article.criteria.build
    @root = @article.criteria.root

    #respond_to do |format|
    #  format.html # new.html.erb
    #  format.json { render json: @criterion }
    #end
  end

  def create
    @article = Article.find(params[:article_id])
    authorize! :modify, @article
    @criterion = article.criteria.build(params[:criterion])
    object = @article.criteria.root.nil? ? "Goal" : "New criterion"
    respond_to do |format|
      if @criterion.save!
        article.model_time_stamp
        format.html { redirect_to @criterion, notice: "#{object} was successfully created." }
        format.json { render json: @criterion, status: :created, location: criterion }
      else
        format.html { render action: "new" }
        format.json { render json: @criterion.errors, status: :unprocessable_entity }
      end
    end        
  end

  # New sub-criterion
  def new_sub
    @criterion = Criterion.find(params[:id])
    @root = @criterion.root
    @return_id = params[:r] ? params[:r] : params[:id]
    @article = @criterion.article
    authorize! :modify, @article
    @child = @article.criteria.build
    @participation = @article.participations.where(user_id:current_user.id).first
  end

  # Saving new sub-criterion
  def create_sub
    @criterion = Criterion.find(params[:id])
    @root = @criterion.root
    @return_id = params[:r] ? params[:r] : params[:id]
    @article = @criterion.article
    authorize! :modify, @article
    @child = @article.criteria.build(params[:criterion])
    @article_id = @article.id
    @participation = @article.participations.where(user_id:current_user.id).first
    @participation_id = @participation.id

    respond_to do |format|
      if @child.save
        @criterion.children << @child
        @article.model_time_stamp
        format.html { redirect_to criterion_path(@child.id), notice: 'New sub-criterion was successfully created.' }
        format.json { render json: @child, status: :created, location: @child }
      else
        format.html { render action: :new_sub }
        format.json { render json: @child.errors, status: :unprocessable_entity }
      end
    end
    # @article.model_time_stamp
  end

  def move
    criterion = Criterion.find(params[:id])
    article = criterion.article
    authorize! :modify, article
    target = Criterion.find(params[:to])
    parent_id = params[:pid]
    case position = params[:position]
    when 'after'
      criterion.move_below(target)
    when 'before'
      criterion.move_above(target)
    when 'inside'
      if target.children.empty?
        criterion.parent = target
        criterion.save!           # need to save it first ...
        criterion.position = 0    #  so position can be set correctly
      elsif criterion.parent==target
        criterion.move_to_top
      else
        criterion.move_above(target.children.first)
      end
    end
    criterion.save!
    if parent_id != criterion.parent_id
      invalidate_related_comparisons(criterion, parent_id)
    end
    article.model_time_stamp
    render nothing:true
  end

  def aggregate_summary
    @criterion = Criterion.find(params[:criterion_id])
    @article = @criterion.article
    authorize! :view, @article
    @article_id = @article.id.to_s
    @participations = @article.participations
    #@participation_id = @article.participations.where(user_id:current_user.id).first.id.to_s
    @summary_table = @criterion.aggregate_summary_table
    @summary = true
  end

  def aggregate_detail
    @criterion = Criterion.find(params[:criterion_id])
    @article = @criterion.article
    authorize! :view, @article
    @article_id = @article.id.to_s
    @participations = @article.participations
    #@participation_id = @article.participations.where(user_id:current_user.id).first.id.to_s
    @detail_table = @criterion.aggregate_detail_table 
    @summary_table = @criterion.aggregate_summary_table
    @detail = true
  end

private

  def valid_participation(article)
    if !params[:p].blank?
      # Get participation from the specified parameter
      participation = Participation.find(params[:p])
    elsif user_signed_in?
      # User is signed in, try to get the current user's participation if exists
      participation = article.participation_by(current_user)
    end

    if participation.nil?
      # Still got no valid participation,
      # then get participation of the article's author
      participation = article.participation_by(article.user)
    end
    participation
  end

  def invalidate_related_comparisons(criterion, old_parent_id)
    old_parent = Criterion.find(old_parent_id)
    old_parent.comparisons.each do |comp| 
      case comp.type
      when 1
        comp.magiq_comparisons.destroy_all
      when 2
        comp.smart_comparisons.destroy_all
      when 3
        comp.ahp_comparisons.destroy_all
        comp.update_attribute :c_r, nil
      end
    end
    old_parent.children.each {|child| child.comparisons.each {|comp| comp.update_attributes(weight:nil, weight_n:nil) }}
    criterion.parent.comparisons.destroy_all
    criterion.parent.children.each {|child| child.comparisons.each {|comp| comp.update_attributes(weight:nil, weight_n:nil) }}
    criterion.comparisons.destroy_all
  end

  def criterion_menu
    session[:link] = 'criteria'
  end

end



