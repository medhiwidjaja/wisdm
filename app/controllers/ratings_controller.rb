# Copyright (c) 2012 Medhi Widjaja

class RatingsController < ApplicationController
  before_filter :disable_right_frame!, :enable_left_frame!
  # commented out for YC demo
  # before_filter :authenticate_user!
  before_filter :rating_menu

  def evaluate
    @article = Article.find(params[:article_id])
    @criterion = @article.criteria.root.leaves.first unless @article.criteria.root.nil?
    # @participation = @article.participations.where(user_id:current_user.id).first
    # @participation_id = @participation.id.to_s
    redirect_to criterion_ratings_path @criterion unless @criterion.nil?

    # authorize! :view, @article
    # @article_id = @article.id.to_s
    # @alternatives = @article.alternatives.asc(:position)
    # @common_properties = @article.common_properties
    # @criterion = @article.criteria.root
    # if params[:p]
    #   @participation_id = params[:p]
    #   @participation = @article.participations.find(@participation_id)
    #   user = @participation.user
    # else
    #   user = current_user
    #   @participation_id = @article.participations.where(user_id:user.id).first.id
    #   @participation = @article.participations.find(@participation_id)
    # end
  end

  def index
    @criterion = Criterion.find(params[:criterion_id])
    @article = @criterion.article
    authorize! :view, @article
    @criteria = @article.criteria
    @article_id = @article.id.to_s
    @alternatives = @article.alternatives
    @participation = valid_participation(@article)
    @participation_id = @participation.id
    user = @participation.user
    
    # if params[:p]
    #   @participation_id = params[:p]
    #   @participation = @article.participations.find(@participation_id)
    #   user = @participation.user
    # else
    #   user = current_user
    #   @participation = @article.participations.where(user_id:user.id).first
    #   @participation_id = @participation.id.to_s unless @participation.nil?
    # end

    @user_can_rate = user.can? :rate, @article
    if @user_can_rate
      @evaluation = @criterion.evaluations.find_or_initialize_by participation_id: @participation_id
      @table = @evaluation.score_table @evaluation.eval_method
      @comparison_method = Evaluation::EVALUATION_METHOD_NAMES[@evaluation.eval_method]
    else
      default_participation_id = @article.participation_by(@article.user.id).id
      @evaluation = @criterion.evaluations.where(participation_id: default_participation_id).first
      unless @evaluation.nil?
        @table = @evaluation.score_table @evaluation.eval_method
        @comparison_method = Evaluation::EVALUATION_METHOD_NAMES[@evaluation.eval_method]
      end
    end

    if @evaluation.method_is_smart?
      @notes = @evaluation.smart_notes
    elsif @evaluation.method_is_magiq?
      @notes = @evaluation.rank_notes
    elsif @evaluation.method_is_pairwise?
      @notes = @evaluation.ahp_notes
    end
    
    respond_to do |format|
      format.html # show.html.slim
      format.js   # show.js.erb
      # format.json # show.json.rabl
    end
  end

  def aggregate_summary
    @criterion = Criterion.find(params[:criterion_id])
    @criterion.reload
    @article = @criterion.article
    authorize! :view, @article
    @article_id = @article.id.to_s
    @size = @criterion.evaluations.size
    @participations = @article.participations
    # @participation_id = @article.participations.where(user_id:current_user.id).first.id.to_s
    @summary_table = @criterion.aggregate_summary_table
    @summary = true  
  end

  def aggregate_detail
    @criterion = Criterion.find(params[:criterion_id])
    @criterion.reload
    @article = @criterion.article
    authorize! :view, @article
    @article_id = @article.id.to_s
    @participations = @article.participations
    # @participation_id = @article.participations.where(user_id:current_user.id).first.id.to_s
    @detail_table = @criterion.aggregate_detail_table 
    @summary_table = @criterion.aggregate_summary_table
    @detail = true
  end
  
private

  def valid_participation(article)
    if !params[:p].nil?
      # Get participation from the specified parameter
      participation = Participation.find(params[:p])
    elsif user_signed_in?
      # User is signed in, try to get the current user's participation if exists
      participation = article.participation_by(current_user)
    end

    if participation.nil?
      # Still got no valid participation,
      # then get participation of the article's author
      participation = article.participation_by(article.user)
    end
    participation
  end

  def subcriteria_table(alternatives)
    unless alternatives.empty?
      alternatives_ratings = alternatives.collect { |alt| alt.rating_for(@participation_id, @criterion.id) }
      unless alternatives_ratings.any? { |rating| rating.nil? }
        # max = alternatives_ratings.max_by { |rating| rating.weight }.weight
        # sum = alternatives_ratings.map { |rating| rating.weight }.sum
        alternatives_ratings.map { |rating| Hash[ 
          no:       rating.alternative.position+1, 
          title:    rating.alternative.title, 
          order:    rating.magiq_order, 
          weight:   rating.weight.to_f, 
          # weight_n: (sum.nil? or sum==0) ? nil : rating.weight.to_f/sum,
          weight_n: rating.weight_n,
          #ratio:    (max.nil? or max==0) ? 0 : rating.weight.to_f/max*100
          ratio:    rating.weight_n.to_f*100
        ]}
      else
        alternatives.map { |alt| Hash[ no: alt.position+1, title: alt.title ] }
      end
    end
  end

  def rating_menu
    session[:link] = 'rate'
  end

end


