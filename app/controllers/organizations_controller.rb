class OrganizationsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :disable_right_frame!, :enable_left_frame!

  # GET /organizations
  # GET /organizations.json
  def index
    @user = current_user
    @organizations = Organization.all_with_members @user.id

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @organizations }
    end
  end

  # GET /organizations/1
  # GET /organizations/1.json
  def show
    @organization = Organization.find(params[:id])
    authorize! :read, @organization
    @user = current_user

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @organization }
    end
  end

  # GET /organizations/new
  # GET /organizations/new.json
  def new
    @user = current_user
    @organization = @user.organizations.new
    authorize! :create, @organization

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @organization }
    end
  end

  # GET /organizations/1/edit
  def edit
    @organization = Organization.find(params[:id])
    authorize! :edit, @organization
    @user = current_user
  end

  # POST /organizations
  # POST /organizations.json
  def create
    @organization = Organization.new(params[:organization])
    authorize! :create, @organization
    @user = current_user

    respond_to do |format|
      if @organization.save
        format.html { redirect_to @organization, notice: 'Organization was successfully created.' }
        format.json { render json: @organization, status: :created, location: @organization }
      else
        format.html { render action: "new" }
        format.json { render json: @organization.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /organizations/1
  # PUT /organizations/1.json
  def update
    @organization = Organization.find(params[:id])
    authorize! :update, @organization
    @user = current_user

    respond_to do |format|
      if @organization.update_attributes(params[:organization])
        format.html { redirect_to @organization, notice: 'Organization was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @organization.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /organizations/1
  # DELETE /organizations/1.json
  def destroy
    @organization = Organization.find(params[:id])
    authorize! :destroy, @organization
    @user = current_user
    @organization.destroy

    respond_to do |format|
      format.html { redirect_to organizations_url }
      format.json { head :no_content }
    end
  end

  def new_members
    @organization = Organization.find(params[:id])
    authorize! :read, @organization
    @members = @organization.members.paginate(page: params[:page])
    @user = current_user

    respond_to do |format|
      format.html 
      format.json { head :no_content }
    end
  end

  def add_members
    @organization = Organization.find(params[:id])
    authorize! :update, @organization
    @new_member = User.find(params[:user_id])
    @organization.add_member! @new_member

    respond_to do |format|
      format.html { redirect_to @organization }
      format.js
    end
  end

  def remove_members
    @organization = Organization.find(params[:id])
    authorize! :update, @organization
    @member = User.find(params[:member_id])
    @organization.remove_member! @member

    respond_to do |format|
      format.html { redirect_to @organization }
      format.js
    end
  end


end
