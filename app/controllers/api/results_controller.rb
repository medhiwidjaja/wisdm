# Copyright (c) 2012 Medhi Widjaja

class Api::ResultsController < ApplicationController

  before_filter :disable_right_frame!, :enable_left_frame!
  # before_filter :authenticate_user!
  # before_filter :results_menu
  before_filter :check_completeness
  before_filter :setup, except: [:rank]

  layout 'embedded'

  def results
    @score_table = @result.score_table
    
    @chart_data  = @result.chart_data
    @stacked_chart_data = @result.detail_chart_data
    @objective_labels = @result.criteria_labels
    @alternative_labels = @result.alternative_labels
    @alternative_names = @result.alternative_names
  end

  def rank
    @criterion = Criterion.find(params[:criterion_id])
    @article = @criterion.article
    authorize! :view, @article
    @article_id = @article.id
    criteria = @article.criteria
    root = criteria.root
    @goal = root
    @participation_id = @participation.id
    @alternatives = @article.alternatives
    @aggregate = true if params[:g]
    @result = Result.new @article, @goal, @participation, @aggregate
    @score_table = @result.score_table @criterion

    @chart_data  = @result.chart_data
    @stacked_chart_data = @result.detail_chart_data
    @objective_labels = @result.criteria_labels
    @alternative_labels = @result.alternative_labels
    @alternative_names = @result.alternative_names
  end

  def benefit_cost
    setup
    unless @alternatives.any? { |alt| alt.properties.where(cost:true).first.nil? }
      # @score_table = @result.score_table
      @chart_data = @result.score_table.map {|a| [[a[:cost], a[:score], a[:abbrev].blank? ? a[:title] : a[:abbrev]]] }
      @cost_benefit_table = @result.benefit_cost_table
      @frontier_line = @result.efficient_frontier_lines(@cost_benefit_table)
      @max = @result.score_table.map{|a| a[:score]}.max
      @max = (((@max+0.05)*2).round(1)/2).round(2)
    else
      @no_cost = true
    end
    if params[:chart]
      render partial: "cost_benefit_chart"
    else
      respond_to do |format|
        format.html
      end
    end
  end

  def sensitivity_analysis
    setup
    @criterion = @article.criteria.root.children.first
    unless @criterion.root?
      @sensitivity_chart_data, @sensitivity_chart_labels = @result.sensitivity_data_for_criterion(@criterion.id)
      @chart_data  = @result.chart_data
      @alternative_labels = @sensitivity_chart_labels
      @weight = @criterion.parent.privileged_evaluation_by(@participation_id).score_for_item(@criterion).weight_n
    end
  end

  def sensitivity_chart
    setup
    @criterion = @article.criteria.find(params[:criterion_id])
    unless @criterion.root?
      @sensitivity_chart_data, @sensitivity_chart_labels = @result.sensitivity_data_for_criterion(params[:criterion_id])
      @chart_data  = @result.chart_data
      @alternative_labels = @sensitivity_chart_labels
      @weight = @criterion.parent.privileged_evaluation_by(@participation_id).score_for_item(@criterion).weight_n
    end
  end

  def flow_diagram
    setup
    if params[:criterion_id]  # url is /criteria/criterion_id/results/sankey.json?p=participation_id
      @root = @article.criteria.find(params[:criterion_id])
    else                       # user is requesting root of hierarchy
      @root = @goal
    end
    @sankey_url = "/criteria/#{@root.id.to_s}/results/sankey.json?p=#{@participation_id}"
    @sankey_url += "&g=a" if @aggregate
  end

  def sankey
    setup
    if params[:criterion_id]  # url is /criteria/criterion_id/results/sankey.json?p=participation_id
      @root = @article.criteria.find(params[:criterion_id])
    else                      # user is requesting root of hierarchy
      @root = @goal
    end
    render json: @result.sankey_table(@root)
  end

private

  def check_completeness
    if params[:article_id]
      @article = Article.find(params[:article_id])
    elsif params[:criterion_id]
      criterion = Criterion.find params[:criterion_id]
      criterion.reload if criterion.article.nil?      # Why????
      @article = criterion.article
      # debugger
    end
    if params[:p]
      user = Participation.find(params[:p]).user
    end
    unless user
      if @article.participation_by current_user
        user = current_user
      else
        user = @article.user
      end
    end
    @participation = @article.participations.where(user_id:user.id).first
    if @participation.nil?
      flash[:error] = "No results found for this user"
      redirect_to :back
    else
      unless @participation.evaluation_complete?
        render template: 'results/incomplete'
      end
    end
  end

  def check_group_completeness
  end

  def setup
    authorize! :view, @article
    # @article and @participation defined in check_completeness
    @article_id = @article.id.to_s
    @participation_id = @participation.id
    @criteria = @article.criteria
    @goal = @criteria.root
    @alternatives = @article.alternatives
    @aggregate = true if params[:g]
    @result = Result.new @article, @goal, @participation, @aggregate
    if @result.nil?
      render template: 'results/incomplete'
    end
  end
  
  private
    def results_menu
      session[:link] = 'results'
    end

end