# Copyright (c) 2012 Medhi Widjaja

class AlternativesController < ApplicationController
  before_filter :disable_right_frame!, :enable_left_frame!
  # commented out for YC demo
  # before_filter :authenticate_user!
  before_filter :alternative_menu
  # before_filter :correct_user, except: [:index, :show]
  
  def index
    @article = Article.find(params[:article_id])
    authorize! :view, @article
    @article_id = @article.id.to_s 
    @alternatives = @article.alternatives.asc(:position)
    @common_properties = @article.common_properties

    respond_to do |format|
      format.html
      format.json { render json: @alternatives }
    end
  end

  def edit
    @article = Article.find(params[:article_id])
    authorize! :modify, @article
    @article_id = @article.id.to_s 
    @alternatives = @article.alternatives.asc(:position)
    @alternative = Alternative.find(params[:id])
    @properties = @alternative.properties
    @common_properties = @article.common_properties
  end

  def new
    @article = Article.find(params[:article_id])
    authorize! :modify, @article
    @article_id = @article.id.to_s 
    @alternative = @article.alternatives.build if user_signed_in?
    @article.common_properties.each do |cp|
      @alternative.properties.build(k: cp)
    end
    @alternatives = @article.alternatives.asc(:position)
  end

  def create
    @article = Article.find(params[:article_id])
    authorize! :modify, @article
    @article_id = @article.id.to_s 
    @alternative = @article.alternatives.build(params[:alternative])
    @alternatives = @article.alternatives.asc(:position)
    @alternative.position = @alternatives.count

    respond_to do |format|
      if @alternative.save
        @article.model_time_stamp
        format.html { redirect_to article_alternative_path(@article, @alternative), notice: 'Alternative was successfully created.' }
        format.json { render json: @alternative, status: :created, location: @alternative }
      else
        format.html { render action: "new" }
        format.json { render json: @alternative.errors, status: :unprocessable_entity }
      end
    end        
  end

  def update
    @alternative = Alternative.find(params[:id])
    @article = Article.find(params[:article_id])
    authorize! :modify, @article
    @alternatives = @article.alternatives.asc(:position)
    respond_to do |format|
      if @alternative.update_attributes(params[:alternative])
        @article.model_time_stamp
        flash[:notice] = 'Article was successfully updated.'
        format.html { redirect_to article_alternative_path(@article, @alternative) }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    article = Article.find(params[:article_id])
    authorize! :modify, article
    alternative = Alternative.find(params[:id])
    alternative.article.model_time_stamp
    unless alternative.product_id.nil?
      product = Product.find(alternative.product_id)
      product.decrease_use_count
    end
    alternative.destroy
    
    respond_to do |format|
      format.html { redirect_to article_alternatives_path(article) }
      format.json { head :no_content }
    end
  end

  def show
    @article = Article.find(params[:article_id])
    authorize! :view, @article
    @article_id = @article.id.to_s 
    @alternative = Alternative.find(params[:id])
    @alternative_id = params[:id]
    # @properties = @alternative.properties

    @alternatives = @article.alternatives.asc(:position)
    @common_properties = @article.common_properties

    @comments = @alternative.root_comments.desc(:created_at)
    @commentable = @alternative
    if user_signed_in?
      @comment = @alternative.comments.new user:current_user
    end    
    
    respond_to do |format|
      format.html # show.html.slim
      format.js
      format.json { render json: @alternative }
    end
  end

  def add_property_row
    article = Article.find(params[:article_id])
    authorize! :modify, article
    @alternative = Alternative.find(params[:id])
    @form_builder = nil
    respond_to do |format|
      format.js
    end
  end

  def search
    @article = Article.find(params[:article_id])
    authorize! :view, @article
    @article_id = @article.id.to_s
    @alternatives = @article.alternatives.asc(:position)
    @common_properties = @article.common_properties
    @search_string = params[:q]
    unless @search_string.blank?
      @products = Product.search @search_string, page: params[:page]
      respond_to do |format|
        format.html # search.html.erb
        format.js
      end
    else
      render nil
    end
  end

  def add
    @article = Article.find(params[:article_id])
    authorize! :modify, @article
    @article_id = @article.id.to_s
    @alternative = @article.add_product(params[:product][:id])
    @alternatives = @article.alternatives.asc(:position)
    @alternative.position = @alternatives.count

    respond_to do |format|
      if @alternative.save
        @article.model_time_stamp
        @product = Product.find(params[:product][:id])
        @product.increase_use_count
        format.html { redirect_to article_alternative_path(@article, @alternative), notice: 'Alternative was successfully created.' }
        format.json { render json: @alternative, status: :created, location: @alternative }
      else
        format.html { render action: "new" }
        format.json { render json: @alternative.errors, status: :unprocessable_entity }
      end
    end        
  end

private

  # Check if the user is allowed to perform action on the article
  def correct_user
    @article = Article.find(params[:article_id])
    unless @article.allow_edit? current_user
      flash[:error] = "You can only view this article." 
      redirect_to article_alternative_path(params[:article_id], params[:id])
    end
  end

  def alternative_menu
    session[:link] = 'alternatives'
  end
end
