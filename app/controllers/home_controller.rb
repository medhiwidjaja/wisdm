# Copyright (c) 2012 Medhi Widjaja

class HomeController < ApplicationController
  before_filter :enable_right_frame!, :disable_left_frame!

  def index
    # @users = User.all
    @new_and_updated =  Article.public.new_and_updated(30).paginate(page: params[:page])
    # @featured_articles = FeaturedArticle.current.desc(:expire_date)
    @featured_articles = FeaturedArticle.current.asc(:title)
    @featured_products = FeaturedProduct.current.desc(:expire_date)
    # @articles = Article.public.asc(:_id).paginate(page: params[:page])
  end

  def plans
    disable_left_frame!
    disable_right_frame!
  end
  
  def tree
    @article_id = "4fa1ee09463a9925d100005c"
    @article = Article.find(@article_id)
    @objective = @article.objectives.root
  end
end
