# Copyright (c) 2012 Medhi Widjaja

class ApplicationController < ActionController::Base
  protect_from_forgery

  # See http://www.ruby-forum.com/topic/168406
  def redirect_to(options = {}, response_status = {})
    if request.xhr?
      # render(:update) {|page| page.redirect_to(options)}
      render js: "window.location.pathname='#{options}'"
    else
      super(options, response_status)
    end
  end
  
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message
  end

  rescue_from Error404, :with => :render_404
  rescue_from Mongoid::Errors::DocumentNotFound, :with => :render_404 if Rails.env.production?

  def render_404
    Rails.logger.info "ROUTE NOT FOUND (404): #{request.url}"

    respond_to do |format|
      format.html { render "/public_errors/not_found", :status => '404 Not Found' }
      format.json { render :json => {:success => false, :message => "Not Found"}, :status => '404 Not Found' }
    end
  end

  # For layout
  def enable_left_frame!
    @has_left_frame = true
  end
  
  def disable_left_frame!
    @has_left_frame = false
  end

  def enable_right_frame!
    @has_right_frame = true
  end
  
  def disable_right_frame!
    @has_right_frame = false
  end

end
