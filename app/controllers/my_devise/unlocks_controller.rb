class MyDevise::UnlocksController < Devise::UnlocksController

  before_filter :set_layout

  def set_layout
    if user_signed_in?
      disable_right_frame!
      enable_left_frame!
    else
      enable_right_frame!
      disable_left_frame!
    end
  end
end