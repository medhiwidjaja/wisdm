class MyDevise::SessionsController < Devise::SessionsController

  before_filter :set_layout

  def set_layout
    if user_signed_in?
      disable_right_frame!
      enable_left_frame!
    else
      enable_right_frame!
      disable_left_frame!
    end
  end
  
  # def new
  #   super
  # end

  # def create
  #   resource = warden.authenticate!(auth_options)
  #   set_flash_message(:success, :signed_in) if is_navigational_format?
  #   sign_in(resource_name, resource)
  #   respond_with resource, :location => after_sign_in_path_for(resource)
  # end

  # # DELETE /resource/sign_out
  # def destroy
  #   redirect_path = after_sign_out_path_for(resource_name)
  #   signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))
  #   set_flash_message :success, :signed_out if signed_out

  #   # We actually need to hardcode this as Rails default responder doesn't
  #   # support returning empty response on GET request
  #   respond_to do |format|
  #     format.any(*navigational_formats) { redirect_to redirect_path }
  #     format.all do
  #       method = "to_#{request_format}"
  #       text = {}.respond_to?(method) ? {}.send(method) : ""
  #       render :text => text, :status => :ok
  #     end
  #   end
  # end
end