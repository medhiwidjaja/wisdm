# Custom devise registration controller
# I just want to change the flash message class from :notice to :success
#
# Don't forget to change the route.rb to point to the right controller
#
class MyDevise::RegistrationsController < Devise::RegistrationsController
  
  before_filter :set_layout

  def set_layout
    if user_signed_in?
      disable_right_frame!
      enable_left_frame!
    else
      enable_right_frame!
      disable_left_frame!
    end
  end

  # # POST /resource
  # def create
  #   build_resource

  #   if resource.save
  #     if resource.active_for_authentication?
  #       set_flash_message :success, :signed_up if is_navigational_format?
  #       sign_in(resource_name, resource)
  #       respond_with resource, :location => after_sign_up_path_for(resource)
  #     else
  #       set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_navigational_format?
  #       expire_session_data_after_sign_in!
  #       respond_with resource, :location => after_inactive_sign_up_path_for(resource)
  #     end
  #   else
  #     clean_up_passwords resource
  #     respond_with resource
  #   end
  # end

  # def update
  #   self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)

  #   if resource.update_with_password(params[resource_name])
  #     if is_navigational_format?
  #       if resource.respond_to?(:pending_reconfirmation?) && resource.pending_reconfirmation?
  #         flash_key = :update_needs_confirmation
  #       end
  #       set_flash_message :success, flash_key || :updated
  #     end
  #     sign_in resource_name, resource, :bypass => true
  #     respond_with resource, :location => after_update_path_for(resource)
  #   else
  #     clean_up_passwords resource
  #     respond_with resource
  #   end
  # end

  # # DELETE /resource/sign_out
  # def destroy
  #   resource.destroy
  #   Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name)
  #   set_flash_message :success, :destroyed if is_navigational_format?
  #   respond_with_navigational(resource){ redirect_to after_sign_out_path_for(resource_name) }
  # end
end 