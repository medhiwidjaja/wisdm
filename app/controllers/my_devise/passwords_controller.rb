class MyDevise::PasswordsController < Devise::PasswordsController

  before_filter :disable_left_frame!, :enable_right_frame!
end