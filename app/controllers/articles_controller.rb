# Copyright (c) 2012 Medhi Widjaja

require 'will_paginate/array'

class ArticlesController < ApplicationController
  before_filter :authenticate_user!, except: [:index, :new_and_updated, :featured, :show]
  before_filter :article_menu
  before_filter :disable_right_frame!, only: [:show, :create, :new, :edit, :edit_common_properties]
  before_filter :disable_left_frame!, except: [:show, :create, :new, :edit, :edit_common_properties] 

  # GET /articles
  # GET /articles.json
  # def index
  #   @articles = Article.public.asc(:_id).paginate(page: params[:page])
    
  #   respond_to do |format|
  #     format.html # index.html.erb
  #     format.json { render json: @articles }
  #   end
  # end

  # TODO: filter only id, title, description, user (as necessary)
  def index
    terms = params[:q]
    unless terms.blank?
      @articles = Article.search page: params[:page] do
        query { string terms }
        filter :terms, :visibility => ['public']
      end
      @terms = terms
      respond_to do |format|
        format.html # search.html.erb
        format.json do
          json_array = @articles.collect do |n|
            n.as_json.merge Hash["img" => n.load.pic_snippet]  
            # we have to load the item from the database, because @articles returned by Tire
            # doesn't contain 'real' articles
          end
          if params[:callback]
            render json: json_array, callback: params[:callback]
          else
            render json: json_array
          end
        end
      end
    end
    @terms = terms
  end

  def new_and_updated
    @new_and_updated =  Article.public.new_and_updated(30).paginate(page: params[:page])
    json_array = @new_and_updated.collect do |n|
      n.as_json.merge Hash["img" => n.pic_snippet]
    end

    respond_to do |format|
      format.html # new_and_updated.html.slim
      format.json do 
        if params[:callback]
          render json: json_array, callback: params[:callback]
        else
          render json: json_array
        end
      end
    end
  end    

  def featured
    @featured = FeaturedArticle.current.desc(:expire_date).paginate(page: params[:page])
    respond_to do |format|
      format.html 
      format.json { render json: @featured }
    end
  end

  def top
    # TODO: sort and filter by some criteria
    @articles = Article.public.paginate(page: params[:page])
    respond_to do |format|
      format.html 
      format.json { render json: @featured }
    end
  end


  # GET /articles/1
  # GET /articles/1.json
  def show
    @article = Article.without(:aggregate).find(params[:id])
    authorize! :read, @article
    @participations = @article.participations.all
    @article_id = @article.id
    @alternatives = @article.alternatives
    @comments = @article.root_comments.desc(:created_at)
    @commentable = @article
    if user_signed_in?
      @comment = @article.comments.new user:current_user
    end
    respond_to do |format|
      format.html # show.html.erb
      # format.json { render json: @article }
      format.json { render json: {id:@article.slug, title: @article.title, description:@article.description, author:@article.user.id} }
    end
  end

  # GET /articles/new
  # GET /articles/new.js
  def new
    @article = current_user.articles.build
    # Set default visibility: 'public' for Free account, and 'private' for others
    @article.visibility = current_user.account == 'free' ? 'public' : 'private'
    authorize! :create, @article
    @user = @article.user
    @categories = Category.all
    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /articles/1/edit
  def edit
    @article = Article.find(params[:id])
    authorize! :update, @article
    @article_id = @article.id.to_s
    @common_property = @article.common_properties
    # @participants = @article.participations.collect { |p| p.user }
    @participations = @article.participations.all
    @categories = Category.all
    # session[:article_id] = @article.id
  end

  # POST /articles
  # POST /articles.json
  def create
    @article = current_user.articles.build(params[:article])
    authorize! :create, @article
    @user = @article.user
    @categories = Category.all
    respond_to do |format|
      if @article.save
        format.html { redirect_to article_path(@article.slug), notice: 'Article was successfully created.' }
        format.js { redirect_to article_path(@article.slug), notice: 'Article was successfully created.' }
        # format.json { render json: @article, status: :created, location: @article }
        format.json do
          if params[:callback]
            render json: @article, status: :created, location: @article, callback: params[:callback]
          else
            render json: @article, status: :created, location: @article
          end
        end
      else
        format.html { render action: "new" }
        format.js { render action: "new" }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /articles/1
  # PUT /articles/1.json
  def update
    @article = Article.find(params[:id])
    authorize! :update, @article
    @categories = Category.all
    respond_to do |format|
      if @article.update_attributes(params[:article])
        format.html { redirect_to @article, notice: 'Article was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /articles/1
  # DELETE /articles/1.json
  def destroy
    @article = Article.find(params[:id])
    authorize! :destroy, @article
    @article.destroy

    respond_to do |format|
      format.html { redirect_to my_articles_path }
      format.json { head :no_content }
    end
  end

  def my_articles
    @user = current_user
    authorize! :read, @user
    @articles = @user.articles.desc(:updated_at).paginate(page: params[:page])
    #TODO: Optimize! cache list in an array stored in User class
    # using @user.articles.desc(:updated_at).map {|a| {label:a.title, id:a.to_param}}
    # update every time Article is saved (after_save :update_list)
  end

  def participating
    @user = current_user
    authorize! :read, @user
    respond_to do |format|
      format.html do # participating.html.slim
        @articles = Participation.where(user:@user).all_active.desc(:updated_at)
                      .collect{ |p| p.article unless p.article.nil? || p.article.user == @user }
                      .compact.paginate(page: params[:page])
      end
      # format.json do # participating.json.rabl
      #   @articles = Participation.where(user:@user).all_active.desc(:updated_at).limit(20)
      #                 .collect{ |p| p.article unless p.article.nil? || p.article.user == @user }
      #                 .compact
      # end
    end
  end

  def watch_list
    @user = current_user
    authorize! :read, @user
    @articles = @user.watch_list.articles.desc(:updated_at).paginate(page: params[:page])
    respond_to do |format|
      format.html  # watch_list.html.slim
      format.json do
        if params[:callback]
          render json: @articles, callback: params[:callback]
        else
          render json: @articles
        end
      end
    end
  end

  def add_property
    @article = Article.find(params[:article_id])
    authorize! :modify, @article
    @article_id = @article.id.to_s
    @property = params[:property]
    @commit = params[:commit]
    @article.add_common_property(@property) unless @commit == "Cancel" || @property.blank?
    respond_to do |format|
      format.js 
    end
  end

  def common_properties
    @article = Article.find(params[:article_id])
    authorize! :view, @article
    @property = params[:property]
    @alternatives = @article.alternatives.asc(:position)
    @alternatives.asc(:position).each do |alt|
      alt.properties.build(k: @property)
    end
    respond_to do |format|
      format.js
      format.html
    end
  end

  def update_common_properties
    article = Article.find(params[:article_id])
    authorize! :modify, article
    property = params[:property]
    alternatives = article.alternatives
    cost = params[:cost] == "yes"    
    entries = params[:alternatives]

    entries.each do |entry|
      alt_id = entry.first
      value = entry.last  
      p = alternatives.find(alt_id).properties.where(k: property).first
      if p 
        p.set(:v, value)
        p.set(:cost, cost)
      else
        alternatives.find(alt_id).properties.create(k: property, v: value, cost: cost)
      end
    end
    article.model_time_stamp

    flash[:notice] = "Common properties for '#{property}' were successfully saved."
    redirect_to article_alternatives_path(article.id)
  end

  def edit_common_properties
    @article = Article.find(params[:article_id])
    authorize! :modify, @article
    @alternatives = @article.alternatives
    @common_properties = @article.common_properties
  end

  def save_common_properties
    @article = Article.find(params[:article_id])
    authorize! :update, @article

    respond_to do |format|
      if @article.update_attributes(params[:article])
        format.html { redirect_to article_alternatives_path, notice: 'Common properties were updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit_common_properties" }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  def add_alternative
  end

  def update_alternatives_order
    article = Article.find(params[:article_id])
    authorize! :modify, article
    article.attributes = params[:article]
    render nothing: true
  end

  def show_participants
    @article = Article.find(params[:article_id])
    @participants = @article.participations
  end

  def like
    @article = Article.find(params[:article_id])
    current_user.like! @article
    respond_to do |format|
      format.html { redirect_to @article }
      format.js
    end
  end
  
  # def copy_form
  #   @article = Article.find_by_id_or_slug(params[:id])
  #   @alternatives = @article.alternatives
  #   @objectives = @article.objectives
  # end

private

  def article_menu
    session[:link] = 'articles'
  end
end
