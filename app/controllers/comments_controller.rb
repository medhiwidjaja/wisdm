# Copyright (c) 2012 Medhi Widjaja

class CommentsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :setup

  def edit
    @comment = Comment.find(params[:id])
    respond_to do |format|
      format.js
    end
  end

  def create
    @comment = Comment.new params[:comment]
    @comment.user = current_user
    @commentable.comments << @comment

    respond_to do |format|
      format.js
    end
  end

  def update
    @comment = Comment.find(params[:id])
    @comment.attributes = params[:comment]
    @comment.save!

    respond_to do |format|
      format.js
    end
  end

  def new
    @comment = Comment.build 
    respond_to do |format|
      format.js
    end
  end

  def destroy
  end

  def new_reply
    @parent_id = params[:id]
    @comment = @commentable.comments.new user:current_user
    @comment.parent_id = @parent_id
    respond_to do |format|
      format.js
    end
  end

  def reply
    @parent_id = params[:id]
    @comment = Comment.new params[:comment]
    @comment.user = current_user
    @commentable.comments << @comment

    respond_to do |format|
      format.js
    end
  end

  private

  def setup
    if params[:article_id]
      @commentable = Article.find(params[:article_id])
      @article = @commentable
    elsif params[:alternative_id]
      @commentable = Alternative.find(params[:alternative_id])
      @article = @commentable.article
    elsif params[:criterion_id]
      @commentable = Criterion.find(params[:criterion_id])
      @article = @commentable.article
    end
    authorize! :read, @article
  end
end