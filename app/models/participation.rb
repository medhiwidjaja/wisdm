# Copyright (c) 2012 Medhi Widjaja

class Participation
  include Mongoid::Document
  include Mongoid::Timestamps
  include AnalysisMethods::ValueTreeExt

  field :invited,    type: Boolean, default: false   # by article author
  field :requesting, type: Boolean, default: false   # by other users
  field :approved,   type: Boolean, default: false   # from a request
  field :confirmed,  type: Boolean, default: false   # from an invitation
  field :rejected,   type: Boolean, default: false   # from a request
  field :declined,   type: Boolean, default: false   # from an invitation
  field :active,     type: Boolean, default: false   # default true after approved or confirmed
  field :aggregate,  type: Boolean, default: false   # denoting that this is group's aggregate
  field :weight,     type: Float
  field :weight_n,   type: Float                     # normalized weight
  field :permissions, :type => Array, default: ['compare', 'rate', 'view']
  field :tree,       type: String
  field :tree_ts,    type: Time
  field :vtdump,     type: Array    # marshal dump of ValueTree
  field :vtdump_ts,  type: Time, default: -> { self.updated_at }

  PERMISSIONS = %w[author modify compare rate analyze view]
  PERMISSIONS_DESCRIPTIONS = {
    author:   "Modify everything (incl. deleting the article), invite and approve new participants",
    modify:   "Modify (edit, add, delete) alternatives and criteria",
    compare:  "Assign weights to criteria",
    rate:     "Assign scores to alternatives with respect to the criteria",
    analyze:  "View result's analysis and reports",
    view:     "View result's ranking"
  }

  class << self
    def with_permissions(*args)
      any_in(:permissions => args)
    end
  end

  has_many :evaluations, dependent: :destroy

  belongs_to :user
  index({user_id: 1})
  field :username     # Denormalization, should be updated when user.name is changed
  before_update :update_username

  belongs_to :article
  index({article_id: 1})

  index({article_id: 1, user_id: 1}, {unique: true})

  default_scope                asc(:created_at)
  scope :all_active,           where(active: true)
  scope :invitation_confirmed, where(invited: true).and(confirmed: true)
  scope :request_approved,     where(requesting: true).and(approved: true)
  scope :aggregate,            where(aggregate: true)

  def has_incomplete_comparison?
    self.article.criteria.root.traverse {|c| c.comparison_by(self.id).nil? }.reduce(:|)
  end

  def ratings_incomplete?
    self.article.criteria.leaves.collect {|c| c.has_incomplete_ratings_by? self.id }.reduce(:|)
  end

  def evaluations_incomplete?
    self.article.criteria.root.traverse {|c| c.has_incomplete_scores_by? self.id }.reduce(:|)
  end

  # Completeness check:
  def comparison_complete?
    self.article.has_alternative? and self.article.has_criteria? and 
      !self.evaluations_incomplete? and !self.ratings_incomplete?
  end

  def evaluation_complete?
    self.article.has_alternative? and self.article.has_criteria? and
      !self.evaluations_incomplete? and !self.ratings_incomplete?
  end

  def status
    if article.user == user
      'Author'
    elsif requesting 
      if approved
        'Request approved'
      elsif rejected
        'Request not approved'
      else
        'Waiting approval'
      end
    elsif invited
      if confirmed
        'Invitation confirmed'
      elsif declined
        'Invitation declined'
      else
        'Waiting confirmation'
      end
    end
  end

  def waiting_approval?
    requesting && !(approved || rejected)
  end

  def waiting_confirmation?
    invited && !(confirmed || declined)
  end

  def vtdump_is_stale?
    vtdump_ts.nil? || article.model_updated_at > vtdump_ts
  end

  def tree_is_stale?
    tree_ts.nil? || article.model_updated_at > tree_ts
  end


  # Note: to check a user's permissions:
  # article.participations.where(user_id: user.id).all(permissions:["analyze","author"])

  private
 
    def update_username
      self.username = User.find(self.user_id).name
    end

 end

















