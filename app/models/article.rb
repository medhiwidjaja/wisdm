# Copyright (c) 2012 Medhi Widjaja

class Article
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Slug
  include Mongoid::Document::Taggable
  include Mongoid::Commentable
  
  #include Mongoid::MultiParameterAttributes

  field :title, type: String
  field :description, type: String
  field :common_properties, type: Array, default: []
  field :model_updated_at, type: Time
  field :allow_copying, type: Boolean
  field :allow_participation, type: Boolean
  field :comparison_method, type: Integer
  field :visibility, type: String       # public, internal, private
  field :like_list, type: Array, default: []
  field :likes, type: Integer, default: 0
  field :recalc, type: Boolean, default: true    # whether the users needs to redo the evaluations (maybe because a criterion/alternative is deleted_)
  slug  :title

  attr_accessible :title, :description, :category_id, :alternatives_attributes, :criteria_attributes
  attr_protected  :user_id, :slug

  validates_presence_of :title, :user_id, :visibility

  field :category_id, type: String
  belongs_to :category, index: true

  belongs_to :user, inverse_of: :articles, index: true

  has_many :featured_dates, class_name: "FeaturedArticle", validate: false, dependent: :destroy
  has_many :criteria, dependent: :destroy
  accepts_nested_attributes_for :criteria, allow_destroy: false
  has_many :alternatives, dependent: :destroy
  accepts_nested_attributes_for :alternatives, allow_destroy: false
  has_many :participations, dependent: :destroy
  accepts_nested_attributes_for :participations, allow_destroy: false
  
  has_and_belongs_to_many :watch_lists, index: true
  field :watch_count, type: Integer, default: 0

  embeds_one :aggregate
  
  default_scope without(:aggregate)
  scope :new_and_updated, ->(n) { all_of(:updated_at.gt => n.days.ago).desc(:updated_at) }
  scope :public, where( visibility: 'public' )
  
  index({title:1, description:1})

  after_create :participate

  # Tire elasticsearch 
  include Tire::Model::Search
  include Tire::Model::Callbacks

  mapping do
    indexes :id,           :index    => :not_analyzed
    indexes :title,        :analyzer => 'snowball', :boost => 100
    indexes :desciption,   :analyzer => 'snowball'
    indexes :tags,         :analyzer => 'keyword'
  end    

  def self.private
    where( :visibility.ne => 'public' )
  end

  def private
    where( :visibility.ne => 'public' )
  end

  # For Tire:
  def to_indexed_json
    self.to_json
  end

  def participation_by(user, has_to_be_active=true)
    if user.class == User
      user_id = user.id
    elsif user.class == Moped::BSON::ObjectId or user.class == String
      user_id = user
    end  
    if has_to_be_active
      self.participations.where(user_id:user_id).all_active.first
    else
      self.participations.where(user_id:user_id).first
    end    
  end

  def has_alternative?
    !alternatives.empty?
  end

  def has_objective?
    !objectives.empty?
  end

  def has_criteria?
    !criteria.empty?
  end

  def need_recalc?
    recalc
  end
  
  def featured_now?
    FeaturedArticle.exists?(conditions: { article_id: self.id, :start_date.lte => Time.now.utc, :expire_date.gte => Time.now.utc })
  end

  def feature_now!(expire_date=7.days.from_now)
    self.featured_dates.create start_date:Time.now.utc, expire_date:expire_date
  end

  # Visibility
  def public?
    visibility == 'public'
  end

  def private?
    visibility == 'private'
  end

  def internal?
    visibility == 'internal'
  end

  # Completeness check:
  def eval_complete?
    has_alternative? and has_objective? and 
      !objectives.root.leaf_nodes.any? {|o| o.has_incomplete_evaluations? } and 
      !objectives.any? {|o| o.has_incomplete_weights? }
  end

  # Add or insert a property into the common_property array
  # Default is inserting it at the end
  def add_common_property(property, position = -1)
    if position == -1
      self.push :common_properties, property
    elsif position < self.common_properties.count
      cp = self.common_properties
      cp.insert(position, property)
      self.set(:common_properties, cp)
    end
  end

  def delete_common_property(position)
    cp = self.common_properties
    cp.delete_at(position)
    self.set(:common_properties, cp)
  end

  def add_product(product_id)
    product = Product.find(product_id)
    n = 0
    parameters = {
      "title"       => product.title,
      "product_id"  => product.id,
      "abbrev"      => product.abbrev,
      "description" => product.description,
      "properties_attributes" => product.properties.reduce({}) {|h, pp| h.merge({(n+=1).to_s => { 'k' => pp.k, 'v' => pp.v, 'unit' => pp.unit } } ) }
    }
    # {"title"=>"Alternative 2", "product_id"=>"1", "abbrev"=>"Alt 2", "description"=>"A short description", "properties_attributes"=>{"1358869518168"=>{"k"=>"Level", "v"=>"1", "unit"=>"dB", "_destroy"=>"false"}, "1358869532043"=>{"k"=>"Noise", "v"=>"3", "unit"=>"dB", "_destroy"=>"false"}}}
    self.alternatives.build parameters
  end

  def model_time_stamp
    self.touch :model_updated_at
  end

  def allow_edit?(this_user)
    self.participations.where(user_id: this_user.id, active: true).exists?
    # this_user == self.user
  end

  def like(user)
    self.push like_list: user.id
    self.inc likes: 1
  end

  def description_snippet(len=100)
    unless self.description.blank?
      texts = Nokogiri::HTML(self.description).at_css('body').children.collect {|u| u.text unless u.text.empty? }.compact
      '<p>'+texts.join('</p><p>').truncate(len)+'</p>'
    end
  end

  def pic_snippet
    unless self.description.blank?
      pic_node = Nokogiri::HTML(self.description).at_css('img')
      pic_node[:src] unless pic_node.nil?
    end
  end

  def initialize_criteria_tree
    root = self.criteria.create! title:self.title
    unless common_properties.blank?
      common_properties.each do |common_property|
        subcriterion = self.criteria.build title:common_property
        root.children << subcriterion
      end
    end
    root
  end

private

  def participate
    self.participations.create user:self.user, active:true, permissions: ['author']
    self.model_time_stamp
  end

  # NOTE: this is just a quick fix. The Mongoid_slug gem's contributors seems
  # to be making progress of making find method to work with slugs or ids.
  # So this method will no longer be necessary when that happens
  def self.find_by_id_or_slug(str)
    # self.where(_id:str).first || self.find_by_slug(str)
    self.find str
  end
end
