# Copyright (c) 2012 Medhi Widjaja

# See this article at StackOverflow http://stackoverflow.com/questions/4190578/rails-devise-preventing-a-user-from-changing-their-email-address
class ImmutableValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    record.errors[attribute] << "cannot be changed after creation" if record.send("#{attribute}_changed?") && !record.new_record?
  end
end

class User
  include Mongoid::Document
  include Mongoid::Timestamps

  attr_accessible :name, :email, :password, :password_confirmation, :remember_me, :account, :bio, :location, :website, :roles

  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  ## Database authenticatable
  field :email,               type: String
  field :encrypted_password,  type: String, default: ""
  field :name
  field :bio,                 type: String
  field :website,             type: String
  field :location,            type: String
  field :birthday,            type: Time
  field :last_logged_at,      type: Time

  field :preferred_languages, type: Set, :default => []

  field :language,            type: String, :default => "en"
  index ({language: 1})
  field :timezone,            type: String
  # field :language_filter,     type: String, :default => "user", :in => LANGUAGE_FILTERS

  field :ip,                  type: String
  field :country_code,        type: String
  field :country_name,        type: String, :default => "unknown"
  field :hide_country,        type: Boolean, :default => false

  # Account type: free, basic, academic, pro, enterprise
  field :account,             type: String, default: "free"  

  # Roles: guest, user, editor, admin
  field :roles,               type: Array, default: ["guest"]

  belongs_to :friend_list
  field :followers_count,     type: Integer, default: 0
  field :following_count,     type: Integer, default: 0
  field :friend_list_id,      type: String
  index({friend_list_id: 1})
  
  belongs_to :watch_list
  field :watch_count,         type: Integer, default: 0
  index({watch_list_id: 1})

#  belongs_to :product_watch_list
  field :product_watch_list,  type: Array, default: []
  field :product_watch_count, type: Integer, default: 0
#  index({product_watch_list_id: 1})

  # A user may participate in many decisions
  has_many :participations, dependent: :destroy, validate: false
  field :join_count,          type: Integer, default: 0

  field :use_gravatar,        type: Boolean, default: true

  has_many :articles, validate: false   
    # Note: there's no dependent: :destroy
    # I don't think it's right to delete article's when it contains other people's comparisons
    # if the user is deleted.

  has_and_belongs_to_many :catalogs, class_name: 'Catalog', inverse_of: :editors

  has_and_belongs_to_many :memberships, class_name: 'Organization', inverse_of: :members
  
  has_many :organizations, inverse_of: :owner

  has_many :notifications, dependent: :destroy
  has_many :comments, dependent: :destroy, validate: false

  has_many :products
  
  validates :name,  presence: true, 
                    uniqueness: { case_sensitive: false }
  validates :email, presence: true,
                    uniqueness: { case_sensitive: false }
  validates :email, immutable: true

  validates_length_of :bio, maximum: 2.kilobytes

  index({ email: 1 }, { unique: true, name: "email_index" })

  scope :all_admins, where(role: 'admin')

  ## Recoverable
  field :reset_password_token,   type: String
  field :reset_password_sent_at, type: Time

  ## Rememberable
  field :remember_created_at, type: Time

  ## Trackable
  field :sign_in_count,      type: Integer, default: 0
  field :current_sign_in_at, type: Time
  field :last_sign_in_at,    type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip,    type: String

  ## Encryptable
  # field :password_salt, type: String

  ## Confirmable
  field :confirmation_token,   type: String
  field :confirmed_at,         type: Time
  field :confirmation_sent_at, type: Time
  field :unconfirmed_email,    type: String # Only if using reconfirmable

  ## Lockable
  # field :failed_attempts, type: Integer, default: 0 # Only if lock strategy is :failed_attempts
  # field :unlock_token,    type: String # Only if unlock strategy is :email or :both
  # field :locked_at,       type: Time

  ## Token authenticatable
  # field :authentication_token, type: String

  after_update  :update_denormalized_fields

  # Tire elasticsearch 
  include Tire::Model::Search
  include Tire::Model::Callbacks

  mapping do
    indexes :id,           :index    => :not_analyzed
    indexes :name,         :analyzer => 'snowball', :boost => 100
    indexes :email,        :analyzer => 'snowball'
  end    

  # These before_create and after_create cause Devise to send multiple confirmation emails to the user 
  # Solution according to: http://stackoverflow.com/questions/13061775/devise-actionmailer-sending-duplicate-emails-for-registration-confirmation
  # is to override confirm!
  # before_create :initialize_fields
  # after_create  :set_user_role

  def confirm!
    initialize_fields
    super
    set_user_role
  end

  ## Related to Following / Followers:

  def follow!(other_user)
    return false if other_user == self
    FriendList.where(_id: self.friend_list_id).add_to_set(:following_ids, other_user.id)
    FriendList.where(_id: other_user.friend_list_id).add_to_set(:follower_ids, self.id)

    self.inc(:following_count, 1)
    other_user.inc(:followers_count, 1)
    true
  end

  def unfollow!(other_user)
    return false if other_user == self
    FriendList.where(_id: self.friend_list_id).pull(:following_ids, other_user.id)
    FriendList.where(_id: other_user.friend_list_id).pull(:follower_ids, self.id)

    self.inc(:following_count, -1)
    other_user.inc(:followers_count, -1)
    true
  end

  def followers
    # User.where(:_id.in => self.friend_list.follower_ids)
    # Eager load the data:
    self.friend_list.followers.map { |u| u }
    # Note: 
    #  The above is much faster than the following query:
    #     User.find(self.friend_list.follower_ids)
    #  Do the following simplistic benchmark in the console:
    # 
    #   s=Time.now; 100.times { a = User.find(dr.friend_list.following_ids)}; e=Time.now; e-s
    #  Compare it with
    #   s=Time.now; 100.times { b = dr.friend_list.following.map {|a| a} }; e=Time.now; e-s
  end

  def following
    # User.where(:_id.in => self.friend_list.following_ids)
    # Eager load the data:
    self.friend_list.following.map {|u| u }
  end

  def following?(other_user)
    FriendList.only(:following_ids).where(_id: self.friend_list_id).first.following_ids.include?(other_user.id)
  end

  ## Related to Article watching

  def watches?(article)
    self.create_watch_list if self.watch_list.nil?
    self.watch_list.article_ids.include?(article.id)
  end

  def watch!(article)
    # just silently ignore it when the user already watches the article
    unless self.watch_list.articles.include? article
      self.watch_list.add_to_set :article_ids, article.id
      self.inc(:watch_count, 1)
      article.add_to_set :watch_list_ids, self.watch_list_id
      article.inc(:watch_count, 1)
      true
    else
      false
    end   
  end

  def unwatch!(article)
    if self.watch_list.article_ids.include? article.id
      self.watch_list.article_ids.delete article.id
      self.inc(:watch_count, -1)
      article.watch_list_ids.delete self.watch_list_id
      article.inc(:watch_count, -1)
      true
    else
      false
    end
  end

  def like!(article)
    # just silently ignore it when the user already likes the article
    unless article.like_list.include? self.id
      article.add_to_set :like_list, self.id
      article.inc(:likes, 1)
    else
      false
    end   
  end

  def likes?(article)
    article.like_list.include? self.id
  end

  ## Related to Product watching

  # def tracks?(product)
  #   self.create_product_watch_list if self.product_watch_list.nil?
  #   self.product_watch_list.product_ids.include?(product.id)
  # end
  
  # def track!(product)
  #   # just silently ignore it when the user already tracks the product
  #   unless self.product_watch_list.products.include? product
  #     self.product_watch_list.add_to_set :product_ids, product.id
  #     self.inc(:product_watch_count, 1)
  #     product.add_to_set :product_watch_list_ids, self.watch_list_id
  #     product.inc(:watch_count, 1)
  #     true
  #   else
  #     false
  #   end   
  # end

  # def untrack!(product)
  #   if self.product_watch_list.product_ids.include? product.id
  #     self.product_watch_list.product_ids.delete product.id
  #     self.inc(:product_watch_count, -1)
  #     product.watch_list_ids.delete self.watch_list_id
  #     product.inc(:watch_count, -1)
  #     true
  #   else
  #     false
  #   end
  # end

  def tracks?(product)
    product.watch_list.include? self.id
  end
  
  def track!(product)
    # just silently ignore it when the user already tracks the product
    unless product_watch_list.include? product.id
      product.add_to_set :watch_list, self.id
      product.inc(:watch_count, 1)
      self.add_to_set :product_watch_list, product.id
      self.inc(:product_watch_count, 1)
      true
    else
      false
    end   
  end

  def untrack!(product)
    if product_watch_list.include? product.id
      product.pull :watch_list, self.id
      product.inc(:watch_count, -1)
      self.pull :product_watch_list, product.id
      self.inc(:product_watch_count, -1)
      true
    else
      false
    end
  end

  def like_this!(product)
    # just silently ignore it when the user already likes the product
    unless product.like_list.include? self.id
      product.add_to_set :like_list, self.id
      product.inc(:likes, 1)
    else
      false
    end   
  end

  def unlike_this!(product)
    if product.like_list.include? self.id
      product.pull :like_list, self.id
      product.inc(:likes, -1)
    else
      false
    end   
  end

  def likes_this?(product)
    product.like_list.include? self.id
  end

  ## Related to notifications
  
  def pending_notifications?
    self.notifications.where(state:'pending').exists?
  end

  ## Roles and Ability

  def has_role?(role_sym)
    self.roles.include? role_sym.to_s
  end

  def admin?
    self.has_role? :admin
  end
  
  def ability
    @ability ||= Ability.new(self)
  end

  delegate :can?, :cannot?, to: :ability

  def participation_in(article)
    article.participations.where(user:self)
  end

  # Check user's permissions on an article, whether it includes certain permissions
  # Example:  user.permissions_in an_article, include: ['compare', 'rate']
  def permissions_in(article, hash)
    participation_in(article).all_active.all(permissions: hash[:include]).exists?
  end

  # Organizations
  def member_of?(org)
    membership_ids.include? org.id
  end

  ## Others

  def email=(value)
    write_attribute :email, (value ? value.downcase : nil)
  end
  
  def self.find_by_email(email)
    where(email: email.downcase).first
  end  

  def gravatar_url(options = { size: 50 })
    size = options[:size]
    gravatar_id = Digest::MD5::hexdigest(self.email.downcase)
    "http://gravatar.com/avatar/#{gravatar_id}.png?s=#{size}"
  end

  def profile_info
    user_profile = { id: self.id, 
      name: self.name, 
      email: self.email, 
      bio: self.bio,
      account: self.account,
      roles: self.roles,
      website: self.website,
      location: self.location,
      country_code: self.country_code,
      country_name: self.country_name,
      gravatar_url: self.gravatar_url,
      organizations: self.organizations.collect {|o| {slug:o.slug, name:o.name}}
    }
  end

private

  def initialize_fields
    self.friend_list = FriendList.create if self.friend_list.nil?
    self.create_watch_list if self.watch_list.nil?
    self.create_product_watch_list if self.product_watch_list.nil?
  end

  def update_denormalized_fields
    if self.name_changed?
      Participation.where(user_id:self.id).update_all username: self.name
    end
  end

  def set_user_role
    self.update_attribute :roles, ['member']
  end
end
