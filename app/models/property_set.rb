# Copyright (c) 2012 Medhi Widjaja

class PropertySet
  include Mongoid::Document

  field :title
  field :description
  field :language, type: String, default: 'en'
  field :country
  
  has_many :properties, as: :propertiable
end