class Catalog
  include Mongoid::Document
  include Mongoid::Timestamps

  field :title,       type: String
  field :description, type: String
  field :visibility,  type: Boolean
  field :tags,        type: String

  belongs_to              :organization
  has_and_belongs_to_many :editors, class_name: 'User', inverse_of: :editor

  has_and_belongs_to_many :products, index: true
  has_and_belongs_to_many :product_groups, index: true
  
end
