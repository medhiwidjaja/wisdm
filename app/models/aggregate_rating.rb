# Copyright (c) 2012 Medhi Widjaja

class AggregateRating
  include Mongoid::Document

  field :weight,    type: Float
  field :weight_n,  type: Float
  field :weight_g,  type: Float
  field :magiq_order, type: Integer
  
  belongs_to :alternative, index: true
  belongs_to :criterion, index:true       # with respect to some Criterion

  index({alternative_id: 1, criterion_id: 1}, {unique: true})

end