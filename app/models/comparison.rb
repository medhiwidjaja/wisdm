# Copyright (c) 2012 Medhi Widjaja

# Deprecated !

class Comparison
  include Mongoid::Document
  include DecisionAnalysis::Base
  include DecisionAnalysis::Smart
  include DecisionAnalysis::Magiq
  include DecisionAnalysis::Ahp

  belongs_to :comparable, polymorphic: true, index: true
  belongs_to :participation, index: true
  
  scope :complete, ne(weight_n: nil)
  index({comparable_id: 1, participation_id: 1}, {unique: true})

  def method_is_smart?
    self.type == 2
  end

  def method_is_magiq?
    self.type == 1
  end

  def method_is_ahp?
    self.type == 3
  end

  def set_method_smart
    self.set(:type, 2)
  end

  def set_method_magiq
    self.set(:type, 1)
  end
  
  def set_method_ahp
    self.set(:type, 3)
  end
end