# Copyright (c) 2012 Medhi Widjaja

class AggregateComparison
  include Mongoid::Document
  include DecisionAnalysis::Base
  include DecisionAnalysis::Smart
  include DecisionAnalysis::Magiq
  include DecisionAnalysis::Ahp

  belongs_to :criterion, index: true

  def method_is_smart?
    self.type == 2
  end

  def method_is_magiq?
    self.type == 1
  end

  def method_is_ahp?
    self.type == 3
  end

  def set_method_smart
    self.set(:type, 2)
  end

  def set_method_magiq
    self.set(:type, 1)
  end
  
  def set_method_ahp
    self.set(:type, 3)
  end
end
