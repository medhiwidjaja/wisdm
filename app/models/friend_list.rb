# Copyright (c) 2012 Medhi Widjaja

class FriendList
  include Mongoid::Document

  # identity type: String
  field :_id,
    type: String,
    pre_processed: true,
    default: ->{ Moped::BSON::ObjectId.new.to_s }
    
  has_one :user, :validate => false

  # field :follower_ids, type: Array, :default => []
  has_and_belongs_to_many :followers, :class_name => "User",
      :inverse_class_name => "User", :validate => false
  index({follower_ids: 1})

  # field :following_ids, type: Array, :default => []
  has_and_belongs_to_many :following, :class_name => "User",
      :inverse_class_name => "User", :validate => false
  index({following_ids: 1})
end