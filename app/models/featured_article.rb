# Copyright (c) 2012 Medhi Widjaja

class FeaturedArticle
  include Mongoid::Document
  include Mongoid::MultiParameterAttributes
  
  field :start_date, type: Date
  index({start_date: 1}) 

  field :expire_date, type: Date
  index({expire_date: 1})

  belongs_to :article, :validate => false, index: true

  scope :current, -> { all_of(:expire_date.gte => Time.now.utc, :start_date.lte => Time.now.utc) }
  scope :expired, -> { all_of(:expire_date.lt => Time.now.utc) }
end
