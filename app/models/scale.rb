# Copyright (c) 2012 Medhi Widjaja

class Scale
  include Mongoid::Document

  field :title, type: String
  field :description, type: String
  field :scale, type: Array
end