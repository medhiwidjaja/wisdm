# Copyright (c) 2012 Medhi Widjaja

class Product
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Slug

  field :title, type: String
  field :abbrev, type: String, default: nil
  field :description, type: String
  field :official, type: Boolean
  field :website, type: String
  field :company, type: String
  field :tags, type: String
  field :visibility, type: String       # public, internal, private
  field :use_count, type: Integer
  field :official, type: Boolean
  
  field :icon_url, type: String

  field :like_list, type: Array, default: []
  field :likes, type: Integer, default: 0

  field :watch_list, type: Array, default: []
  field :watch_count, type: Integer, default: 0

  slug  :title

  has_and_belongs_to_many :catalogs, index: true

  belongs_to  :user, index: true
  belongs_to  :organization, index: true
  embeds_many :property_sets
  embeds_many :properties, as: :propertiable
  accepts_nested_attributes_for :properties, allow_destroy: true
  accepts_nested_attributes_for :property_sets, allow_destroy: true

  belongs_to :category, index: true
  
  validates_presence_of :title
  
  scope :public, where( visibility: 'public' )
  
  include Tire::Model::Search
  include Tire::Model::Callbacks

  mapping do
    indexes :id,           :index    => :not_analyzed
    indexes :title,        :analyzer => 'snowball', :boost => 100
    indexes :desciption,   :analyzer => 'snowball'
    indexes :tags,         :analyzer => 'keyword'
    indexes :company,      :analyzer => 'keyword'
  end    

  # Visibility
  def self.private
    where( :visibility.ne => 'public' )
  end

  def private
    where( :visibility.ne => 'public' )
  end
  
  def public?
    visibility == 'public'
  end

  def private?
    visibility == 'private'
  end

  # For Tire:
  def to_indexed_json
    self.to_json
  end

  def increase_use_count(count=1)
    self.inc :use_count, count
  end

  def decrease_use_count(count=1)
    self.inc :use_count, -count
  end

end
