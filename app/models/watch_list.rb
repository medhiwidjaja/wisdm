# Copyright (c) 2012 Medhi Widjaja

class WatchList
  include Mongoid::Document

  field :_id,
    pre_processed: true,
    default: ->{ Moped::BSON::ObjectId.new.to_s }
    
  has_one :user

  # field :article_ids, type: Array, :default => []
  has_and_belongs_to_many :articles, inverse_of: nil
end
