# Copyright (c) 2012 Medhi Widjaja

class Organization
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Slug
  include Mongoid::Document::Taggable

  field :name,                type: String
  field :description,         type: String
  field :website,             type: String
  field :language,            type: String, default: 'en'
  field :location,            type: String
  field :timezone,            type: String
  field :country_code,        type: String
  field :country_name,        type: String, :default => "unknown"
  field :hide_country,        type: Boolean, :default => false
  field :logo_filename,       type: String
  field :logo_content_type,   type: String
  slug  :name

  # Account type: free, basic, academic, pro, enterprise
  field :account,             type: String, default: "free"  

  field :like_list, type: Array, default: []
  field :likes, type: Integer, default: 0

  field :followers, type: Array, default: []
  field :followers_count, type: Integer, default: 0

  belongs_to :owner, class_name: 'User', inverse_of: :organization
  has_and_belongs_to_many :members, class_name: 'User', inverse_of: :memberships

  # NOTES:
  # To create an organization:
  #   org = user.organizations.new
  #
  # To add members:
  #   org.members << a_user
  #   org.members << [user1, user2, user3]
  #
  # To remove a member a user:
  #   org.members.delete(user2)
  #
  # User's memberships are stored in the User model in an array
  #   user.membership_ids

  has_many :catalogs
  has_many :products

  validates_presence_of :name

  after_create :add_owner_as_member

  # Class methods

  def self.all_with_members(user_ids)
    user_ids = [user_ids] unless user_ids.is_a? Array
    criteria.in(member_ids: user_ids)
  end

  # Instance methods

  def has_member?(user)
    members.include? user
  end

  def add_member!(user)
    members << user
  end

  def remove_member!(user)
    self.members.delete user unless user == owner
  end

  private

    def add_owner_as_member
      self.add_member! owner
    end
    
end

