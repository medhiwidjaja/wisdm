# Copyright (c) 2012 Medhi Widjaja

class Result
  attr_reader :vt, :sankey

  # TODO: Optimize! Cache computed value tree in embedded document here. 
  # Recompute only when article.model_updated_at > self.updated_at

  def initialize(article, root, participation, aggregate=false)
    unless participation.nil? or root.nil? or article.nil?
      @article = article
      @root = root
      @participation = participation
      @participation_id = participation.id
      @alternatives = article.alternatives

      if cache_is_stale? aggregate
        if aggregate
          @vt = @root.aggregate_value_tree
          @vt.globalize! :weight
          @article.aggregate = Aggregate.new vtdump: @vt.marshal_dump, vtdump_ts: Time.now.utc
        else
          @vt = @participation.value_tree @root
          @vt.globalize! :weight
          @participation.update_attributes vtdump: @vt.marshal_dump, vtdump_ts: Time.now.utc
        end
        @article.update_attribute :recalc, false
      else
        @vt = AnalysisMethods::ValueTree.new ('_id')
        if aggregate
          @vt.marshal_load @article.aggregate.vtdump
        else
          @vt.marshal_load @participation.vtdump
        end
      end
    else
      nil
    end
  end

  def cache_is_stale?(aggregate)
    if aggregate 
      @article.need_recalc? || @article.aggregate.nil? || @article.aggregate.vtdump.nil? ||  @article.model_updated_at > @article.aggregate.vtdump_ts
    else
      @article.need_recalc? || @participation.vtdump.nil? || @article.model_updated_at > @participation.vtdump_ts
    end
  end

  # Returns a table of alternatives' weighted scores, incl. detail by each criterion
  # Score table: {rank: x, alt_id: id, title: xxx, score: xx, obj:{"obj id":xxx,...}}  
  # TODO: Optimize this! Takes too many hits to the MongoDB database
  def score_table(root=@root, recalc=false)
    if @score_table.nil? or recalc
      Rails.logger.debug "Score table calculating new results"
      @score_table = @vt.search(root.id.to_s).consolidate :weight_g
    else
      Rails.logger.debug "Score table using available cache"
      @score_table
    end
  end

  def chart_data
    # Score table: {rank: x, alt_id: id, title: xxx, score: xx, obj:{"obj id":xxx,...}}
    score_table.map {|a| a[:score] }
  end

  def detail_chart_data
    score_table.map {|a| a[:obj].map {|o| o.last } }.transpose
  end

  def criteria_labels
    score_table.first[:obj].map {|o| @article.criteria.find(o.first).title }
  end

  def alternative_labels
    score_table.map {|a| split_lines(a[:abbrev].blank? ? a[:title] : a[:abbrev]) }
  end

  def alternative_names
    score_table.map {|a| a[:title] }
  end

  def sankey_table(criterion=@root)
    if criterion == @root
      vt = @vt
    else
      vt = @vt.search(criterion.id.to_s)
    end

    #base_node = @vt.search criterion.id.to_s
    i = 0
    vt.each do|node| 
      unless node.leaf?
        node.content.update(:no => i)
        i+=1
      end
    end
    nodes = []
    vt.each { |node| nodes << Hash[name: node.title] unless node.leaf? }
    no_of_criteria = nodes.size

    @alternatives.asc(:position).each { |alt| nodes << Hash[name: alt.title] }
    vt.each_leaf {|node| node.content.update(no: node.no+no_of_criteria) }
    
    vt.globalize! :weight
 
    links = vt.collect { |node| unless node == vt || node.weight_g==0.0 then Hash[source:node.parent.no, target:node.no, value: node.weight_g] end }
    Hash[nodes: nodes, links: links.compact!]
  end

  def benefit_cost_table
    costs = self.score_table.map { |alt| cost=@alternatives.find(alt[:alt_id]).cost; cost unless cost.nil? }
    sum = costs.sum
    i = 0
    self.score_table.map do |a| 
      cost = a[:cost] # costs[i]
      cost_n = cost/sum
      i+=1
      { title:a[:title], score:a[:score], cost:cost, cost_n:cost_n, ratio:a[:score]/cost_n }
    end.sort {|a,b| a[:ratio] <=> b[:ratio]}.reverse
  end

  def efficient_frontier_lines(cost_benefit_table)
    sorted_by_cost = cost_benefit_table.sort_by { |c| c[:cost] }
    filtered_list = sorted_by_cost.reduce ([sorted_by_cost.first]) { |c, a| 
                      slope1 = (a[:score]-c[-2][:score]) / (a[:cost_n]-c[-2][:cost_n]) unless c[-2].nil?
                      slope2 = (a[:score]-c.last[:score]) / (a[:cost_n]-c.last[:cost_n])
                      if (c.last[:score] < a[:score])
                        slope_ratio = 2  # determines how big of difference of ratios to exclude a point
                        if !c[-2].nil? and slope2 / slope1 > slope_ratio
                          c.pop
                        end
                        c << a 
                      end
                      c 
                    }
    filtered_list.collect { |a| [ a[:cost], a[:score] ] }
  end

  def sensitivity_data_for_criterion(criterion_id)
    sc = self.score_table @root
    node = @vt.search criterion_id.to_s
    @weight = node.weight_n

    # This setup assumes that the utility function with respect to weight is linear, 
    # otherwise we will have to calculate utility at every weight interval

    # Get intersection at weight = 0 for the selected objective
    node.content.update weight: 0.0
    @vt.globalize! :weight
    sc0 = @vt.consolidate :weight_g

    # Get intersection at weight = max for the selected objective
    # maximum weight value among the siblings:
    max_value = node.parent.children.collect {|c| c.weight}.max 
    node.content.update weight: max_value
    node.siblings.each {|sibling| sibling.content.update weight: 0.0 }
    @vt.globalize! :weight
    sc1 = @vt.consolidate :weight_g

    #hsc  =  sc.sort {|a,b| a[:alt_id] <=> b[:alt_id]}.reduce({}) {|hash,a| hash.merge Hash[a[:alt_id], [a[:score],a[:title]]]}
    hsc0 = sc0.sort_by {|s| s[:alt_id]}.reduce({}) {|hash,a| hash.merge Hash[a[:alt_id], [a[:score],a[:title]]]}
    hsc1 = sc1.sort_by {|s| s[:alt_id]}.reduce({}) {|hash,a| hash.merge Hash[a[:alt_id], [a[:score],a[:title]]]}

    [ hsc0.collect {|p| key=p.first; [[0.0,p.last.first], [1.0,hsc1[key].first]] },
    hsc0.collect {|p| p.last.last } ]
  end

  private

  # Split the string into 2 lines at the closest space before the center, separated by <br/>
  def split_lines(str)
    unless str.length < 10
      length = str.split.first.length > str.length/2 ? str.split.first.length : str.length/2
      regex = /(.{1,#{length}}) (.+)$/
      '<div style="text-align:center">'+str.scan(regex).join('<br/>')+'</div>'
    else
      '<div style="text-align:center">'+str+'</div>'
    end
  end
end