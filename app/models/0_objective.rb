# Copyright (c) 2012 Medhi Widjaja

# Deprecated !

class Objective
  include Mongoid::Document
  include Mongoid::Tree
  include Mongoid::Tree::Ordering
  include Mongoid::Tree::Traversal
  include DecisionAnalysis::AhpMethod::AhpNode
  include Mongoid::TreeExt
  
  field :title, type: String
  field :description, type: String
  field :position, type: Integer
  field :weight, type: Float
  field :weight_n, type: Float
  field :weight_g, type: Float
  field :total_r, type: Float
  field :total_w, type: Float
  field :method, type: Integer
  field :comparisons, type: Hash   # deprecated
  field :pairs, type: Hash         # deprecated
  field :active, type: Boolean, default: true
  field :cost, type: Boolean, default: false
  field :magiq_order, type: Integer
  field :tie, type: Boolean

  embeds_many :evaluations
  accepts_nested_attributes_for :evaluations, allow_destroy: false

  embeds_many :pairwise_comparisons
  accepts_nested_attributes_for :pairwise_comparisons, allow_destroy: false

  validates_presence_of :title, :article_id

  belongs_to :article, index: true

  accepts_nested_attributes_for :children, allow_destroy: false

  before_save :assign_default_position
  before_destroy :destroy_children

  # Scopes are still broken due to a bug in Mongoid 2.x
  #  related to default values
  scope :active, where(:active.ne => false).asc(:id)
  scope :inactive, where(:active.ne => true).asc(:id)
  scope :benefit, where(:cost.ne => true).asc(:id)
  scope :cost, where(:cost.ne => false).asc(:id)

  ##
  # Convert tree as ValueTree < Tree::TreeNode object. Passes each document node to
  # the block in the argument
  #
  # Example:
  # Suppose the document has title and value fields:
  #
  #   root.as_value_tree('title') { |o| {value: o.value} }
  #
  def as_value_tree(name, &hash)
    node = DecisionAnalysis::ValueTree.new(self[name].to_s, hash.call(self))
    unless self.leaf? 
      self.children.collect { |c| node << c.as_value_tree(name, &hash) }
    end
    node
  end

  def is_active?
    active
  end

  def is_cost?
    cost
  end

  # Returns true if any of the child objective nodes have no weight
  def has_incomplete_weights?
     self.children.map{|c| c.weight}.any? {|weight| weight.nil?}
  end

  # Returns an array of Objective ids within the tree that having no weight
  def with_incomplete_weights
    self.traverse {|o| if o.weight.nil? then o.id end }
  end

  # Returns whether the node has evaluations
  def has_empty_evaluations?
    self.evaluations.nil? or self.evaluations == []
  end

  # Returns whether the node's evaluations is incomplete
  def has_incomplete_evaluations?
    self.evaluations.count < self.article.alternatives.count
  end

  # Returns the alternatives that have not been rated yet 
  #  with respect to this node
  def alternatives_with_no_evaluation
    has_evaluations = self.evaluations.collect {|r| r.alternative_id }
    all_alternatives = self.article.alternatives.collect {|a| a.id }
    return all_alternatives - has_evaluations
  end

  def method_is_smart?
    self.method == 2
  end

  def method_is_magiq?
    self.method == 1
  end

  def method_is_ahp?
    self.method == 3
  end

  def set_method_smart
    self.set(:method, 2)
  end

  def set_method_magiq
    self.set(:method, 1)
  end
  
  def set_method_ahp
    self.set(:method, 3)
  end

  private

    def assign_default_position
      return unless self.position.nil? || self.parent_id_changed?

      if self.siblings.empty? || self.siblings.collect(&:position).compact.empty?
        self.position = 0
      else
        self.position = self.siblings.max(:position).to_i + 1
      end
    end
  
end