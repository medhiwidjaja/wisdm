# Copyright (c) 2012 Medhi Widjaja

class Evaluation
  include Mongoid::Document
  include AnalysisMethods::Weights
  include AnalysisMethods::Magiq
  include AnalysisMethods::Direct
  include AnalysisMethods::Ahp
  include AnalysisMethods::Rating
  include AnalysisMethods::Smart

  EVALUATION_METHODS = { 'MAGIQ' => 10,
                         'SMART' => 20, 
                         'Direct' => 30, 
                         'Rating' => 40, 
                         'Pairwise' => 50
                       }
  EVALUATION_METHOD_NAMES = EVALUATION_METHODS.invert

  RANK_METHODS = { :rank_order_centroid => 11,
                   :rank_sum => 12,
                   :rank_reciprocal => 13,
                   :rank_exponential => 14
                 }
  RANK_METHOD_NAMES = RANK_METHODS.invert
  
  AHP_METHODS = { :distributive_ahp => 51,
                  :ideal_ahp => 52,
                  :multiplicative_ahp => 53
                }

  AHP_METHOD_NAMES = AHP_METHODS.invert

  field :rank_order,  type: Integer
  field :eval_method, type: Integer
  field :rank_method, type: Integer
  field :eo, as: :eval_object, type: String    # the class of the objects we're evaluating
  field :valid, type: Boolean, default: true   # if false the user needs to redo the comparison

  belongs_to :evaluable, polymorphic:true, index: true
  belongs_to :participation, index: true

  validates_presence_of :eo, :eval_method

  index({evaluable_id: 1, participation_id: 1}, {unique: true})

  scope :evaluated, where(:eval_method.exists => true)
  
  def scorables
    article = self.evaluable.article
    case self.eval_object 
    when "Alternative"
      scorables = article.alternatives.asc(:position)
    when "Criterion"
      scorables = evaluable.children.asc(:id)
    end
  end

  def eval_method_name
    EVALUATION_METHOD_NAMES[eval_method]
  end

  def method_is_magiq?
    eval_method == EVALUATION_METHODS['MAGIQ']
  end

  def method_is_smart?
    eval_method == EVALUATION_METHODS['SMART']
  end

  def method_is_direct?
    eval_method == EVALUATION_METHODS['Direct']
  end

  def method_is_rating?
    eval_method == EVALUATION_METHODS['Rating']
  end

  def method_is_pairwise?
    eval_method == EVALUATION_METHODS['Pairwise']
  end

  def comparison_is_valid?
    valid
  end

  def self.method_verb(method)
    case method
    when EVALUATION_METHODS['Pairwise']
      'Pairwise compare'
    when EVALUATION_METHODS['Rating']
      'Rate'
    when EVALUATION_METHODS['Direct']
      'Direct compare'
    when EVALUATION_METHODS['SMART']
      'SMART compare'
    when EVALUATION_METHODS['MAGIQ']
      'Rank'
    end
  end        

end