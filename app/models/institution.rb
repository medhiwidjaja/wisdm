# Copyright (c) 2012 Medhi Widjaja

class Institution
  include Mongoid::Document
  include Mongoid::Timestamps

  field :no,        type: Integer
  field :region,    type: String
  field :tingkat,   type: String
  field :name,      type: String
  field :prodi,     type: String
  field :sk_no,     type: String
  field :sk_year,   type: String
  field :peringkat, type: String
  field :expiry,    type: String
  field :info_url,  type: String


  def self.import(filename)
    record = Institution.new
    num = 0
    IO.foreach(filename) do |line|
      if line =~ /START/
        record = Institution.new
        num = 0
      elsif line =~ /^<td><a href=\"(.*)\">\s*(.*)<\/a><\/td>/
        record.update_attribute :info_url, $1
        record.update_attribute :name, $2
      elsif line =~ /^<td>(.*)<\/td>/ 
        num += 1
        case (num)
        when 1
          record.update_attribute :no, $1
        when 2
          record.update_attribute :region, $1
        when 3
          record.update_attribute :tingkat, $1
        when 4
          record.update_attribute :prodi, $1
        when 5
          record.update_attribute :sk_no, $1
        when 6
          record.update_attribute :sk_year, $1
        when 7
          record.update_attribute :peringkat, $1
        when 8
          record.update_attribute :expiry, $1
        end
      elsif line =~ /^<\/tr><tr/
        record.save
        record = Institution.new
        num = 0
      end
    end
  end

  def self.test(filename)
    f = 0
    IO.foreach(filename) do |line| 
      if line=~/START/ 
        puts "BEGIN" 
        f = 0
      elsif line=~/END/ 
        f += 1
        puts "FIN" + f.to_s
      end
    end
  end


end