# Copyright (c) 2012 Medhi Widjaja

class Aggregate
  include Mongoid::Document

  field :vtdump,     type: Array    # marshal dump of ValueTree
  field :vtdump_ts,  type: Time
  # TODO: cache value tree in :vtdump

  embedded_in :article

end
