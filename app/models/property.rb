# Copyright (c) 2012 Medhi Widjaja

class Property
	include Mongoid::Document

  field :k,           type: String, as: :name    # key
  field :v,           as:   :value             # value
  field :unit,        type: String
  field :description, type: String
  field :cost,        type: Boolean   # to indicate whether this will be used as cost in
                                      # cost benefit analysis

  embedded_in :propertiable, polymorphic: true

  validates_presence_of :name
end