# Copyright (c) 2012 Medhi Widjaja

class Criterion
  include Mongoid::Document
  include Mongoid::Tree
  include Mongoid::Tree::Ordering
  include Mongoid::Tree::Traversal
  include Mongoid::TreeExt
  include Mongoid::Commentable
  
  field :title,           type: String
  field :abbrev,          type: String
  field :description,     type: String
  field :cost,            type: Boolean, default: false
  field :active,          type: Boolean, default: true
  field :weight_agg,      type: Float
  field :comparison_type, type: Integer, default: 0
  field :eval_method,     type: Integer
  field :property_link,   type: String

  validates_presence_of :title, :article_id

  belongs_to :article, index: true

  has_many :evaluations, as: :evaluable, dependent: :destroy
  # has_many :ratings, dependent: :destroy
  # has_many :scores, as: :scorable

  accepts_nested_attributes_for :children, allow_destroy: false

  before_save     :assign_default_position
  before_destroy  :destroy_children
  before_destroy  :invalidate_related_evaluations

  def active?
    active
  end

  def cost?
    cost
  end

  def evaluation_by(participant)
    if participant.class == User
      pid = self.article.participation_by(participant.id).id
    elsif participant.class == Participation
      pid = participant.id
    elsif participant.class == Moped::BSON::ObjectId or participant.class == String
      pid = participant
    end
    self.evaluations.where(participation_id: pid).first
  end

  def privileged_evaluation_by(participant)
    if participant.class == User
      user = participant
    elsif participant.class == Participation
      user = participant.user
    elsif participant.class == Moped::BSON::ObjectId or participant.class == String
      user = Participation.find(participant).user
    end
    action = self.leaf? ? :rate : :compare
    privileged = user.can? action, self.article
    
    self.reload if self.title.nil?   # Why do I need this???
    if privileged
      self.evaluation_by participant
    else
      self.evaluation_by self.article.user
    end
  end

  # Returns whether the node has a judgment by the requested user
  def has_evaluation_by?(participant)
    !self.privileged_evaluation_by(participant).nil?
  end

  def has_incomplete_ratings_by?(participation_id)
    if self.leaf?
      ev = self.privileged_evaluation_by(participation_id)
      if ev.nil? or !ev.comparison_is_valid?
        true
      else
        alts = Alternative.where(article_id: article.id)
        Score.where(judgment_id:ev.id, wrt:self.id).in(scorable_id: alts.map(&:_id)).any? &:blank?
      end
    else
      false
    end
  end

  def has_incomplete_scores_by?(participation_id)
    unless self.leaf?
      ev = self.privileged_evaluation_by(participation_id)
      if ev.nil? or !ev.comparison_is_valid?
        true
      else
        children_ids = self.children.map &:id
        Score.where(judgment_id:ev.id, wrt:self.id).in(scorable_id:children_ids).any? &:blank?
      end
    else
      false
    end
  end

  # Aggregate weight (average)
  def aggregate_weight(type=:geometric)
    unless self.root?
      n = self.parent.evaluations.evaluated.count
      begin
        if type==:arithmetic
          avg = self.parent.evaluations.evaluated.reduce(0.0) do |total, ev|
                  participant_weight = ev.participation.weight_n || 1.0/n
                  score = Score.find_by(judgment_id:ev.id, scorable_id:self.id, wrt:self.parent.id, eval_method:ev.eval_method)
                  total + (score.weight_n * participant_weight)
                end
        elsif type==:geometric
          avg = self.parent.evaluations.evaluated.reduce(1.0) do |product, ev|
                  participant_weight = ev.participation.weight_n || 1.0/n
                  score = Score.find_by(judgment_id:ev.id, scorable_id:self.id, wrt:self.parent.id, eval_method:ev.eval_method)
                  product * (score.weight_n ** participant_weight)
                end
        end
      rescue
        avg = nil
      end
    else
      avg = 1.0
    end
    avg
  end

  def aggregate_summary_table(type = :geometric)
    scored_items = self.leaf? ? self.article.alternatives : self.children
    unless scored_items.blank?
      table = scored_items.asc(:position).map do |o|
        w = self.leaf? ? o.aggregate_rating_wrt(self, type) : o.aggregate_weight(type)
        unless w.nil?
          Hash[
            no:       o.position + 1, 
            title:    o.title,
            weight_n: w,
            ratio:    w * 100
          ]
        else
          nil
        end
      end
    end
    table unless table.any? &:blank?
  end

  def aggregate_detail_table
    table = self.evaluations.collect {|ev| [ev.participation.username, ev.score_table] }
    # table unless table.first.nil? || table.first.any?(&:blank?)
    table.collect { |r| r unless r.any? &:blank? }.compact
  end

  def evaluation_table(participation_id)
    unless self.children.empty?
      children_evaluations = self.children.collect {|child| child.evaluation_by(participation_id) }
      unless children_evaluations.any? {|eval| eval.nil? || eval.weight.nil? }
        max = children_evaluations.max_by {|eval| eval.weight }.weight
        children_evaluations.map {|eval| Hash[
          no:       eval.evaluable.position+1, 
          title:    eval.evaluable.title, 
          order:    eval.rank_order, 
          weight:   eval.weight.to_f, 
          weight_n: eval.weight_n.to_f,
          # ratio:    (max.nil? or max==0) ? 0 : eval.weight.to_f/max*100
          ratio:    eval.weight_n.to_f*100
        ]}
      else
        self.children.map {|c| Hash[no:c.position+1, title:c.title]}
      end
    end
  end

  def as_json(participation_id, options={})
    {
      label: self.title, 
      id: self.id, 
      weights_incomplete: self.has_incomplete_scores_by?(participation_id),
      ratings_incomplete: self.has_incomplete_ratings_by?(participation_id),
      children: self.children.map { |child| child.as_json(participation_id) }
    }
  end

  def as_tree(participation_id)
    attributes = self.as_json(participation_id)
    attributes.to_json
  end

  ##
  # Convert tree as ValueTree < Tree::TreeNode object. Passes each document node to
  # the block in the argument
  #
  # Example:
  # Suppose the document has title and value fields:
  #
  #   root.as_value_tree('title') { |o| {value: o.value} }
  #
  def as_value_tree(name, &proc)
    node = AnalysisMethods::ValueTree.new(self[name].to_s, proc.call(self))
    unless self.leaf? 
      self.children.collect { |c| node << c.as_value_tree(name, &proc) }
    else
      self.article.alternatives.collect { |a| node << AnalysisMethods::ValueTree.new(a[name].to_s, proc.call(a)) }
    end
    node
  end

  def aggregate_value_tree(type=:geometric)
    name = "_id"
    node = AnalysisMethods::ValueTree.new(self[name].to_s, { 
      title: self.title, 
      abbrev: self.abbrev,
      no: self.position,
      weight: self.aggregate_weight(type),
      normalize: true
    })
    unless self.leaf? 
      self.children.collect { |c| node << c.aggregate_value_tree(type) }
    else 
      self.article.alternatives.collect do |a| 
        node << AnalysisMethods::ValueTree.new(a[name].to_s, { 
          title: a.title, 
          abbrev: a.abbrev,
          no: a.position,
          cost: a.cost,
          weight: a.aggregate_rating_wrt(Criterion.find(node.name), type),
          normalize: true
        })
      end
    end
    node
  end

  private

  def invalidate_related_evaluations
    # TODO: This may need some optimization with map-reduce or even MongoDB's findAndModify function
    unless self.root?
      article_dirty = false
      self.parent.evaluations.each do |ev|
        ev_dirty = false
        for comparisons in [ev.smart_comparisons, ev.pairwise_comparisons, ev.rank_comparisons]
          unless comparisons.empty?
            if comparisons.where(id1:self.id).exists?
              comparisons.where(id1:self.id).destroy
              ev_dirty = true
            end
            if comparisons.where(id2:id).exists?
              comparisons.where(id2:id).destroy
              ev_dirty = true            
            end
          end
        end
        if ev_dirty
          ev.update_attribute :valid, false
          article_dirty = true
        end
      end
      if article_dirty
        self.article.update_attribute :recalc, true
        Score.where(scorable_id: self.id).destroy
      end
    end
  end
end





