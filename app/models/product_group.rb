# Copyright (c) 2012 Medhi Widjaja

class ProductGroup
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Tree
  include Mongoid::Tree::Traversal
  include Mongoid::Tree::Ordering
  include Mongoid::Slug

  field :title,         type: String
  field :description,   type: String
  field :active,        type: Boolean, default: true
  field :icon_uri,      type: String
  field :color,         type: String
  
  belongs_to :catalog, index: true
  has_many :products

  validates_presence_of :title
  accepts_nested_attributes_for :children, allow_destroy: false
  
end
