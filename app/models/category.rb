class Category
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Slug

  field :title, :type => String
  field :description, :type => String, default: ""
  field :count, :type => Integer
  field :color, :type => String
  field :followers_count, :type => Integer
  slug  :title

  validates_length_of :title, :minimum => 2
  validates_uniqueness_of :title

  has_many :articles

end
