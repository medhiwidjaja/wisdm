# Copyright (c) 2012 Medhi Widjaja

class Notification
  include Mongoid::Document
  include Mongoid::Timestamps
  extend ActiveSupport::Concern

  field :email, 		   type: String
  field :accepted, 	   type: Boolean, default: false
  field :state, 		   type: String,  default: "pending"
  field :accepted_by,  type: String
  field :accepted_at,  type: Time
  field :sender_id,    type: String
  field :sender_name,  type: String
  field :article_id,   type: String
  field :article_name, type: String
  field :body, 			   type: String
  field :model,        type: String
  field :model_id,     type: String
  field :type,         type: String

  belongs_to :user

  index({user_id: 1})
  index({email: 1})

  validates_length_of :body, :in => 0..400

  scope :pending, where(state: 'pending')

  # ClassMethods
  def Notification.new_notice(person, model, type, sender, subject, body="")
    if body.empty?
      case type
      when 'invite'
        body = "Your opinions count. You have been invited to participate in the decision."
      when 'approve'
        body = "Your request to join the decision has been approved."
      when 'request'
        body = "#{sender.name} has requested to join in the decision."
      when 'confirm'
        body = "Yes, #{sender.name} has agreed to join in the decision."
      when 'decline'
        body = "Sorry to decline. Thanks for the invitation."
      when 'reject'
        body = "Sorry, your request cannot be approved at this moment."
      end
    end

    Notification.new user_id: person.id, 
                      email: person.email, 
                      model: model.class, 
                      model_id: model.id,  
                      sender_id: sender.id,
                      sender_name: sender.name,
                      article_id: model.article_id,
                      subject: subject,
                      body: body,
                      type: type
  end

  # Instance methods

  def pending?
    state == 'pending'
  end

  def title
    {'invite' => 'Invitation', 
     'approve' => 'Approval', 
     'request' => 'Request',
     'confirm' => 'Confirmation',
     'decline' => 'Declined',
     'reject' => 'Not approved' }[ self.type ]
  end
end