# Copyright (c) 2012 Medhi Widjaja

# Deprecated !

class Judgment
  include Mongoid::Document
  include DecisionAnalysis::Base
  include DecisionAnalysis::Smart
  include DecisionAnalysis::Magiq
  include DecisionAnalysis::Ahp

  belongs_to :criterion, index: true

  belongs_to :participation, index: true

  index({criterion_id: 1 , participation_id: 1 }, {unique: true} )

  # Returns the children of the criterion related to this judgment
  def criterion_children
    self.criterion.children
  end

  # def children_judgments
  #   criterion_children.
  # end

  def method_is_smart?
    self.type == 2
  end

  def method_is_magiq?
    self.type == 1
  end

  def method_is_ahp?
    self.type == 3
  end

  def set_method_smart
    self.set(:type, 2)
  end

  def set_method_magiq
    self.set(:type, 1)
  end
  
  def set_method_ahp
    self.set(:type, 3)
  end
end