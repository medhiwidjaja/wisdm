# Copyright (c) 2012 Medhi Widjaja

# Deprecated !

class Rating
  include Mongoid::Document

  field :weight,    type: Float
  field :weight_n,  type: Float
  field :weight_g,  type: Float
  field :magiq_order, type: Integer
  field :notes, type: String
  
  belongs_to :alternative, index: true
  # belongs_to_related :participation, index: true
  belongs_to :participation, index: true
  # belongs_to_related :criterion, index:true       # with respect to some Criterion
  belongs_to :criterion, index:true

  index({alternative_id: 1, participation_id: 1, criterion_id: 1 }, {unique: true})

end