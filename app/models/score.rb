# Copyright (c) 2012 Medhi Widjaja

class Score
  include Mongoid::Document

  field :rating,     type: Float
  field :weight,     type: Float
  field :weight_n,   type: Float
  field :weight_g,   type: Float
  field :rank_order, type: Integer
  field :total_r,    type: Float
  field :total_w,    type: Float
  field :eval_method, type: Integer
  field :normalize,  type: Boolean, default: true
  
  field :wrt, as: :with_respect_to
  field :scorable_id
  field :scorable_type, type: String

  belongs_to :judgment, polymorphic: true, index: true  # belongs to :evaluation or :aggregate

  index({judgment_id: 1, scorable_id: 1, wrt: 1, eval_method: 1 }, {unique: true})
  index({judgment_id: 1, scorable_id: 1, wrt: 1 })
  index({scorable_id: 1})

  scope :with_method, ->(method) { where(eval_method: method) }

  def scorable
    self.scorable_type.constantize.where(id:self.scorable_id).first
  end

end