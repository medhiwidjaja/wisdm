# Copyright (c) 2012 Medhi Widjaja

class PairwiseComparison
  include Mongoid::Document
  
  field :id1, type: Moped::BSON::ObjectId
  field :id2, type: Moped::BSON::ObjectId
  field :value, type: String

  embedded_in :objective
end