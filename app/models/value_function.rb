# Copyright (c) 2012 Medhi Widjaja
  
class ValueFunction
  include Mongoid::Document
  field :type,    type: String
  field :xmin,    type: Float
  field :xmax,    type: Float
  field :ymin,    type: Float
  field :ymax,    type: Float

end
