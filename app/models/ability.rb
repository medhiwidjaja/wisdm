class Ability
	include CanCan::Ability

	def initialize(user)
		# Define abilities for the passed in user here. For example:
		#
		#   user ||= User.new # guest user (not logged in)
		#   if user.admin?
		#     can :manage, :all
		#   else
		#     can :read, :all
		#   end
		#
		# The first argument to `can` is the action you are giving the user permission to do.
		# If you pass :manage it will apply to every action. Other common actions here are
		# :read, :create, :update and :destroy.
		#
		# The second argument is the resource the user can perform the action on. If you pass
		# :all it will apply to every resource. Otherwise pass a Ruby class of the resource.
		#
		# The third argument is an optional hash of conditions to further filter the objects.
		# For example, here the user can only update published articles.
		#
		#   can :update, Article, :published => true
		#
		# See the wiki for details: https://github.com/ryanb/cancan/wiki/Defining-Abilities
		
		alias_action :new_request, :send_request, to: :request

		@user = user || User.new # guest user (not logged in)

		@user.roles << 'guest' if @user.new_record?

		# This one calls each method according to the roles
		@user.roles.each { |role| send(role.downcase) }
	end

	# Admin user can do anything
	def admin
		can :manage, :all
	end

	# An editor can manage the website: Featured article, article categories
	def editor
		member
		can :manage, FeaturedArticle
		can :manage, FeaturedProduct
		can :manage, Category
	end

	# Guest, a non-signed in user, can only view public articles
	def guest
		can :read, Article, visibility: 'public'
		can :view, Article, visibility: 'public'
		can :read, User
		can :read, Category
		can :read, Product, visibility: 'public'
	end

	# Member is a registered user
	def member
		# Member includes all guest's privileges
		guest
		# Member's account type determines his/her abilities
		send @user.account.downcase
	end

	# Types of user account: free, basic, academic, pro and enterprise

	def free
		# Article
		cannot :manage, Article, visibility: 'private'
		cannot :manage, Article, visibility: 'internal'
		can :manage, Article do |article|
			(article.public? && article.user_id == @user.id)
		end
		can :modify, Article do |article|
			@user.permissions_in(article, include:'modify') && !article.internal?
		end
		can :compare, Article do |article|
			@user.permissions_in(article, include:'compare') && !article.internal?
		end
		can :rate, Article do |article|
			@user.permissions_in(article, include:'rate') && !article.internal?
		end
		can :analyze, Article do |article|
			@user.permissions_in(article, include:'analyze') && !article.internal?
		end
		can :view, Article do |article|
			@user.permissions_in(article, include:'view') && !article.internal?
		end
		can :read, Article do |article|
			@user.permissions_in(article, include:'view') && !article.internal?
		end
		can :update, Comparison do |comparison|
			comparison.participation.user == @user
		end
		can :update, Evaluation do |evaluation|
			evaluation.participation.user == @user
		end
		# Product
		cannot :manage, Product, visibility: 'private'
		cannot :manage, Product, visibility: 'internal'
		can :manage, Product do |product|
			(product.public? && product.user_id == @user.id)
		end
		# Participation
		can :create, Participation, article: { user: @user }
		can :read,   Participation do |participation|
			participation.article.user == @user || participation.user == @user
		end
		can :update, Participation, article: { user: @user }
		can :delete, Participation do |participation|
			participation.article.user == @user || participation.user == @user
		end
		can :request, Participation, article: { visibility: 'public' }
		can :confirm, Participation do |participation|
			participation.user == @user && !participation.article.internal?
		end
		can :approve, Participation, article: { user: @user }
		# Notification
		can :manage, Notification, user: @user
		# Organization
		can :read, Organization do |organization|
			organization.has_member? @user
		end
		# User
		can [:read, :account, :update], User, _id: @user.id
	end

	def basic
		free
		can :manage, Article do |article|
			(article.private? && article.user_id == @user.id)
		end
		can :modify, Article do |article|
			@user.permissions_in article, include: 'modify' && !article.internal?
		end
		can :compare, Article do |article|
			@user.permissions_in article, include: 'compare' && !article.internal?
		end
		can :rate, Article do |article|
			@user.permissions_in article, include: 'rate' && !article.internal?
		end
		can :analyze, Article do |article|
			@user.permissions_in article, include: 'analyze' && !article.internal?
		end
		can [:read, :view], Article do |article|
			@user.permissions_in article, include: 'view' && !article.internal?
		end
		can :request, Participation
		can :manage, Organization, owner: @user		
		can :create, Product
		can :manage, Product do |product|
			product.user_id == @user.id
		end
	end

	def academic
		basic
		can :manage, Article do |article|
			article.internal? && article.user_id == @user.id
		end
		can :create_report, Article do |article|
			article.user_id == @user.id
		end
		can :import_export_excel, Article do |article|
			article.user_id == @user.id
		end
	end

	def pro
		basic
		can :manage, Article do |article|
			article.internal? && article.user_id == @user.id
		end
		can :create, Catalog
		can :manage, Catalog, user: @user
		can :confirm, Participation do |participation|
			participation.user == @user
		end
		can :create_pdf_report, Article
		can :import_export_excel, Article
	end

	def commerce
		pro
	end
	
	def enterprise
		pro
	end

end
