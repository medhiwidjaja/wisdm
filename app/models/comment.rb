# Copyright (c) 2012 Medhi Widjaja

class Comment
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Tree
  include Mongoid::Tree::Ordering
  include Mongoid::Tree::Traversal

  field :body, type: String
  field :likes, type: Array         # array of user_ids
  field :like_count, type: Integer

  belongs_to :user, index: true
  belongs_to :commentable, polymorphic: true, index: true

  validates_presence_of :body
  validates_presence_of :user

  before_destroy :move_children_to_parent
  after_create :increment_count
  after_destroy :decrement_count

  # Class Methods
  def self.find_comments_for(object)
    where(commentable_id:object.id).desc(:created_at)
  end

  def self.find_comments_by(user)
    where(user_id:user.id).desc(:created_at)
  end

  def self.root_comments_for(object) 
    where(commentable_id:object.id).roots
  end

  # Instance Methods
  def save_reply(reply)
    reply.save
    self.children << reply
  end

  def liked_by(user)
    self.likes.add_to_set user.id
    self.inc :like_count, 1
  end

  def disliked_by(user)
    self.likes.pull user.id
    self.inc :like_count, -1
  end

  protected

  def increment_count
    self.commentable.inc :comment_count, 1
  end

  def decrement_count
    self.commentable.inc :comment_count, -1
  end

end
