# Copyright (c) 2012 Medhi Widjaja

class Alternative
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Commentable
  
  field :title, type: String
  field :abbrev, type: String, default: nil
  field :description, type: String, default: nil
  field :position, type: Integer

  validates_presence_of :title, :article_id
  validates_numericality_of :position
  
  embeds_many :properties, as: :propertiable
  accepts_nested_attributes_for :properties, allow_destroy: true

  # has_many :ratings, dependent: :destroy

  belongs_to :article, index: true
  belongs_to :product, index: true

  around_destroy :invalidate_related_evaluations

  def cost
    begin
      val_str = properties.where(cost:true).first.v.strip 
      if /^[+-]?[0-9]{1,3}(?:(?:\,[0-9]{3})*(?:\.[0-9]+)?|[0-9]*(?:\.[0-9]+)?)$/.match val_str
        value = val_str.delete(',').to_f
      end
      value
    rescue
      nil
    end
  end

  def rating_for(participation_id, criterion_id)
    self.ratings.where(participation_id: participation_id).where(criterion_id: criterion_id).first
  end

  # Aggregate rating (average) with respect to a criterion
  # TODO: Use map/reduce functions of MongoDB
  def aggregate_rating_wrt(criterion, type=:geometric)
    n = criterion.evaluations.evaluated.count
    unless n==0
      if type==:arithmetic
        avg = criterion.evaluations.evaluated.reduce(0.0) do |total, ev|
                participant_weight = ev.participation.weight_n || 1.0/n
                score = Score.find_by(judgment_id:ev.id, scorable_id:self.id, wrt:criterion.id, eval_method:ev.eval_method)
                total + (score.weight_n * participant_weight)
              end
      elsif type==:geometric
        avg = criterion.evaluations.evaluated.reduce(1.0) do |product, ev|
                participant_weight = ev.participation.weight_n || 1.0/n
                score = Score.find_by(judgment_id:ev.id, scorable_id:self.id, wrt:criterion.id, eval_method:ev.eval_method)
                product * (score.weight_n ** participant_weight)
              end
      end
    end
    #rescue
    #  avg = nil
    #end
    avg
  end

  def invalidate_related_evaluations
    art = self.article
    id = self.id
    criteria_ids = art.criteria.leaves.collect(&:id)

    yield

    article_dirty = false
    # check all evaluations under the article's criteria leaf nodes
    evaluations = Evaluation.in(evaluable_id: criteria_ids).where(eo:'Alternative')
    evaluations.each do |ev|
      ev_dirty = false
      for comparisons in [ev.smart_comparisons, ev.pairwise_comparisons, ev.rank_comparisons]
        unless comparisons.empty?
          if comparisons.where(id1:id).exists?
            comparisons.where(id1:id).destroy
            ev_dirty = true        
          end
          if comparisons.where(id2:id).exists?
            comparisons.where(id2:id).destroy  
            ev_dirty = true          
          end
        end
      end
      if ev_dirty
        ev.update_attribute :valid, false
        article_dirty = true
      end
    end
    if article_dirty
      art.update_attribute :recalc, true 
      Score.where(scorable_id: id).destroy
    end
  end

end
