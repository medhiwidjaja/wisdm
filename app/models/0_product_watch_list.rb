# Copyright (c) 2012 Medhi Widjaja

# DEPRECATED

class ProductWatchList
  include Mongoid::Document

  field :_id,
    pre_processed: true,
    default: ->{ Moped::BSON::ObjectId.new.to_s }
    
  has_one :user

  has_and_belongs_to_many :products, inverse_of: nil
end