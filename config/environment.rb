# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
Wisdm::Application.initialize!

# For debugging errors:
Rails.logger = Logger.new(STDOUT)