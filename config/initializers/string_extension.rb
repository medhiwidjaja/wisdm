class String
  def numeric?
    true if Float(self) rescue false
  end

  def integer?
    self =~ /\A[+-]?\d+\Z/ ? true : false
  end
end