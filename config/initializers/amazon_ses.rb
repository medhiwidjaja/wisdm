ActionMailer::Base.add_delivery_method :ses, AWS::SES::Base,
  :access_key_id     => Figaro.env.amazon_access_key,
  :secret_access_key => Figaro.env.amazon_secret_key