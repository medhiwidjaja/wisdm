Wisdm::Application.routes.draw do

  root to: "home#index"

  devise_for :users,
             :controllers  => {
               :registrations => 'my_devise/registrations',
               :sessions      => 'my_devise/sessions'
             }
  
  resources :users do
    member do
      get :following, :followers, :my_articles
    end
    resources :friend_lists, only: [:create, :destroy]
  end

  resources :articles do
    post :add_property
    resources :criteria
    resources :alternatives
    resources :participations do
      get :invite
      get :request
      post :approve
      post :submit_request
    end
  end

  resources :criteria do
    resources :judgments do
      get :smart_compare
      get :pairwise_compare
      get :magiq_compare
      put :update_smart
      put :update_pairwise
      put :update_magiq
    end
    resources :analyses do
      collection do
        get :sensitivity_chart
        get :rank
        get :what_if
        get :cost_benefit
      end
    end
  end

  resources :articles do
    post :add_property

    resources :alternatives
    resources :participations do
      get :invite
      get :request
    end

    resources :criteria do
      resources :judgments
      member do
        get :smart_compare
        get :pairwise_compare
        get :magiq_compare
        put :update_smart
        put :update_pairwise
        put :update_magiq

        resources :evaluations do
          get :smart_evaluate
          get :pairwise_evaluate
          get :magiq_evaluate
          put :update_smart_eval
          put :update_pairwise_eval
          put :update_magiq_eval
          post :compare
        end
      end
    end

    resources :objectives do
      resources :analyses do
        collection do
          get :sensitivity_chart
          get :rank
          get :what_if
          get :cost_benefit
        end
      end        
      member do
        get :direct_compare
        get :pairwise_compare
        get :leaves_compare
        get :magiq_compare
        put :update_direct
        put :update_pairwise
        put :update_magiq
        get :compare

        resources :ratings do
          collection do
            put :update_leaves
            get :direct_evaluate
            get :pairwise_evaluate
            get :magiq_evaluate
            put :update_direct_eval
            put :update_pairwise_eval
            put :update_magiq_eval
            post :compare
          end
        end
      end
    end
  end

  match 'articles/:article_id/evaluate', to: 'ratings#evaluate', as: 'article_evaluate'
  match 'articles/:article_id/rate', to: 'evaluations#evaluate', as: 'article_rate'
  match 'articles/:article_id/analyze', to: 'analyses#analyze', as: 'article_analysis'
  match 'articles/:article_id/sensitivity', to: 'analyses#sensitivity', as: 'article_sensitivity'
  match 'articles/:article_id/whatif', to: 'analyses#whatif', as: 'article_whatif'
  match 'articles/:article_id/cost_benefit', to: 'analyses#cost_benefit', as: 'article_cost_benefit'
  match 'articles/:article_id/objectives/:id/new_sub', to: 'objectives#new_sub', as: 'new_sub'
  match 'articles/:article_id/objectives/:objective_id/create_sub', to: 'objectives#create_sub', via: :post, as: 'create_sub'
  match 'articles/:article_id/criteria/:id/new_sub', to: 'criteria#new_sub', as: 'new_sub_criterion'
  match 'articles/:article_id/criteria/:criterion_id/create_sub', to: 'criteria#create_sub', via: :post, as: 'create_sub_criterion'
  match 'articles/:article_id/incomplete_eval', to: 'analyses#incomplete_eval', as: 'incomplete_eval'
  match 'articles/:article_id/common_properties/:property', to: 'articles#common_properties', as: 'article_common_properties'
  match 'articles/:article_id/update_common_properties/:property', to: 'articles#update_common_properties', via: :post, as: 'article_update_common_properties'
  # resources :users, only: [:index, :show]
  
  match '/new_and_updated', to: 'articles#new_and_updated'
  match '/my_articles', to: 'users#my_articles'
  
  scope '/manage', :as => 'manage' do
    controller 'admin/manage' do
      match 'feature' => :feature
      match 'users' => :users
      match 'setting' => :settings
    end
  end

  match '/tree', to: 'home#tree'
  
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
