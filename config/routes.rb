Wisdm::Application.routes.draw do

  resources :catalogs


  root to: "home#index"

  # devise_for :users
  devise_for :users,
             :controllers  => {
               :registrations => 'my_devise/registrations',
               :sessions      => 'my_devise/sessions',
               :passwords     => 'my_devise/passwords',
               :unlocks       => 'my_devise/unlocks',
               :confirmations => 'my_devise/confirmations'
             }
  
  resources :users do
    collection do
      get :index
    end
    member do
      get :show, :following, :followers, :my_articles, :account
    end
    resources :friend_lists, only: [:create, :destroy]
  end

  resources :articles do

    post :add_property
    post :update_alternatives_order
    get  :show_participants
    post :like
    
    resources :alternatives do
      collection do
        get  :search
        post :search
        post :add
      end
    end

    resources :criteria, only: [:index, :create, :new]

    resources :watch_lists, only: [:create, :destroy]
    resources :participations do
      collection do
        get  :invite
        post :send_invitation
        get  :new_request
        post :send_request
        get  :assign_weights
        post :update_weights
      end
      member do
        post :approve
        post :confirm
      end
    end

    resources :comments, only: [:edit, :create, :update, :destroy]
  end

  resources :criteria do
    get :tree
    post :move
    get :aggregate_detail
    get :aggregate_summary

    resources :comparisons do
      collection do
        post :compare
        get :smart
        get :pairwise
        get :magiq
        put :update_comparison
      end
    end

    resources :evaluations do
      collection do
        get :rank
        get :smart
        get :pairwise
        get :direct
        get :adjust
        put :update_rank
        put :update_smart
        put :update_pairwise
        put :update_direct
        put :update_evaluation
      end
    end

    member do
      get :smart_compare
      get :pairwise_compare
      get :magiq_compare
      put :update_smart
      put :update_pairwise
      put :update_magiq
      get :new_sub
      post :create_sub
    end

    resources :ratings do
      collection do
        get :aggregate_detail
        get :aggregate_summary
      end
    end
    
    resources :results do
      collection do
        get :sensitivity_chart
        get :rank
        get :flow_diagram
        get :sankey
        get :benefit_cost
      end
    end

    resources :comments, only: [:edit, :create, :update, :destroy]
  end

  resources :products do
    resources :product_watch_lists, only: [:create, :destroy]
    collection do
      get   :featured
      get   :top
    end
    post :like
    post :unlike
    post :watch
    post :unwatch
    get  :info_panel
  end

  resources :categories

  resources :notifications, only: [:show] do
    member do
      post :dismiss
    end
  end

  resources :organizations do
    member do
      get    :members, to: :new_members
      post   :members, to: :add_members
      # delete :members, to: :remove_members
      match 'members/:member_id', to: :remove_members, via: :delete, as: 'remove_member'
    end
  end
  
  # Results
  match 'articles/:article_id/rate', to: 'ratings#evaluate', as: 'article_rate'
  match 'articles/:article_id/results', to: 'results#results', as: 'article_results'
  match 'articles/:article_id/sensitivity_analysis', to: 'results#sensitivity_analysis', as: 'article_sensitivity_analysis'
  match 'articles/:article_id/sensitivity_chart', to: 'results#sensitivity_analysis', as: 'article_sensitivity_chart'
  match 'articles/:article_id/flow_diagram', to: 'results#flow_diagram', as: 'article_flow_diagram'
  match 'articles/:article_id/sankey', to: 'results#sankey', as: 'article_sankey'
  match 'articles/:article_id/benefit_cost', to: 'results#benefit_cost', as: 'article_benefit_cost'
  match 'articles/:article_id/incomplete', to: 'results#incomplete', as: 'article_incomplete'

  #### These are routes for old parts, Deprecated 
  # match 'articles/:article_id/evaluate', to: 'evaluations#evaluate', as: 'article_evaluate'
  # match 'articles/:article_id/analyze', to: 'analyses#analyze', as: 'article_analysis'
  # match 'articles/:article_id/sensitivity', to: 'analyses#sensitivity', as: 'article_sensitivity'
  # match 'articles/:article_id/whatif', to: 'analyses#whatif', as: 'article_whatif'
  # match 'articles/:article_id/cost_benefit', to: 'analyses#cost_benefit', as: 'article_cost_benefit'
  # match 'articles/:article_id/incomplete_eval', to: 'analyses#incomplete_eval', as: 'incomplete_eval'
  # match 'articles/:article_id/objectives/:id/new_sub', to: 'objectives#new_sub', as: 'new_sub'
  # match 'articles/:article_id/objectives/:objective_id/create_sub', to: 'objectives#create_sub', via: :post, as: 'create_sub'
  ####

  # Comments
  match '/alternatives/:alternative_id/comments', to: 'comments#create', as: 'alternative_comments', via: :post
  match '/alternatives/:alternative_id/comments/new', to: 'comments#new', as: 'new_alternative_comment', via: :get
  match '/alternatives/:alternative_id/comments/:id', to: 'comments#update', as: 'alternative_comment', via: :put
  match '/alternatives/:alternative_id/comments/:id/edit', to: 'comments#edit', as: 'edit_alternative_comment', via: :get
  match '/alternatives/:alternative_id/comments/:id', to: 'comments#destroy', as: 'alternative_comment', via: :destroy
  match '/alternatives/:alternative_id/comments/:id/reply', to: 'comments#reply', as: 'reply_alternative_comment', via: :post
  match '/articles/:article_id/comments/:id/reply', to: 'comments#reply', as: 'reply_article_comment', via: :post
  match '/criteria/:criterion_id/comments/:id/reply', to: 'comments#reply', as: 'reply_criterion_comment', via: :post
  match '/articles/:article_id/comments/:id/new_reply', to: 'comments#new_reply', as: 'new_reply_article_comment', via: :get
  match '/alternatives/:alternative_id/comments/:id/new_reply', to: 'comments#new_reply', as: 'new_reply_alternative_comment', via: :get
  match '/criteria/:criterion_id/comments/:id/new_reply', to: 'comments#new_reply', as: 'new_reply_criterion_comment', via: :get

  # Alternative properties
  match 'articles/:article_id/common_properties/:property', to: 'articles#common_properties', as: 'article_common_properties'
  match 'articles/:article_id/update_common_properties/:property', to: 'articles#update_common_properties', via: :post, as: 'article_update_common_properties'
  match 'articles/:article_id/edit_common_properties', to: 'articles#edit_common_properties', via: :get, as: 'article_edit_common_properties'
  match 'articles/:article_id/common_properties', to: 'articles#save_common_properties', via: :put, as: 'article_save_common_properties'

  match 'articles/:article_id/alternatives/:id/add_property_row', to: 'alternatives#add_property_row', as: 'add_property_row'

  # Articles
  match 'featured_articles', to: 'articles#featured'
  match 'top_articles', to: 'articles#top'
  match 'new_and_updated', to: 'articles#new_and_updated'
  match 'my_articles', to: 'articles#my_articles'
  match 'participating', to: 'articles#participating'
  match 'watch_list', to: 'articles#watch_list'

  # Users
  match 'current_user', to: 'users#current'

  # Prices and plans
  match 'plans', to: 'home#plans'
  
  # Admin/management
  scope '/manage', :as => 'manage' do
    resources :featured_articles
    resources :featured_products
    resources :categories
    controller 'admin/manage' do
      # match 'feature' => :feature
      match 'users' => :users
      match 'user/:user_id' => :user, as: 'user', via: :get
      match 'user/:user_id' => :update_user_account, as: 'user', via: :post
      match 'setting' => :settings
    end
  end

  # API points
  scope '/api', as: 'api' do
    match 'articles/:article_id/rate', to: 'api/ratings#evaluate', as: 'article_rate'
    match 'articles/:article_id/results', to: 'api/results#results', as: 'article_results'
    match 'articles/:article_id/sensitivity_analysis', to: 'api/results#sensitivity_analysis', as: 'article_sensitivity_analysis'
    match 'articles/:article_id/sensitivity_chart', to: 'api/results#sensitivity_analysis', as: 'article_sensitivity_chart'
    match 'articles/:article_id/flow_diagram', to: 'api/results#flow_diagram', as: 'article_flow_diagram'
    match 'articles/:article_id/sankey', to: 'api/results#sankey', as: 'article_sankey'
    match 'articles/:article_id/benefit_cost', to: 'api/results#benefit_cost', as: 'article_benefit_cost'
    match 'articles/:article_id/incomplete', to: 'api/results#incomplete', as: 'article_incomplete'
  end

  match 'public_errors', to: 'public_errors#routing'
  match '*a', :to => 'public_errors#routing'
  
  # match '/tree', to: 'home#tree'

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
