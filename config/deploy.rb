set :application, "foreplot"
set :repository,  "git@bitbucket.org:medhiwidjaja/wisdm.git"

# set :scm, :git # You can set :scm explicitly or Capistrano will make an intelligent guess based on known version control directory names
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`

role :web, "foreplot.com"                          # Your HTTP server, Apache/etc
role :app, "foreplot.com"                          # This may be the same as your `Web` server
# role :db,  "foreplot.com", :primary => true # This is where Rails migrations will run
# role :db,  "foreplot.com"

set :deploy_to, "/var/www"

set :user, "ubuntu"
set :password, nil

set :scm_username, "medhiwidjaja"

ssh_options[:keys] = ["~/.ec2/nv2-keypair"]

# if you want to clean up old releases on each deploy uncomment this:
# after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
# namespace :deploy do
#   task :start do ; end
#   task :stop do ; end
#   task :restart, :roles => :app, :except => { :no_release => true } do
#     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
#   end
# end